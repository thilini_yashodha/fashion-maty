package com.example.fashiondesigner;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.example.fashiondesigner.order.tailorViewOrder;
import com.example.fashiondesigner.order.viewOrder;
import com.example.fashiondesigner.suggest.suggestViewT;

public class TailorOrderFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view= inflater.inflate(R.layout.fragment_order,null);

        CardView ongoing=view.findViewById(R.id.onGoing);
        CardView complete=view.findViewById(R.id.complete);
        CardView suggest=view.findViewById(R.id.suggest);

        ongoing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), tailorViewOrder.class);
                intent.putExtra("clickView","1");
                startActivity(intent);
            }
        });


        complete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), tailorViewOrder.class);
                intent.putExtra("clickView","2");
                startActivity(intent);
            }
        });


        suggest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), suggestViewT.class);
                startActivity(intent);
            }
        });



        return view;

    }

}

