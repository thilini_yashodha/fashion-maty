package com.example.fashiondesigner;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fashiondesigner.Adapter.Model.VerticalModel;
import com.example.fashiondesigner.Adapter.VerticalRecyclerViewAdapter;
import com.example.fashiondesigner.chat.chatUsersAdapter;
import com.example.fashiondesigner.chat.chatUsersModel;
import com.example.fashiondesigner.chat.userModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ChatFragment extends Fragment {

    RecyclerView recyclerView;
    chatUsersAdapter userAdupter;
    ArrayList<chatUsersModel> mUsers;

    String senderUserID;

    private List<String> userList;
    DatabaseReference mdata;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragmnet_chat,container,false);

        final FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
        senderUserID=user.getUid();


        mUsers=new ArrayList<>();

        recyclerView=view.findViewById(R.id.chatReView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));

        userAdupter=new chatUsersAdapter(getActivity(),mUsers);
        recyclerView.setAdapter(userAdupter);


        userList=new ArrayList<>();

        mdata= FirebaseDatabase.getInstance().getReference();

        mdata.child("chats").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                userList.clear();

                for(DataSnapshot ds:dataSnapshot.getChildren())
                {
                    userModel chat=ds.getValue(userModel.class);

                    if(chat.getSender().equals(senderUserID))
                    {
                       if(!(userList.contains(chat.getReciver())))
                       {
                           userList.add(chat.getReciver());
                       }

                    }
                    if(chat.getReciver().equals(senderUserID))
                    {
                        if(!(userList.contains(chat.getSender())))
                        {
                            userList.add(chat.getSender());
                        }

                    }
                }

                readChats();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return view;

    }


    private void readChats()
    {
        mUsers.clear();
       for(final String id:userList)
       {
           mdata.child(id).child("customerData").addValueEventListener(new ValueEventListener() {
               @Override
               public void onDataChange(@NonNull DataSnapshot dataSnapshotc) {



                   if(dataSnapshotc.exists())
                   {
                       String imageUrl=dataSnapshotc.child("imageUrl").getValue().toString();
                       String name=dataSnapshotc.child("name").getValue().toString();

                       chatUsersModel umodel=new chatUsersModel();

                       umodel.setImaageUrl(imageUrl);
                       umodel.setName(name);
                       umodel.setUserID(id);

                       mUsers.add(umodel);
                       userAdupter.notifyDataSetChanged();
                   }

               }

               @Override
               public void onCancelled(@NonNull DatabaseError databaseError) {

               }
           });

           mdata.child(id).child("tailorData").addValueEventListener(new ValueEventListener() {
               @Override
               public void onDataChange(@NonNull DataSnapshot dataSnapshott) {


                   if(dataSnapshott.exists())
                   {
                       String imageUrl=dataSnapshott.child("imageuri").getValue().toString();
                       String name=dataSnapshott.child("businessName").getValue().toString();

                       chatUsersModel umodel=new chatUsersModel();

                       umodel.setImaageUrl(imageUrl);
                       umodel.setName(name);
                       umodel.setUserID(id);

                       mUsers.add(umodel);
                       userAdupter.notifyDataSetChanged();
                   }
               }

               @Override
               public void onCancelled(@NonNull DatabaseError databaseError) {

               }
           });
       }


    }
}
