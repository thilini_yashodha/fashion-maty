package com.example.fashiondesigner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class Tailor2 extends AppCompatActivity
        implements BottomNavigationView.OnNavigationItemSelectedListener{

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tailor2);

        BottomNavigationView navigation=findViewById(R.id.tbottomNav);
        navigation.setOnNavigationItemSelectedListener(this);


        loadFragment(new ItemFragment());
    }

    private boolean loadFragment(Fragment fragment)
    {
        if(fragment != null)
        {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.tfragmentContainer,fragment)
                    .commit();

            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment=null;

        switch (item.getItemId())
        {
            case R.id.itemID:
                fragment=new ItemFragment();
                break;

            case R.id.torderID:
                fragment=new TailorOrderFragment();
                break;

            case R.id.tchatID:
                fragment=new ChatFragment();
                break;

            case R.id.tprofileID:
                fragment=new TailorProfileFragment();
                break;
        }

        return loadFragment(fragment);

    }


}
