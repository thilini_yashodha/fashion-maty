package com.example.fashiondesigner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Register extends AppCompatActivity {

    EditText getemail, password,rePassword;
    Button btCustomer,btTailor;
    FirebaseAuth mFirebaseAuth;
    DatabaseReference mdata;


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        FirebaseAuth.getInstance().signOut();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mFirebaseAuth = FirebaseAuth.getInstance();
        getemail = findViewById(R.id.editText);
        password = findViewById(R.id.editText2);
        rePassword = findViewById(R.id.editText3);
        btCustomer = findViewById(R.id.button);
        btTailor = findViewById(R.id.button2);
        mdata = FirebaseDatabase.getInstance().getReference();



        btCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email= getemail.getText().toString();
                String pwd = password.getText().toString();
                String rePwd = rePassword.getText().toString();



                if(email.isEmpty())
                {
                    getemail.setError("Please Enter email");
                    getemail.requestFocus();
                }

                else if (pwd.isEmpty())
                {
                    password.setError("Please Enter Password");
                    password.requestFocus();
                }

                else if (rePwd.isEmpty())
                {
                    rePassword.setError("Please Verify your Password");
                    rePassword.requestFocus();
                }


                else if(email.isEmpty() && pwd.isEmpty() && rePwd.isEmpty())
                {
                    Toast.makeText(Register.this,"Fiels are empty!",Toast.LENGTH_SHORT).show();
                }

                else if (!(pwd.equals(rePwd)))
                {
                    rePassword.setError("Error!  Re-enter email to verification ");
                    rePassword.requestFocus();
                }

                else if(!(email.isEmpty() && pwd.isEmpty() && rePwd.isEmpty()))
                {
                    mFirebaseAuth.createUserWithEmailAndPassword(email,pwd).addOnCompleteListener(Register.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if(!task.isSuccessful())
                            {
                                Toast.makeText(Register.this,"Registration is unsuccessful!",Toast.LENGTH_SHORT).show();

                            }

                            else
                            {

                                final FirebaseUser currentUser=mFirebaseAuth.getCurrentUser();
                                currentUser.sendEmailVerification().addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {

                                        Toast.makeText(Register.this,"Verification email has been sent!",Toast.LENGTH_SHORT).show();

                                            String user_id = mFirebaseAuth.getCurrentUser().getUid();
                                            mdata.child(user_id).child("flag").setValue("0");
                                            mdata.child(user_id).child("tag").setValue("0");
                                            Toast.makeText(Register.this,"Registration is successful!",Toast.LENGTH_SHORT).show();
                                            FirebaseAuth.getInstance().signOut();
                                            startActivity(new Intent(Register.this,MainActivity.class));
                                            finish();



                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {

                                        Toast.makeText(Register.this,"email verification is fail! : "+e.getMessage(),Toast.LENGTH_SHORT).show();
                                    }
                                });


                            }
                        }
                    });
                }

                else
                {
                    Toast.makeText(Register.this,"Error occured!",Toast.LENGTH_SHORT).show();
                }


            }
        });




        btTailor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email= getemail.getText().toString();
                String pwd = password.getText().toString();
                String rePwd = rePassword.getText().toString();


                if(email.isEmpty())
                {
                    getemail.setError("Please Enter email");
                    getemail.requestFocus();
                }

                else if (pwd.isEmpty())
                {
                    password.setError("Please Enter Password");
                    password.requestFocus();
                }

                else if (rePwd.isEmpty())
                {
                    rePassword.setError("Please Verify your Password");
                    rePassword.requestFocus();
                }


                else if(email.isEmpty() && pwd.isEmpty() && rePwd.isEmpty())
                {
                    Toast.makeText(Register.this,"Fiels are empty!",Toast.LENGTH_SHORT).show();
                }

                else if (!(pwd.equals(rePwd)))
                {
                    rePassword.setError("Error!  Re-enter email to verification ");
                    rePassword.requestFocus();
                }

                else if(!(email.isEmpty() && pwd.isEmpty() && rePwd.isEmpty()))
                {
                    mFirebaseAuth.createUserWithEmailAndPassword(email,pwd).addOnCompleteListener(Register.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if(!task.isSuccessful())
                            {
                                Toast.makeText(Register.this,"Registration is unsuccessful!",Toast.LENGTH_SHORT).show();

                            }

                            else
                            {

                                final FirebaseUser currentUser=mFirebaseAuth.getCurrentUser();
                                currentUser.sendEmailVerification().addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {

                                        Toast.makeText(Register.this,"Verification email has been sent!",Toast.LENGTH_SHORT).show();

                                        String user_id = mFirebaseAuth.getCurrentUser().getUid();
                                        mdata.child(user_id).child("flag").setValue("1");
                                        mdata.child(user_id).child("tag").setValue("0");
                                        Toast.makeText(Register.this,"Registration is successful!",Toast.LENGTH_SHORT).show();
                                        FirebaseAuth.getInstance().signOut();
                                        startActivity(new Intent(Register.this,MainActivity.class));
                                        finish();


                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {

                                        Toast.makeText(Register.this,"email verification is fail! : "+e.getMessage(),Toast.LENGTH_SHORT).show();
                                    }
                                });

                            }
                        }
                    });
                }

                else
                {
                    Toast.makeText(Register.this,"Error occured!",Toast.LENGTH_SHORT).show();
                }


            }
        });




    }
}
