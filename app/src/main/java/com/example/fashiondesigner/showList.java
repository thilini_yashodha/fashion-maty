package com.example.fashiondesigner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fashiondesigner.Adapter.HorizontalREcyclerViewAdapter;
import com.example.fashiondesigner.Adapter.Model.HorizontalModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class showList extends AppCompatActivity  {

    String click,clickItemName,subClick;
    ListView listView;
    List<String> name;
    List<String> imageString;
    List<String> price;
    List<String> pushIDArr;
    List<String> imageBack;

    DatabaseReference mdata;
    String user_ID;

    showList.MyAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_list);


        Intent intent = getIntent();
        click = intent.getStringExtra("clickCategory").toLowerCase();
        subClick = intent.getStringExtra("subClickCategory").toLowerCase();
        clickItemName = intent.getStringExtra("clickItemName").toLowerCase();


        listView=findViewById(R.id.itemList);

        TextView title=findViewById(R.id.clickItem);
      //  TextView yourList=findViewById(R.id.yourList);


        title.setText(clickItemName);
     //   yourList.setText("YOUR " + clickItemName.toUpperCase() + " LIST");

        FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
        user_ID=user.getUid();
        mdata= FirebaseDatabase.getInstance().getReference().child(user_ID).child("item").child(click).child(subClick).child(clickItemName).child("fullDress");

        name = new ArrayList<>();
        imageString=new ArrayList<>();
        price=new ArrayList<>();
        pushIDArr=new ArrayList<>();
        imageBack=new ArrayList<>();

        final ArrayList<HorizontalModel> a = new ArrayList<>();

        adapter=new showList.MyAdapter(this,name,imageString,price,pushIDArr,imageBack);

        mdata.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                a.clear();
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    HorizontalModel data= dataSnapshot1.getValue(HorizontalModel.class);

                    a.add(data);
                }

                for(int i=0;i<a.size();i++)
                {
                    name.add(a.get(i).getName());
                    imageString.add(a.get(i).getImageUrl());
                    price.add(a.get(i).getPrice());
                    pushIDArr.add(a.get(i).getPushID());
                    imageBack.add(a.get(i).getImageBack());

                    adapter.notifyDataSetChanged();
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });




        listView.setAdapter(adapter);

       listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           @Override
           public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

               String clickpushID=pushIDArr.get(position);
               String title=name.get(position);
               String pc=price.get(position);
               String image=imageString.get(position);
               Intent intent = new Intent(showList.this ,showListClick.class);
               intent.putExtra("clickCategory",click);
               intent.putExtra("subClickCategory",subClick);
               intent.putExtra("clickItemName",clickItemName);
               intent.putExtra("pushID",clickpushID);
               intent.putExtra("title",title);
               intent.putExtra("price",pc);
               intent.putExtra("image",image);
               startActivity(intent);
           }
       });


    }

    class MyAdapter  extends ArrayAdapter<String>
    {
        Context context;
        List<String> mTitle;
        List<String> mImages;
        List<String> mPrice;
        List<String> mpushID;
        List<String> mImageBack;


        MyAdapter(Context c,List<String> title,List<String> image,List<String> iprice,List<String> pushID,List<String> iBack)
        {
            super(c,R.layout.row_view_item,title);
            this.context=c;
            this.mTitle=title;
            this.mPrice=iprice;
            this.mpushID=pushID;
            this.mImages=image;
            this.mImageBack=iBack;

        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            LayoutInflater layoutInflater=(LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row=layoutInflater.inflate(R.layout.row_view_item,parent,false);
            ImageView images= row.findViewById(R.id.imageView);
            ImageView imageB=row.findViewById(R.id.imageViewBack);
            TextView titles=row.findViewById(R.id.textView);
            TextView prices=row.findViewById(R.id.textPrice);


            Picasso.get().load(mImages.get(position)).into(images);

            if (mImageBack.get(position) != null)
            {
                Picasso.get().load(mImageBack.get(position)).into(imageB);
                imageB.setVisibility(View.VISIBLE);
            }


            titles.setText(mTitle.get(position).toUpperCase());
            prices.setText("$ "+mPrice.get(position));

            return row;
        }
    }



}
