package com.example.fashiondesigner;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ItemFragment extends Fragment {

    Spinner category;
    Button set;
    ListView list;
    ArrayList<String> arrayList;
    ArrayAdapter<String> adapter;
    ArrayAdapter<String> categoryAdapter;

    DatabaseReference mdata;
    String uid,result;
    List<String> selectCategory;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view= inflater.inflate(R.layout.fragment_item,null);


        category=view.findViewById(R.id.spinner);
        set=view.findViewById(R.id.button3);
        list=view.findViewById(R.id.list);

        selectCategory=new ArrayList<>();
        selectCategory.add("-select your category-");


        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        uid=user.getUid();
        mdata= FirebaseDatabase.getInstance().getReference();


        mdata.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot dataSnapshot1 : dataSnapshot.child("selectCategory").getChildren()) {
                    String data = dataSnapshot1.getValue().toString();

                        selectCategory.add(data.toUpperCase());

                }
                selectCategory.add("Add Category");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        arrayList=new ArrayList<String>();
        adapter=new ArrayAdapter<String>(getActivity(), R.layout.raw_item,arrayList);

        categoryAdapter=new ArrayAdapter<>(getActivity(),android.R.layout.simple_spinner_item,selectCategory);
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        category.setAdapter(categoryAdapter);


        category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                result=category.getSelectedItem().toString();
                String gdh="dj";
                if((!result.equals("-select your category-"))&& (!result.equals("Add Category")))
                {
                    set.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {



                            if(!(arrayList.contains(result)))
                            {
                                mdata.child(uid).child("category").push().setValue(result.toLowerCase());
                                arrayList.add(result.toUpperCase());
                                adapter.notifyDataSetChanged();
                            }

                        }
                    });
                }

                else if (result.equals("Add Category")) {

                     alert();

                }

                }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        mdata.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                arrayList.clear();
                for (DataSnapshot dataSnapshot1 : dataSnapshot.child(uid).child("category").getChildren()) {
                    String data = dataSnapshot1.getValue().toString();
                    arrayList.add(data.toUpperCase()); // add all data into list.
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        list.setAdapter(adapter);


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String click =(String) parent.getItemAtPosition(position);
                Intent intent = new Intent(getActivity() ,category.class);
                intent.putExtra("click",click);
                startActivity(intent);

            }
        });



        return view;


    }


    public void alert()
    {
        View view1=(LayoutInflater.from(getActivity())).inflate(R.layout.tailordalogbox,null);
        AlertDialog.Builder alertdialog=new AlertDialog.Builder(getActivity());
        alertdialog.setView(view1);


        final EditText getData=view1.findViewById(R.id.reenterbn);

        alertdialog.setCancelable(false)
                .setPositiveButton("ADD", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String s=getData.getText().toString();
                        if(!(arrayList.contains(s)))
                        {
                            mdata.child(uid).child("category").push().setValue(s.toLowerCase());
                            selectCategory.remove(0);
                            selectCategory.remove(selectCategory.size() - 1);
                            selectCategory.add(s.toUpperCase());
                            mdata.child("selectCategory").setValue(selectCategory);
                            selectCategory.add(0,"-select your category-");
                            selectCategory.add("Add Category");


                            arrayList.add(s.toUpperCase());
                            adapter.notifyDataSetChanged();
                        }


                    }

                })
                .setNegativeButton("CANCLE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });



        alertdialog.create();
        alertdialog.show();

    }



}




