package com.example.fashiondesigner.measurements;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.fashiondesigner.R;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class deliverAddress extends AppCompatActivity {

    String category,subCategory,itemName,imageUrl,userID,fabricKey,colourKey,fullDRessPushID,orderPushID,customerUerID;
    EditText edName,edTele,edStreet,edHomeNo,edState,edCity,edZip;
    Spinner country;
    Button conform;

    DatabaseReference mdata;

    LatLng cPoint,tPoint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deliver_address);

        Intent intent = getIntent();
        category = intent.getStringExtra("category").toLowerCase();
        subCategory = intent.getStringExtra("subCategory").toLowerCase();
        itemName = intent.getStringExtra("itemName").toLowerCase();
        imageUrl = intent.getStringExtra("imageUrl");
        userID = intent.getStringExtra("userID");
        fabricKey = intent.getStringExtra("fabricKey");
        colourKey = intent.getStringExtra("colourKey");
        fullDRessPushID=intent.getStringExtra("fulldressPushID");
        orderPushID=intent.getStringExtra("orderPushID");
        customerUerID=intent.getStringExtra("customerUserID");

        mdata= FirebaseDatabase.getInstance().getReference().child(customerUerID).child("order");

        edName=findViewById(R.id.name);
        edTele=findViewById(R.id.tele);
        edStreet=findViewById(R.id.street);
        edHomeNo=findViewById(R.id.homeNo);
        edState=findViewById(R.id.state);
        edCity=findViewById(R.id.city);
        edZip=findViewById(R.id.zip);

        country=findViewById(R.id.spiner);
        conform=findViewById(R.id.conform);


        Locale[] locales = Locale.getAvailableLocales();
        ArrayList<String> countries = new ArrayList<String>();
        for (Locale locale : locales) {
            String country = locale.getDisplayCountry();
            if (country.trim().length() > 0 && !countries.contains(country)) {
                countries.add(country);
            }
        }
        Collections.sort(countries);


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, countries);
        // set the view for the Drop down list
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // set the ArrayAdapter to the spinner
        country.setAdapter(dataAdapter);
        country.setSelection(97);


        final DatabaseReference tailordata= FirebaseDatabase.getInstance().getReference().child(userID).child("tailorData");
        tailordata.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                String addresst=dataSnapshot.child("address").getValue().toString();
                String countryt=dataSnapshot.child("country").getValue().toString();
                String cityt=dataSnapshot.child("city").getValue().toString();

                String tadd=addresst+","+countryt+","+cityt;

                tPoint=getLocationFromAddress(deliverAddress.this,tadd);

            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



        conform.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                HashMap<String,Object> map=new HashMap<>();

                String result=country.getSelectedItem().toString();

                String name=edName.getText().toString();
                String tele=edTele.getText().toString();
                String street=edStreet.getText().toString();
                String home=edHomeNo.getText().toString();
                String state=edState.getText().toString();
                String city=edCity.getText().toString();
                String zip=edZip.getText().toString();

                String add=street+","+home+","+state+","+city+","+city+","+zip;
                cPoint=getLocationFromAddress(deliverAddress.this,add);

                if(tPoint!=null && cPoint!=null)
                {
                    Location startPoint=new Location("locationA");
                    startPoint.setLatitude(tPoint.latitude);
                    startPoint.setLongitude(tPoint.longitude);

                    Location endPoint=new Location("locationB");
                    endPoint.setLatitude(cPoint.latitude);
                    endPoint.setLongitude(cPoint.longitude);

                    double distance=startPoint.distanceTo(endPoint);
                    double distanceKM=distance/1000;
                    map.put("distance",distanceKM);
                }
                else
                {
                    map.put("distance","0.0");
                }




                map.put("country",result);
                map.put("name",name);
                map.put("tele",tele);
                map.put("street",street);
                map.put("home",home);
                map.put("state",state);
                map.put("city",city);
                map.put("zip",zip);


                mdata.child(orderPushID).child("deliverAddress").setValue(map);

                Intent intent = new Intent(deliverAddress.this, placeOrder.class);
                intent.putExtra("userID",userID);
                intent.putExtra("category",category);
                intent.putExtra("subCategory",subCategory);
                intent.putExtra("itemName",itemName);
                intent.putExtra("fulldressPushID",fullDRessPushID);
                intent.putExtra("fabricKey",fabricKey);
                intent.putExtra("colourKey",colourKey);
                intent.putExtra("imageUrl",imageUrl);
                intent.putExtra("orderPushID",orderPushID);
                intent.putExtra("customerUserID",customerUerID);
                startActivity(intent);



            }
        });

    }


    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng(location.getLatitude(), location.getLongitude() );
            String jhf="jdf";

        } catch (Exception ex) {

            ex.printStackTrace();
        }

        return p1;
    }

}
