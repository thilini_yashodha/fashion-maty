package com.example.fashiondesigner.measurements;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.fashiondesigner.R;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class ladiesMeasurement extends AppCompatActivity {

    DatabaseReference mdata;
    String click,clickItemName,subClick,pushID;
    String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ladies_measurement);

        Intent intent = getIntent();
        click = intent.getStringExtra("clickCategory").toLowerCase();
        subClick = intent.getStringExtra("subClickCategory").toLowerCase();
        clickItemName=intent.getStringExtra("clickItemName").toLowerCase();
        pushID=intent.getStringExtra("pushID");


        FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
        userID=user.getUid();
        mdata= FirebaseDatabase.getInstance().getReference().child(userID);

        PhotoView photoView = findViewById(R.id.photo);
        photoView.setImageResource(R.drawable.women);


        LinearLayout checkBoxContainer = findViewById(R.id.checkList);
        CheckBox checkBox;

        final ArrayList<String> list=new ArrayList<>();
        list.add("A  - Neck round");
        list.add("B  - Above bust round");
        list.add("C  - Bust round");
        list.add("D  - Below bust round");
        list.add("E  - Waist round");
        list.add("F  - Hips round");
        list.add("G  - Armhole round");
        list.add("H  - Bicep round");
        list.add("I  - Elbow round");
        list.add("J  - Wrist round");
        list.add("K  - Thigh round");
        list.add("L  - Knee round");
        list.add("M  - Ankle round");
        list.add("N  - Neck to apex");
        list.add("O  - Shoulder");
        list.add("P  - Cross Front");
        list.add("Q  - Creoss Back");
        list.add("R  - From weist to end of dress length");
        list.add("S  - From shoulder to end of dres length");
        list.add("T  - Blouse lengh(up to waist or end of dress)");
        list.add("U  - Neck depth(Back)");
        list.add("V  - Neck depth(Front)");
        list.add("W  - Full hand length");
        list.add("X  - Half length hand");



        final  ArrayList<CheckBox> arryOfCheckBox=new ArrayList<>();

        for(int i=0;i<list.size();i++)
        {
            checkBox=new CheckBox(this);
            checkBox.setId(i);
            checkBox.setText(list.get(i));
            checkBox.setTag(list.get(i));

            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked)
                    {
                        Toast.makeText(ladiesMeasurement.this,"selected",Toast.LENGTH_SHORT).show();
                    }
                }
            });

            arryOfCheckBox.add(checkBox);
            checkBoxContainer.addView(checkBox);

        }

        Button conform = findViewById(R.id.conform);
        final ArrayList<String> selected=new ArrayList<>();

        conform.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i=0;i<arryOfCheckBox.size();i++)
                {
                    if(arryOfCheckBox.get(i).isChecked())
                    {

                        selected.add(list.get(i));


                    }
                }

                DatabaseReference allMdata=FirebaseDatabase.getInstance().getReference();
                allMdata.child("allItems").child(pushID).child("measureState").setValue("1");

                mdata.child("measurements").child(pushID).child("data").setValue(selected);
                mdata.child("measurements").child(pushID).child("state").setValue("1");
                mdata.child("measurements").child(pushID).child("gender").setValue("0");
                Intent intent = new Intent(ladiesMeasurement.this , conformMeasurement.class);
                intent.putExtra("clickCategory",click);
                intent.putExtra("subClickCategory",subClick);
                intent.putExtra("clickItemName",clickItemName);
                intent.putExtra("pushID",pushID);
                intent.putExtra("userID",userID);
               // intent.putExtra("gender",0);
                startActivity(intent);


            }
        });




    }
}
