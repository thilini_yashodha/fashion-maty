package com.example.fashiondesigner.measurements;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.fashiondesigner.R;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class gentMeasuremet extends AppCompatActivity {

    DatabaseReference mdata;
    String click,clickItemName,subClick,pushID;
    String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gent_measuremet);


        Intent intent = getIntent();
        click = intent.getStringExtra("clickCategory").toLowerCase();
        subClick = intent.getStringExtra("subClickCategory").toLowerCase();
        clickItemName=intent.getStringExtra("clickItemName").toLowerCase();
        pushID=intent.getStringExtra("pushID");


        FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
        userID=user.getUid();
        mdata= FirebaseDatabase.getInstance().getReference().child(userID);

        PhotoView photoView = findViewById(R.id.photo);
        photoView.setImageResource(R.drawable.men);

        LinearLayout checkBoxContainer = findViewById(R.id.checkList);
        CheckBox checkBox;

        final ArrayList<String> list=new ArrayList<>();
        list.add("1  - Neck circumference");
        list.add("2  - Chest upper");
        list.add("3  - Chest lower");
        list.add("4  - Waist circumference");
        list.add("5  - Hip circumference");
        list.add("6  - Tight circumference at widest palce");
        list.add("7  - Knee circumference just below kneecap");
        list.add("8  - Calf at widest");
        list.add("9  - Arms eye");
        list.add("10 - Blcep circumference");
        list.add("11 - Wrist circumference");
        list.add("12 - Shoulder to shoulder");
        list.add("13 - Neck to waist");
        list.add("14 - Crotch(rise)");
        list.add("15 - Inner thigh");
        list.add("16 - Knee to ankle");
        list.add("17 - Knee to shoulder");
        list.add("18 - From shoulder to end of dres length");
        list.add("19 - Elbow to wrist");
        list.add("20 - Outer thigh");
        list.add("21 - Waist to knee for coats");



        final  ArrayList<CheckBox> arryOfCheckBox=new ArrayList<>();

        for(int i=0;i<list.size();i++)
        {
            checkBox=new CheckBox(this);
            checkBox.setId(i);
            checkBox.setText(list.get(i));
            checkBox.setTag(list.get(i));

            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked)
                    {
                        Toast.makeText(gentMeasuremet.this,"selected",Toast.LENGTH_SHORT).show();
                    }
                }
            });

            arryOfCheckBox.add(checkBox);
            checkBoxContainer.addView(checkBox);

        }

        Button conform = findViewById(R.id.conform);
        final ArrayList<String> selected=new ArrayList<>();

        conform.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i=0;i<arryOfCheckBox.size();i++)
                {
                    if(arryOfCheckBox.get(i).isChecked())
                    {

                        selected.add(list.get(i));


                    }
                }


                DatabaseReference allMdata=FirebaseDatabase.getInstance().getReference();
                allMdata.child("allItems").child(pushID).child("measureState").setValue("1");

                mdata.child("measurements").child(pushID).child("data").setValue(selected);
                mdata.child("measurements").child(pushID).child("state").setValue("1");
                mdata.child("measurements").child(pushID).child("gender").setValue("1");
                Intent intent = new Intent(gentMeasuremet.this , conformMeasurement.class);
                intent.putExtra("clickCategory",click);
                intent.putExtra("subClickCategory",subClick);
                intent.putExtra("clickItemName",clickItemName);
                intent.putExtra("pushID",pushID);
                intent.putExtra("userID",userID);
                //intent.putExtra("gender",1);
                startActivity(intent);


            }
        });





    }
}
