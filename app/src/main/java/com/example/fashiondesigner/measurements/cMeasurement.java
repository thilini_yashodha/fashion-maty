package com.example.fashiondesigner.measurements;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.fashiondesigner.Adapter.Model.measurement;
import com.example.fashiondesigner.Adapter.Model.searchViewModel;
import com.example.fashiondesigner.Adapter.measurementAdupter;
import com.example.fashiondesigner.Adapter.searchFulldressAdupter;
import com.example.fashiondesigner.R;
import com.example.fashiondesigner.chat.meassageActivity;
import com.example.fashiondesigner.customerSearch.viewSeaechColour;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

public class cMeasurement extends AppCompatActivity {

    String category,subCategory,itemName,imageUrl,userID,fabricKey,colourKey,fullDRessPushID;

    DatabaseReference mdata;
    PhotoView photoView;

    ArrayList<measurement> arrayListMeasurement;
    RecyclerView recyclerView;
    measurementAdupter adapter;
    int k=0;

    ArrayList<String> inputMeasurement;
    ArrayList<String> measurementName;
    String customerUserID;

    EditText height;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_c_measurement);

        Intent intent = getIntent();
        category = intent.getStringExtra("category").toLowerCase();
        subCategory = intent.getStringExtra("subCategory").toLowerCase();
        itemName = intent.getStringExtra("itemName").toLowerCase();
        imageUrl = intent.getStringExtra("imageUrl");
        userID = intent.getStringExtra("userID");
        fabricKey = intent.getStringExtra("fabricKey");
        colourKey = intent.getStringExtra("colourKey");
        fullDRessPushID=intent.getStringExtra("fulldressPushID");


        height=findViewById(R.id.height);

        mdata= FirebaseDatabase.getInstance().getReference().child(userID).child("measurements").child(fullDRessPushID);

        FirebaseUser currentUser= FirebaseAuth.getInstance().getCurrentUser();
        customerUserID=currentUser.getUid();

        final DatabaseReference customerData=FirebaseDatabase.getInstance().getReference().child(customerUserID);

        photoView = findViewById(R.id.photo);
        Button conform=findViewById(R.id.conform);
        Button video=findViewById(R.id.video);

        Button bookTailor=findViewById(R.id.bookTailor);

        bookTailor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(cMeasurement.this, meassageActivity.class);
                intent.putExtra("userID",userID);
                startActivity(intent);

            }
        });

        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(cMeasurement.this, PlayVideo.class);
                startActivity(intent);
            }
        });


        arrayListMeasurement = new ArrayList<>();

        recyclerView=findViewById(R.id.recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        adapter=new measurementAdupter(this,arrayListMeasurement);
        recyclerView.setAdapter(adapter);



        mdata.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String gender=dataSnapshot.child("gender").getValue().toString();

                if(gender.equals("1"))
                {
                    photoView.setImageResource(R.drawable.men);
                }
                else if(gender.equals("0"))
                {
                    photoView.setImageResource(R.drawable.women);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



        mdata.child("data").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot1) {
                for(DataSnapshot ds:dataSnapshot1.getChildren())
                {
                    measurement mModel=new measurement();
                    String data=ds.getValue().toString();
                    mModel.setQuestion(data);
                    arrayListMeasurement.add(mModel);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        inputMeasurement=new ArrayList<>();
        measurementName=new ArrayList<>();
        final HashMap<String,Object> map=new HashMap<>();

        conform.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String ht=height.getText().toString();


                if((arrayListMeasurement.size()==inputMeasurement.size()) && (ht!= null)  )
                {
                    DatabaseReference  pushData=  customerData.child("order").push();
                    String push_key=pushData.getKey();
                    map.put("mData",inputMeasurement);
                    map.put("orderPushID",push_key);
                    map.put("height",ht);

                    pushData.setValue(map);

                    customerData.child("order").child(push_key).child("orderState").setValue("0");


                    Toast.makeText(cMeasurement.this,"saved",Toast.LENGTH_SHORT).show();


                    Intent intent = new Intent(cMeasurement.this, deliverAddress.class);
                    intent.putExtra("userID",userID);
                    intent.putExtra("category",category);
                    intent.putExtra("subCategory",subCategory);
                    intent.putExtra("itemName",itemName);
                    intent.putExtra("fulldressPushID",fullDRessPushID);
                    intent.putExtra("fabricKey",fabricKey);
                    intent.putExtra("colourKey",colourKey);
                    intent.putExtra("imageUrl",imageUrl);
                    intent.putExtra("orderPushID",push_key);
                    intent.putExtra("customerUserID",customerUserID);
                    startActivity(intent);

                }
                else
                {
                    Toast.makeText(cMeasurement.this,"You should provide all the data!",Toast.LENGTH_SHORT).show();
                }


            }
        });


    }


    public void goTOGetMeasuremet(String data, String question) {

        if(!(measurementName.contains(question)))
        {
            measurementName.add(question);
            inputMeasurement.add(data);
        }
        else
        {
            for (int j=0;j<measurementName.size();j++)
            {
                if(question==measurementName.get(j))
                {
                    inputMeasurement.set(j,data);
                    break;
                }
            }
        }

    }
}
