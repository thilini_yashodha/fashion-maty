package com.example.fashiondesigner.measurements;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.VideoView;

import com.example.fashiondesigner.R;

import java.io.IOException;
import java.util.ArrayList;

public class PlayVideo extends AppCompatActivity {

    ArrayList<String> arrayList;
    ArrayAdapter adupter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_video);

        final VideoView videoView=findViewById(R.id.videoView);
        ListView listView=findViewById(R.id.list);

        arrayList=new ArrayList<>();
        arrayList.add("WOMEN TOP MEASUREMENTS");
        arrayList.add("MEN TOP MEASUREMENTS");
        arrayList.add("WOMEN BOTTOM MEASUREMENTS");
        arrayList.add("MEN BOTTOM MEASUREMENTS");

        adupter= new ArrayAdapter(this,R.layout.raw_item,arrayList);
        listView.setAdapter(adupter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i)
                {
                    case 0:
                        videoView.setVideoURI(Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.video1));
                        break;

                    case 1:
                        videoView.setVideoURI(Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.video2));
                        break;

                    case 2:
                        videoView.setVideoURI(Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.video3));
                        break;

                    case 3:
                        videoView.setVideoURI(Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.video4));
                        break;

                }
                videoView.setMediaController(new MediaController(PlayVideo.this));
                videoView.requestFocus();
                videoView.start();
            }
        });


    }

}