package com.example.fashiondesigner.measurements;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.fashiondesigner.R;
import com.example.fashiondesigner.viewItemClick;

public class tMeasurement extends AppCompatActivity {

    String click,clickItemName,user_ID,subClick,pushID;
    RadioGroup radioGroup;
    RadioButton radioButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_t_measurement);


        Intent intent = getIntent();
        click = intent.getStringExtra("clickCategory").toLowerCase();
        subClick = intent.getStringExtra("subClickCategory").toLowerCase();
        clickItemName=intent.getStringExtra("clickItemName").toLowerCase();
        user_ID=intent.getStringExtra("user_ID");
        pushID=intent.getStringExtra("pushID");

        Button conti=findViewById(R.id.conti);

        radioGroup=findViewById(R.id.radioGroup);

        conti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int radioID=radioGroup.getCheckedRadioButtonId();
                radioButton=findViewById(radioID);

                if(radioButton.getText().equals("GENTLEMAN"))
                {
                    Intent intent = new Intent(tMeasurement.this , gentMeasuremet.class);
                    intent.putExtra("clickCategory",click);
                    intent.putExtra("subClickCategory",subClick);
                    intent.putExtra("clickItemName",clickItemName);
                    intent.putExtra("pushID",pushID);
                    startActivity(intent);
                }
                else if(radioButton.getText().equals("LADIES"))
                {
                    Intent intent = new Intent(tMeasurement.this , ladiesMeasurement.class);
                    intent.putExtra("clickCategory",click);
                    intent.putExtra("subClickCategory",subClick);
                    intent.putExtra("clickItemName",clickItemName);
                    intent.putExtra("pushID",pushID);
                    startActivity(intent);
                }

            }
        });


    }


    public void  checkButton(View V)
    {
        int radioID=radioGroup.getCheckedRadioButtonId();
        radioButton=findViewById(radioID);

        Toast.makeText(this,radioButton.getText()+ " SELLECTED",Toast.LENGTH_SHORT).show();
    }
}
