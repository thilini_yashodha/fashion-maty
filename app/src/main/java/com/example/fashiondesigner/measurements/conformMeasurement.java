package com.example.fashiondesigner.measurements;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fashiondesigner.Adapter.Model.VerticalModel;
import com.example.fashiondesigner.R;
import com.example.fashiondesigner.Tailor2;
import com.example.fashiondesigner.chat.meassageActivity;
import com.example.fashiondesigner.verticalHorizontalList;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class conformMeasurement extends AppCompatActivity {

    String click,clickItemName,subClick,pushID;
   // String gender;
    String userID;
    DatabaseReference mdata;

    RadioGroup radioGroup;
    RadioButton radioButton;

    String build="";
    TextView showList;
    EditText getReco;
    String add,a="";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conform_measurement);

        Intent intent = getIntent();
        click = intent.getStringExtra("clickCategory").toLowerCase();
        subClick = intent.getStringExtra("subClickCategory").toLowerCase();
        clickItemName=intent.getStringExtra("clickItemName").toLowerCase();
        pushID=intent.getStringExtra("pushID");
        userID=intent.getStringExtra("userID");
        String hgsdh="hdjnjv";


        Button conti=findViewById(R.id.conti);
        radioGroup=findViewById(R.id.radioGroup);
        showList = findViewById(R.id.selectedData);
        getReco=findViewById(R.id.additional);
        TextView title=findViewById(R.id.clickItem);
        Button change=findViewById(R.id.change);
        final EditText heighPortion=findViewById(R.id.heightPortion);
        final EditText widthPortion=findViewById(R.id.widthPortion);


        title.setText(clickItemName.toUpperCase());

        mdata= FirebaseDatabase.getInstance().getReference().child(userID);



        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // mdata.child("measurements").child(pushID).child("additional").setValue(getReco.getText().toString());

                mdata.child("measurements").child(pushID).child("gender").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String b= dataSnapshot.getValue().toString();
                        if(b.equals("0"))
                        {
                            Intent intent = new Intent(conformMeasurement.this , ladiesMeasurement.class);
                            intent.putExtra("clickCategory",click);
                            intent.putExtra("subClickCategory",subClick);
                            intent.putExtra("clickItemName",clickItemName);
                            intent.putExtra("pushID",pushID);
                            startActivity(intent);
                        }
                        else if(b.equals("1"))
                        {
                            Intent intent = new Intent(conformMeasurement.this , gentMeasuremet.class);
                            intent.putExtra("clickCategory",click);
                            intent.putExtra("subClickCategory",subClick);
                            intent.putExtra("clickItemName",clickItemName);
                            intent.putExtra("pushID",pushID);
                            startActivity(intent);
                        }


                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });



            }
        });


        mdata.child("measurements").child(pushID).child("additional").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue()!=null)
                {
                    a= dataSnapshot.getValue().toString();
                    getReco.setText(a);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });




       mdata.child("measurements").child(pushID).child("data").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot ds:dataSnapshot.getChildren())
                {
                    String data=ds.getValue().toString();
                    build=build+ data+ "\n\n";

                }

                showList.setText(build);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });








        conti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int radioID=radioGroup.getCheckedRadioButtonId();
                radioButton=findViewById(radioID);
                add=getReco.getText().toString();

                if(radioButton.getText().equals("YES"))
                {
                    if(!(add.equals(a)) )
                    {
                        mdata.child("measurements").child(pushID).child("additional").setValue(add);
                    }

                    mdata.child("measurements").child(pushID).child("help").setValue("1");
                    alert();

                }
                else if(radioButton.getText().equals("NO") )
                {
                    if(!(add.equals(a)))
                    {
                        mdata.child("measurements").child(pushID).child("additional").setValue(add);
                    }
                    mdata.child("measurements").child(pushID).child("help").setValue("0");

                    alert();
                }



            }
        });


        final ArrayList<String> category=new ArrayList<>();
       DatabaseReference ref=FirebaseDatabase.getInstance().getReference().child("selectCategory");
       ref.addListenerForSingleValueEvent(new ValueEventListener() {
           @Override
           public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

               for(DataSnapshot d:dataSnapshot.getChildren())
               {
                   String data=d.getValue().toString();
                   category.add(data);
               }
               if(!(category.contains(click)))
               {
                   heighPortion.setHint("give persentage of dress length to human height(100%)");
                   String h=heighPortion.getText().toString();

                   widthPortion.setHint("give persentage of dress width to human width(100%)");
                   String w=widthPortion.getText().toString();

                   mdata.child("measurements").child(pushID).child("portion").child("height").setValue(h);
                   mdata.child("measurements").child(pushID).child("portion").child("width").setValue(w);
               }
               else
               {
                   heighPortion.setVisibility(View.GONE);
                  // heighPortion.setFocusable(false);

                   widthPortion.setVisibility(View.GONE);
                   //widthPortion.setFocusable(false);
               }
           }

           @Override
           public void onCancelled(@NonNull DatabaseError databaseError) {

           }
       });



    }

    public void  checkButton(View V)
    {
        int radioID=radioGroup.getCheckedRadioButtonId();
        radioButton=findViewById(radioID);

        Toast.makeText(this,radioButton.getText()+ " SELLECTED",Toast.LENGTH_SHORT).show();
    }

    private void alert() {

        View view1=(LayoutInflater.from(conformMeasurement.this)).inflate(R.layout.success_box,null);
        AlertDialog.Builder alertdialog=new AlertDialog.Builder(conformMeasurement.this);
        alertdialog.setView(view1);

        final TextView getData=view1.findViewById(R.id.reenterbn);
        getData.setText("Progress is completed!");


        alertdialog.setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent intent = new Intent(conformMeasurement.this, Tailor2.class);
                        startActivity(intent);
                        finish();
                        onBackPressed();


                    }

                })
                .setNegativeButton("CANCLE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });





        alertdialog.create();
        alertdialog.show();

    }


}
