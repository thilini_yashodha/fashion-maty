package com.example.fashiondesigner.measurements;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.fashiondesigner.R;
import com.example.fashiondesigner.fabric;
import com.example.fashiondesigner.order.payments;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

public class placeOrder extends AppCompatActivity {

    String category,subCategory,itemName,imageUrl,userID,fabricKey,colourKey,fullDRessPushID,orderPushID,customerUerID;
    String strBuilt="",colour="",height,mPrice="0";
    int k=0;
    double cost=1.0,h=0.0;
    Button placeOrderBtn;
    EditText getAdd;

    ImageView getImage;
    private int GALLERY = 1, CAMERA = 2;
    public Uri filePath;
    StorageReference mstorage;
    DatabaseReference corder;
    double measureCost=0.0;

    String image,addFire;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_order);

        Intent intent = getIntent();
        category = intent.getStringExtra("category").toLowerCase();
        subCategory = intent.getStringExtra("subCategory").toLowerCase();
        itemName = intent.getStringExtra("itemName").toLowerCase();
        imageUrl = intent.getStringExtra("imageUrl");
        userID = intent.getStringExtra("userID");
        fabricKey = intent.getStringExtra("fabricKey");
        colourKey = intent.getStringExtra("colourKey");
        fullDRessPushID=intent.getStringExtra("fulldressPushID");
        orderPushID=intent.getStringExtra("orderPushID");
        customerUerID=intent.getStringExtra("customerUserID");

        mstorage= FirebaseStorage.getInstance().getReference();


        final TextView shopName,tailorName,email,tele,address,city;
        shopName=findViewById(R.id.shopName);
        tailorName=findViewById(R.id.tailorName);
        email=findViewById(R.id.email);
        tele=findViewById(R.id.tele);
        address=findViewById(R.id.address);
        city=findViewById(R.id.city);

        final TextView categoryTv,subcategoryTv,itemNameTv,colourNameTv;
        final ImageView itemImage,fabricImage;

        categoryTv=findViewById(R.id.category);
        subcategoryTv=findViewById(R.id.subCategory);
        itemNameTv=findViewById(R.id.itemName);
        colourNameTv=findViewById(R.id.colourName);

        itemImage=findViewById(R.id.itemImage);
        fabricImage=findViewById(R.id.fabrciImage);

        categoryTv.setText(category.toUpperCase());
        subcategoryTv.setText(subCategory.toUpperCase());
        itemNameTv.setText(itemName.toUpperCase());

        final TextView yorMData=findViewById(R.id.mData);

        final TextView fabricPrice,measurementPrice,total;
        fabricPrice=findViewById(R.id.fabricPrice);
        measurementPrice=findViewById(R.id.tailorFee);
        total=findViewById(R.id.totalaCharge);

        placeOrderBtn=findViewById(R.id.placeOrder);
        getAdd=findViewById(R.id.getAdd);
        ImageButton getImageBtn=findViewById(R.id.imageButton);
        getImage=findViewById(R.id.addImage);


        getImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
            }
        });


      DatabaseReference allItem=FirebaseDatabase.getInstance().getReference().child("allItems").child(fullDRessPushID);
      allItem.addListenerForSingleValueEvent(new ValueEventListener() {
          @Override
          public void onDataChange(@NonNull DataSnapshot dataSnapshot3) {

              image=dataSnapshot3.child("image").getValue().toString();
              Glide.with(itemImage.getContext())
                      .load(image)
                      .into(itemImage);

            //  mPrice=dataSnapshot3.child("priceRange").getValue().toString();

          }

          @Override
          public void onCancelled(@NonNull DatabaseError databaseError) {

          }
      });





        DatabaseReference colouData=FirebaseDatabase.getInstance().getReference().child(userID).child("fabric").child("colours");
        colouData.child(fabricKey).child(colourKey).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot2) {
                String image=dataSnapshot2.child("imageUrl").getValue().toString();
                String name=dataSnapshot2.child("name").getValue().toString();

                Glide.with(fabricImage.getContext())
                        .load(image)
                        .into(fabricImage);

                colour=name.toUpperCase();


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });




        final DatabaseReference tailordata= FirebaseDatabase.getInstance().getReference().child(userID).child("tailorData");
        tailordata.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String shopNamet=dataSnapshot.child("businessName").getValue().toString();
                String tailorNamet=dataSnapshot.child("tailorName").getValue().toString();
                String emailt=dataSnapshot.child("email").getValue().toString();
                String telet=dataSnapshot.child("tele").getValue().toString();
                String addresst=dataSnapshot.child("address").getValue().toString();
                String countryt=dataSnapshot.child("country").getValue().toString();
                String cityt=dataSnapshot.child("city").getValue().toString();


                shopName.setText(shopNamet.toUpperCase());
                tailorName.setText("BY "+tailorNamet.toUpperCase());
                email.setText(emailt);
                tele.setText(telet);
                address.setText(addresst);
                city.setText(countryt+ " - "+ cityt );

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        corder=FirebaseDatabase.getInstance().getReference().child(customerUerID).child("order").child(orderPushID);

        final DatabaseReference measure=FirebaseDatabase.getInstance().getReference().child(userID).child("measurements").child(fullDRessPushID);

        final ArrayList<String> nameArr=new ArrayList<>();
        final ArrayList<String> valArr=new ArrayList<>();

        measure.child("data").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot ds:dataSnapshot.getChildren())
                {
                    final String name=ds.getValue().toString();
                    nameArr.add(name);
                   // Log.d("name",name);

                }

                corder.child("mData").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshotx) {

                            for(DataSnapshot dx: dataSnapshotx.getChildren())
                            {
                                String val=dx.getValue().toString();
                                valArr.add(val);

                                Log.d("val",val);

                                strBuilt=strBuilt+ nameArr.get(k)+" : "+val+"cm\n\n";
                                k++;

                            }

                        yorMData.setText(strBuilt);

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });



            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });





        DatabaseReference fabric=FirebaseDatabase.getInstance().getReference().child(userID).child("fabric").child("fabricDetails");
        fabric.child(fabricKey).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final String price=dataSnapshot.child("price").getValue().toString();
                String fabeicName=dataSnapshot.child("name").getValue().toString();
                String patter=dataSnapshot.child("pattern").getValue().toString();

                addFire=fabeicName.toUpperCase() +"\n\n"+patter.toUpperCase()+"\n\n"+colour;

                colourNameTv.setText(fabeicName.toUpperCase() +"\n\n"+patter.toUpperCase()+"\n\n"+colour);
                final double p=Double.parseDouble(price);
                Log.d("p",String.valueOf(p));

                corder.child("height").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        height=dataSnapshot.getValue().toString();
                        h=Double.parseDouble(height);

                        if((category.equals("frock"))|| (category.equals("coat")))
                        {
                            Log.d("h",String.valueOf(h));

                            cost= p*h;
                            Log.d("cost",String.valueOf(cost));

                        }
                        else if(category.equals("blouse")||category.equals("jeans")||category.equals("skirt")||category.equals("shirt")||category.equals("tshirt")||category.equals("trousers"))
                        {
                            // double h=Double.parseDouble(height);

                            cost= (p*h)/2;
                        }
                        else
                        {
                            measure.child("portion").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                    if((dataSnapshot.child("height").getValue()!=null) && (dataSnapshot.child("width").getValue()!=null))
                                    {
                                        String  hx=dataSnapshot.child("height").getValue().toString();
                                        String wx=dataSnapshot.child("width").getValue().toString();

                                        int wi=Integer.parseInt(wx);
                                        int hi=Integer.parseInt(hx);

                                        cost= p*wi*hi*0.0001*h;
                                    }


                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });

                        }

                        final double roundCost = Math.round(cost * 100.0) / 100.0;

                        fabricPrice.setText("Cost for fabric : $ " + roundCost );

                        DatabaseReference allItem=FirebaseDatabase.getInstance().getReference().child("allItems").child(fullDRessPushID);
                        allItem.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot3) {

                                if((dataSnapshot3.child("priceRange").getValue())!=null)
                                {
                                    mPrice=dataSnapshot3.child("priceRange").getValue().toString();
                                    measureCost=Double.parseDouble(mPrice);
                                }
                                else
                                {
                                    measureCost=0.0;
                                }

                                corder.child("deliverAddress").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshotdis)
                                    {

                                        String dis=dataSnapshotdis.child("distance").getValue().toString();
                                        Log.d("distance",dis);
                                        // Double distance=Double.parseDouble(dis);
                                        double distance=Double.parseDouble(dis);
                                        int count=0;

                                        while (distance>0)
                                        {
                                            distance=distance-3000;
                                            count++;
                                        }

                                        Log.d("count",String.valueOf(count));
                                        Double meCost=measureCost + count*0.5;

                                        double roundMeCost = Math.round(meCost * 100.0) / 100.0;

                                        measurementPrice.setText("Measurement + delivery fee : $ " + roundMeCost);

                                        Double sum=roundCost+roundMeCost;
                                        final double roundSum = Math.round(sum * 100.0) / 100.0;

                                        total.setText("Total cost : $ " + roundSum);

                                        placeOrderBtn.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {



                                                upload();

                                                corder.child("price").setValue(String.valueOf(roundSum));


                                            }
                                        });

                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });



                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });




                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });






            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }

    private void showPictureDialog() {

        androidx.appcompat.app.AlertDialog.Builder pictureDialog = new androidx.appcompat.app.AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                dispathTakePictureIntent();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);


    }


    private  void dispathTakePictureIntent()
    {
        Intent takePictureIntent= new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(takePictureIntent.resolveActivity(getPackageManager())!= null) {
            File photoFile = null;
            try {
                {
                    photoFile = createImageFile();
                }
            } catch (IOException ex) {

            }

            if (photoFile != null) {
                Uri photoUri = FileProvider.getUriForFile(this, "com.example.android.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(takePictureIntent, CAMERA);
            }
        }
    }

    String currentPhotoPath;

    private  File createImageFile() throws IOException
    {
        String timeStamp=new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName="JPEG_"+timeStamp+"_";
        File storageDir= Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image=File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        currentPhotoPath=image.getAbsolutePath();
        return  image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_CANCELED) {
            return;
        }

        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                filePath = contentURI;
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    Toast.makeText(placeOrder.this, "Image Saved!", Toast.LENGTH_SHORT).show();

                    getImage.setImageBitmap(bitmap);


                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(placeOrder.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {

            File f=new File(currentPhotoPath);
            getImage.setImageURI(Uri.fromFile(f));
            filePath=Uri.fromFile(f);

            Intent mediaScanIntent=new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri=Uri.fromFile(f);
            mediaScanIntent.setData(contentUri);
            this.sendBroadcast(mediaScanIntent);

        }
    }


    private void upload() {

        if (filePath != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Wait...");
            progressDialog.show();

            final StorageReference ref = mstorage.child("Order/" + UUID.randomUUID().toString());
            ref.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    String savedUri = uri.toString();

                                    String text=getAdd.getText().toString();
                                    HashMap <String,String> addMap=new HashMap<>();

                                    addMap.put("text",text);
                                    addMap.put("imageUrl",savedUri);

                                     corder.child("addAdditional").setValue(addMap);



                                     HashMap<String,String> orderData=new HashMap<>();

                                     orderData.put("category",category);
                                    orderData.put("subCategory",subCategory);
                                    orderData.put("itemName",itemName);
                                    orderData.put("fullDreassPushID",fullDRessPushID);
                                    orderData.put("fabrciKey",fabricKey);
                                    orderData.put("colourKey",colourKey);
                                    orderData.put("fullImageUrl",image);
                                    orderData.put("addFire",addFire);


                                    corder.child("orderItemData").setValue(orderData);


                                    Intent intent = new Intent(placeOrder.this, payments.class);
                                    intent.putExtra("customerUserID",customerUerID);
                                    intent.putExtra("userID",userID);
                                    intent.putExtra("orderPushID",orderPushID);
                                    startActivity(intent);



                                }
                            });
                            progressDialog.dismiss();

                            Toast.makeText(placeOrder.this, "Succesful", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(placeOrder.this, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                    .getTotalByteCount());


                            progressDialog.setMessage("Successful " );
                        }
                    });
        }
        else
        {
            Toast.makeText(placeOrder.this, "no image!", Toast.LENGTH_SHORT).show();
            String text=getAdd.getText().toString();
            HashMap <String,String> addMap=new HashMap<>();

            addMap.put("text",text);

            corder.child("addAdditional").setValue(addMap);


            HashMap<String,String> orderData=new HashMap<>();

            orderData.put("category",category);
            orderData.put("subCategory",subCategory);
            orderData.put("itemName",itemName);
            orderData.put("fullDreassPushID",fullDRessPushID);
            orderData.put("fabrciKey",fabricKey);
            orderData.put("colourKey",colourKey);
            orderData.put("fullImageUrl",image);
            orderData.put("addFire",addFire);


            corder.child("orderItemData").setValue(orderData);


            Intent intent = new Intent(placeOrder.this, payments.class);
            intent.putExtra("customerUserID",customerUerID);
            intent.putExtra("orderPushID",orderPushID);
            intent.putExtra("userID",userID);
            startActivity(intent);
        }


    }

}
