package com.example.fashiondesigner;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.example.fashiondesigner.customerSearch.searchTailor;
import com.example.fashiondesigner.customerSearch.showRatings;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import static android.app.Activity.RESULT_OK;
import static androidx.core.content.ContextCompat.checkSelfPermission;

public class    TailorProfileFragment extends Fragment {

    DatabaseReference mdata;
    StorageReference mStorage;
    FirebaseAuth firebaseAuth;
    String user_id;
    public Uri iuri;

    TextView businessName,tailorName, tele, email, country, city, discription, add;
    Button editbn,editbn1,editbn2,editbn3,editbn4,editbn5,editbn6,editbn7,button;

    EditText getData;
    ImageView imageView;
    String check="0";
    String imageUrl;
    private int GALLERY = 1, CAMERA = 2;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_tailor_profile,null);

        mdata= FirebaseDatabase.getInstance().getReference();
        mStorage= FirebaseStorage.getInstance().getReference();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        user_id = user.getUid();

        businessName=view.findViewById(R.id.etbname);
        editbn=view.findViewById(R.id.editbn);
        editbn1=view.findViewById(R.id.editbn1);
        editbn2=view.findViewById(R.id.editbn2);
        editbn3=view.findViewById(R.id.editbn3);
        editbn4=view.findViewById(R.id.editbn4);
        editbn5=view.findViewById(R.id.editbn5);
        editbn6=view.findViewById(R.id.editbn6);
        editbn7=view.findViewById(R.id.editbn7);
        imageView=view.findViewById(R.id.imageView2);
        tailorName=view.findViewById(R.id.ettname);
        tele=view.findViewById(R.id.evteleNO);
        email=view.findViewById(R.id.evemail);
        country=view.findViewById(R.id.evcountry);
        city=view.findViewById(R.id.evcity);
        discription=view.findViewById(R.id.evdiscript);
        add=view.findViewById(R.id.etadd);
        button=view.findViewById(R.id.btn);

        Button review=view.findViewById(R.id.review);
        final RatingBar bar=view.findViewById(R.id.ratingStar2);

        review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), showRatings.class);
                intent.putExtra("userID",user_id);
                startActivity(intent);
            }
        });

        final DatabaseReference rates=FirebaseDatabase.getInstance().getReference().child(user_id).child("reviews").child("reviewData").child("totalReviews");
        rates.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshotr) {
                if (dataSnapshotr.exists())
                {
                    String amount=dataSnapshotr.getValue().toString();
                    bar.setRating(Float.valueOf(amount));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();

            }
        });

        display();

        editbn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alert(0);
            }
        });

        editbn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alert(1);
            }
        });

        editbn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alert(2);
            }
        });

        editbn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alert(3);
            }
        });

        editbn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alert(4);
            }
        });

        editbn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alert(5);
            }
        });

        editbn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alert(6);
            }
        });

        editbn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alert(7);
            }
        });



       Button logout=view.findViewById(R.id.logOut);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Intent log = new Intent(getActivity(), MainActivity.class);
                startActivity(log);
                getActivity().onBackPressed();
               // getActivity().finish();
            }
        });



        return  view;
    }

    private void display() {

        mdata.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String bn=dataSnapshot.child(user_id).child("tailorData").child("businessName").getValue().toString();
                businessName.setText(bn);
                String tn=dataSnapshot.child(user_id).child("tailorData").child("tailorName").getValue().toString();
                tailorName.setText(tn);
                String telNO=dataSnapshot.child(user_id).child("tailorData").child("tele").getValue().toString();
                tele.setText(telNO);
                String e_mail=dataSnapshot.child(user_id).child("tailorData").child("email").getValue().toString();
                email.setText(e_mail);
                String cnty=dataSnapshot.child(user_id).child("tailorData").child("country").getValue().toString();
                country.setText(cnty);
                String cty=dataSnapshot.child(user_id).child("tailorData").child("city").getValue().toString();
                city.setText(cty);
                String dis=dataSnapshot.child(user_id).child("tailorData").child("discription").getValue().toString();
                discription.setText(dis);
                String address=dataSnapshot.child(user_id).child("tailorData").child("address").getValue().toString();
                add.setText(address);


                imageUrl=dataSnapshot.child(user_id).child("tailorData").child("imageuri").getValue().toString();
                Glide.with(getActivity()).load(imageUrl).into(imageView);
               // Picasso.get().load(imageUrl).into(imageView);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                Toast.makeText(getActivity(),"ERROR!",Toast.LENGTH_SHORT).show();
            }
        });





    }



    public void alert(final int x)
    {
        View view=(LayoutInflater.from(getActivity())).inflate(R.layout.tailordalogbox,null);
        AlertDialog.Builder alertdialog=new AlertDialog.Builder(getActivity());
        alertdialog.setView(view);


        getData=view.findViewById(R.id.reenterbn);


        alertdialog.setCancelable(false)
                .setPositiveButton("EDIT", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if(x==0)
                        {
                            String s=getData.getText().toString();
                            businessName.setText(s);
                            mdata.child(user_id).child("tailorData").child("businessName").setValue(s);

                        }
                        else if(x==1)
                        {

                            String s1=getData.getText().toString();
                            tailorName.setText(s1);
                            mdata.child(user_id).child("tailorData").child("tailorName").setValue(s1);
                        }

                        else if(x==2)
                        {

                            String s2=getData.getText().toString();
                            tele.setText(s2);
                            mdata.child(user_id).child("tailorData").child("tele").setValue(s2);
                        }
                        else if(x==3)
                        {

                            String s3=getData.getText().toString();
                            email.setText(s3);
                            mdata.child(user_id).child("tailorData").child("email").setValue(s3);
                        }
                        else if(x==4)
                        {

                            String s4=getData.getText().toString();
                            country.setText(s4);
                            mdata.child(user_id).child("tailorData").child("country").setValue(s4);
                        }
                        else if(x==5)
                        {

                            String s5=getData.getText().toString();
                            city.setText(s5);
                            mdata.child(user_id).child("tailorData").child("city").setValue(s5);
                        }
                        else if(x==6)
                        {

                            String s6=getData.getText().toString();
                            discription.setText(s6);
                            mdata.child(user_id).child("tailorData").child("discription").setValue(s6);
                        }
                        else if(x==7)
                        {

                            String s7=getData.getText().toString();
                            add.setText(s7);
                            mdata.child(user_id).child("tailorData").child("address").setValue(s7);
                        }





                    }
                })
                .setNegativeButton("CANCLE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });



        alertdialog.create();
        alertdialog.show();
    }


    private void showPictureDialog() {

        androidx.appcompat.app.AlertDialog.Builder pictureDialog = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                dispathTakePictureIntent();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);


    }


    private  void dispathTakePictureIntent()
    {
        Intent takePictureIntent= new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(takePictureIntent.resolveActivity(getActivity().getPackageManager())!= null) {
            File photoFile = null;
            try {
                {
                    photoFile = createImageFile();
                }
            } catch (IOException ex) {

            }

            if (photoFile != null) {
                Uri photoUri = FileProvider.getUriForFile(getActivity(), "com.example.android.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(takePictureIntent, CAMERA);
            }
        }
    }

    String currentPhotoPath;

    private  File createImageFile() throws IOException
    {
        String timeStamp=new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName="JPEG_"+timeStamp+"_";
        File storageDir= Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image=File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        currentPhotoPath=image.getAbsolutePath();
        return  image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == getActivity().RESULT_CANCELED) {
            return;
        }

        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                iuri = contentURI;
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), contentURI);
                    Toast.makeText(getActivity(), "Image Saved!", Toast.LENGTH_SHORT).show();

                    imageView.setImageBitmap(bitmap);
                    upload();


                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {

            File f=new File(currentPhotoPath);
            imageView.setImageURI(Uri.fromFile(f));
            iuri=Uri.fromFile(f);

            Intent mediaScanIntent=new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri=Uri.fromFile(f);
            mediaScanIntent.setData(contentUri);
            getActivity().sendBroadcast(mediaScanIntent);

            upload();

        }
    }





    public void upload()
    {
        if (iuri != null) {
            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            final StorageReference ref =mStorage.child("tailor_profile").child(user_id);
            ref.putFile(iuri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    String duri = uri.toString();

                                    mdata.child(user_id).child("tailorData").child("imageuri").setValue(duri);


                                }
                            });
                            progressDialog.dismiss();

                            Toast.makeText(getActivity(), "Uploaded", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setMessage("Uploaded " + (int) progress + "%");
                        }
                    });
        }

        else
        {
            Toast.makeText(getActivity(), "Unccessful!", Toast.LENGTH_SHORT).show();
        }
    }


}

