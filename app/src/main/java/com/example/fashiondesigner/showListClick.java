package com.example.fashiondesigner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.fashiondesigner.measurements.conformMeasurement;
import com.example.fashiondesigner.measurements.tMeasurement;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class showListClick extends AppCompatActivity {

    TextView priceText;
    String click,clickItemName,subClick,name,price,imageUrl,pushID;

    DatabaseReference mdata;
    String user_ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_list_click);

        Intent intent = getIntent();
        click = intent.getStringExtra("clickCategory").toLowerCase();
        subClick = intent.getStringExtra("subClickCategory").toLowerCase();
        clickItemName=intent.getStringExtra("clickItemName").toLowerCase();
        pushID=intent.getStringExtra("pushID");
        price=intent.getStringExtra("price");
        imageUrl=intent.getStringExtra("image");
        name=intent.getStringExtra("title");

        FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
        user_ID=user.getUid();
        mdata= FirebaseDatabase.getInstance().getReference().child(user_ID).child("item").child(click).child(subClick).child(clickItemName).child("fullDress");




        TextView title=findViewById(R.id.title);
        TextView upper=findViewById(R.id.clickItem);
        ImageView image=findViewById(R.id.image);
        priceText=findViewById(R.id.price);
        Button editPrice=findViewById(R.id.editPrice);
        Button colours=findViewById(R.id.viewDress);
        Button measurements=findViewById(R.id.measurements);

        upper.setText(clickItemName.toUpperCase());
        title.setText(name.toUpperCase());
        Picasso.get().load(imageUrl).into(image);
        priceText.setText("$ " +price);

        editPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert();
            }
        });


        measurements.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatabaseReference mdataM=FirebaseDatabase.getInstance().getReference().child(user_ID).child("measurements").child(pushID).child("state");
                mdataM.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String state=dataSnapshot.getValue().toString();

                        if(state.equals("0"))
                        {
                            Intent intent = new Intent(showListClick.this , tMeasurement.class);
                            intent.putExtra("clickCategory",click);
                            intent.putExtra("subClickCategory",subClick);
                            intent.putExtra("clickItemName",clickItemName);
                            intent.putExtra("pushID",pushID);
                            intent.putExtra("userID",user_ID);
                            startActivity(intent);
                        }
                        else if(state.equals("1"))
                        {
                            Intent intent = new Intent(showListClick.this , conformMeasurement.class);
                            intent.putExtra("clickCategory",click);
                            intent.putExtra("subClickCategory",subClick);
                            intent.putExtra("clickItemName",clickItemName);
                            intent.putExtra("pushID",pushID);
                            intent.putExtra("userID",user_ID);
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


            }
        });



        colours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(showListClick.this ,fabric.class);
                intent.putExtra("clickCategory",click);
                intent.putExtra("subClickCategory",subClick);
                intent.putExtra("clickItemName",clickItemName);
                intent.putExtra("pushID",pushID);
                startActivity(intent);
            }
        });

    }

    private void alert() {

        View view=(LayoutInflater.from(showListClick.this)).inflate(R.layout.tailordalogbox,null);
        AlertDialog.Builder alertdialog=new AlertDialog.Builder(showListClick.this);
        alertdialog.setView(view);


        final EditText getData=view.findViewById(R.id.reenterbn);

        alertdialog.setCancelable(false)
                .setPositiveButton("EDIT", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String s=getData.getText().toString();
                        priceText.setText("$ "+ s);
                        mdata.child(pushID).setValue(s);


                    }
                })
                .setNegativeButton("CANCLE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });



        alertdialog.create();
        alertdialog.show();
    }
}
