package com.example.fashiondesigner.order;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.fashiondesigner.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class cSuggestViewOrderClick extends AppCompatActivity {

    String tailoruserID,cUserID,orderKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_c_suggest_view_order_click);

        Intent intent = getIntent();
        tailoruserID = intent.getStringExtra("tailoruserID");
        orderKey=intent.getStringExtra("orderKey");

        FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
        cUserID=user.getUid();

        final TextView shopName,tailorName,email,tele,address,city;
        shopName=findViewById(R.id.shopName);
        tailorName=findViewById(R.id.tailorName);
        email=findViewById(R.id.email);
        tele=findViewById(R.id.tele);
        address=findViewById(R.id.address);
        city=findViewById(R.id.city);

        final TextView orderID=findViewById(R.id.orderID);
        final TextView textViewFront=findViewById(R.id.frontText);
        final TextView textViewBack=findViewById(R.id.backText);
        final TextView addText=findViewById(R.id.textData);
        final TextView colourName=findViewById(R.id.colourName);

        final ImageView imageFront=findViewById(R.id.front);
        final ImageView imageBack=findViewById(R.id.back);
        final ImageView fabricImage=findViewById(R.id.fabrciImage);

        final TextView track=findViewById(R.id.track);

        final Button placeOrder=findViewById(R.id.placeOrder);



        DatabaseReference tailordata= FirebaseDatabase.getInstance().getReference().child(tailoruserID).child("tailorData");
        tailordata.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String shopNamet=dataSnapshot.child("businessName").getValue().toString();
                String tailorNamet=dataSnapshot.child("tailorName").getValue().toString();
                String emailt=dataSnapshot.child("email").getValue().toString();
                String telet=dataSnapshot.child("tele").getValue().toString();
                String addresst=dataSnapshot.child("address").getValue().toString();
                String countryt=dataSnapshot.child("country").getValue().toString();
                String cityt=dataSnapshot.child("city").getValue().toString();


                shopName.setText(shopNamet.toUpperCase());
                tailorName.setText("BY "+tailorNamet.toUpperCase());
                email.setText(emailt);
                tele.setText(telet);
                address.setText(addresst);
                city.setText(countryt+ " - "+ cityt );

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        final DatabaseReference orderData=FirebaseDatabase.getInstance().getReference().child(cUserID).child("order").child(orderKey);
        orderData.child("orderItemData").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshotOID) {

                if(dataSnapshotOID.exists())
                {
                    if(dataSnapshotOID.child("frontImage").exists())
                    {
                        String imagef=dataSnapshotOID.child("frontImage").getValue().toString();
                        Glide.with(imageFront.getContext())
                                .load(imagef)
                                .into(imageFront);


                    }
                    else
                    {
                        imageFront.setVisibility(View.GONE);
                        textViewFront.setVisibility(View.GONE);
                    }

                    if(dataSnapshotOID.child("backImage").exists())
                    {
                        String imageb=dataSnapshotOID.child("backImage").getValue().toString();
                        Glide.with(imageBack.getContext())
                                .load(imageb)
                                .into(imageBack);
                    }
                    else
                    {
                        imageBack.setVisibility(View.GONE);
                        textViewBack.setVisibility(View.GONE);
                    }

                    if(dataSnapshotOID.child("text").exists())
                    {
                        String text=dataSnapshotOID.child("text").getValue().toString();
                        addText.setText(text);

                    }
                    else
                    {
                        addText.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        orderData.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshotOD) {

                if(dataSnapshotOD.exists())
                {
                    String fabricKey=dataSnapshotOD.child("fabricData").child("fabrciKey").getValue().toString();
                    String colourKey=dataSnapshotOD.child("fabricData").child("colourKey").getValue().toString();

                    String colour=dataSnapshotOD.child("fabricData").child("addFire").getValue().toString();

                    String orderTime=dataSnapshotOD.child("orderTime").getValue().toString();


                    orderID.setText("Oder ID : "+ orderTime);

                    colourName.setText(colour);


                    final DatabaseReference itemData=FirebaseDatabase.getInstance().getReference().child(tailoruserID);
                    itemData.child("fabric").child("colours").child(fabricKey).child(colourKey).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshotCI) {

                            String colourImage=dataSnapshotCI.child("imageUrl").getValue().toString();

                            Glide.with(fabricImage.getContext())
                                    .load(colourImage)
                                    .into(fabricImage);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });





                    orderData.child("orderState").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshotOS) {
                            if(dataSnapshotOS.exists())
                            {
                                String stateCheck=dataSnapshotOS.getValue().toString();

                                if(stateCheck.equals("2"))
                                {

                                    orderData.child("price").addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshotP) {

                                            String price=dataSnapshotP.getValue().toString();

                                            track.setText("PAID $ "+price);


                                            placeOrder.setVisibility(View.GONE);
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });
                                }
                                else if(stateCheck.equals("1"))
                                {
                                    orderData.child("deliver").addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshotDL) {

                                            if(dataSnapshotDL.exists())
                                            {

                                                placeOrder.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {

                                                        orderData.child("orderState").setValue("2");

                                                        itemData.child("orders").child(orderKey).child("orderState").setValue("2");

                                                        orderData.child("price").addListenerForSingleValueEvent(new ValueEventListener() {
                                                            @Override
                                                            public void onDataChange(@NonNull DataSnapshot dataSnapshotP) {

                                                                String price=dataSnapshotP.getValue().toString();

                                                                track.setText("PAID $ "+price);


                                                                placeOrder.setVisibility(View.GONE);
                                                            }

                                                            @Override
                                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                                            }
                                                        });


                                                    }
                                                });


                                                String deliverD=dataSnapshotDL.getValue().toString();

                                                if(deliverD.equals("1"))
                                                {
                                                    track.setText("ON DELIVERING");
                                                }
                                            }
                                            else
                                            {
                                                track.setText("ON TAILERING");
                                                placeOrder.setVisibility(View.GONE);


                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });



                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });












                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }
}
