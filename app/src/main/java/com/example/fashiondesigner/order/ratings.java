package com.example.fashiondesigner.order;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fashiondesigner.R;
import com.example.fashiondesigner.measurements.placeOrder;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

public class ratings extends AppCompatActivity {

    String tailorID,cUserID,orderKey;
    ImageView commentImage;
    private int GALLERY = 1, CAMERA = 2;
    public Uri filePath;
    StorageReference mstorage;
    DatabaseReference mdata,mdataItem,mdataOrder,mdataAll,mdataRateMean;
    EditText comments;
    Button rateImage;
    RatingBar rates;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ratings);

        Intent intent = getIntent();
        tailorID = intent.getStringExtra("tailorID");
        orderKey=intent.getStringExtra("orderKey");

        FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
        cUserID=user.getUid();

        mdata=FirebaseDatabase.getInstance().getReference().child(tailorID).child("reviews");
        mdataItem=FirebaseDatabase.getInstance().getReference().child(tailorID).child("item");
        mdataOrder=FirebaseDatabase.getInstance().getReference().child(cUserID).child("order").child(orderKey).child("orderItemData");
        mdataAll=FirebaseDatabase.getInstance().getReference().child("allItems");
        mdataRateMean=FirebaseDatabase.getInstance().getReference().child("rateMean");

        final TextView shopName,tailorName,email,tele,address,city;
        shopName=findViewById(R.id.shopName);
        tailorName=findViewById(R.id.tailorName);
        email=findViewById(R.id.email);
        tele=findViewById(R.id.tele);
        address=findViewById(R.id.address);
        city=findViewById(R.id.city);

        rates=findViewById(R.id.ratingStar);
        comments=findViewById(R.id.comment);
        commentImage= findViewById(R.id.commentImage);
        rateImage=findViewById(R.id.rateImage);

        Button post=findViewById(R.id.post);

        mstorage= FirebaseStorage.getInstance().getReference();


        DatabaseReference tailordata= FirebaseDatabase.getInstance().getReference().child(tailorID).child("tailorData");
        tailordata.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String shopNamet=dataSnapshot.child("businessName").getValue().toString();
                String tailorNamet=dataSnapshot.child("tailorName").getValue().toString();
                String emailt=dataSnapshot.child("email").getValue().toString();
                String telet=dataSnapshot.child("tele").getValue().toString();
                String addresst=dataSnapshot.child("address").getValue().toString();
                String countryt=dataSnapshot.child("country").getValue().toString();
                String cityt=dataSnapshot.child("city").getValue().toString();


                shopName.setText(shopNamet.toUpperCase());
                tailorName.setText("BY "+tailorNamet.toUpperCase());
                email.setText(emailt);
                tele.setText(telet);
                address.setText(addresst);
                city.setText(countryt+ " - "+ cityt );

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        rateImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
            }
        });

        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               upload();
            }
        });



    }

    private void showPictureDialog() {

        androidx.appcompat.app.AlertDialog.Builder pictureDialog = new androidx.appcompat.app.AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                dispathTakePictureIntent();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);


    }


    private  void dispathTakePictureIntent()
    {
        Intent takePictureIntent= new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(takePictureIntent.resolveActivity(getPackageManager())!= null) {
            File photoFile = null;
            try {
                {
                    photoFile = createImageFile();
                }
            } catch (IOException ex) {

            }

            if (photoFile != null) {
                Uri photoUri = FileProvider.getUriForFile(this, "com.example.android.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(takePictureIntent, CAMERA);
            }
        }
    }

    String currentPhotoPath;

    private  File createImageFile() throws IOException
    {
        String timeStamp=new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName="JPEG_"+timeStamp+"_";
        File storageDir= Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image=File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        currentPhotoPath=image.getAbsolutePath();
        return  image;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_CANCELED) {
            return;
        }

        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                filePath = contentURI;
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    Toast.makeText(ratings.this, "Image Saved!", Toast.LENGTH_SHORT).show();

                    commentImage.setImageBitmap(bitmap);


                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(ratings.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {

            File f=new File(currentPhotoPath);
            commentImage.setImageURI(Uri.fromFile(f));
            filePath=Uri.fromFile(f);

            Intent mediaScanIntent=new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri=Uri.fromFile(f);
            mediaScanIntent.setData(contentUri);
            this.sendBroadcast(mediaScanIntent);

        }
    }


    private void upload() {

        if (filePath != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Wait...");
            progressDialog.show();

            final StorageReference ref = mstorage.child("Review/" + UUID.randomUUID().toString());
            ref.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    String savedUri = uri.toString();

                                    String text=comments.getText().toString();
                                    final HashMap<String,String> addMap=new HashMap<>();

                                    addMap.put("text",text);
                                    addMap.put("commentImageUrl",savedUri);
                                    addMap.put("cutomerID",cUserID);

                                    final float star=rates.getRating();


                                    mdata.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshotR) {

                                            float totalRevies= (float) 0.0;
                                            int count=0;

                                            if(dataSnapshotR.child("reviewData").child("totalReviews").exists())
                                            {
                                                totalRevies=Float.valueOf(dataSnapshotR.child("reviewData").child("totalReviews").getValue().toString());
                                                count=Integer.parseInt(dataSnapshotR.child("reviewData").child("count").getValue().toString());

                                                totalRevies=((totalRevies*count) + star)/(count+1);
                                                count=count+1;

                                                mdata.child("reviewData").child("totalReviews").setValue(String.valueOf(totalRevies));
                                                mdata.child("reviewData").child("count").setValue(String.valueOf(count));
                                            }
                                            else
                                            {
                                                mdata.child("reviewData").child("totalReviews").setValue(String.valueOf(star));
                                                mdata.child("reviewData").child("count").setValue("1");
                                            }



                                            mdataOrder.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshotO) {
                                                    if (dataSnapshotO.exists())
                                                    {

                                                        final String category=dataSnapshotO.child("category").getValue().toString();
                                                        final String subCategory=dataSnapshotO.child("subCategory").getValue().toString();
                                                        final String itemName=dataSnapshotO.child("itemName").getValue().toString();
                                                        final String itemPushID=dataSnapshotO.child("fullDreassPushID").getValue().toString();

                                                        mdataItem.child(category).child(subCategory).child(itemName).child("itemRating").addListenerForSingleValueEvent(new ValueEventListener() {
                                                            @Override
                                                            public void onDataChange(@NonNull DataSnapshot dataSnapshotI) {

                                                                float totalItemRevies= (float) 0.0;
                                                                int countItem=0;

                                                                if (dataSnapshotI.exists())
                                                                {
                                                                    totalItemRevies=Float.valueOf(dataSnapshotI.child("totalItemReviews").getValue().toString());
                                                                    countItem=Integer.parseInt(dataSnapshotI.child("countItem").getValue().toString());

                                                                    totalItemRevies=((totalItemRevies*countItem) + star)/(countItem+1);
                                                                    countItem=countItem+1;

                                                                    mdataItem.child(category).child(subCategory).child(itemName).child("itemRating").child("totalItemReviews").setValue(String.valueOf(totalItemRevies));
                                                                    mdataItem.child(category).child(subCategory).child(itemName).child("itemRating").child("countItem").setValue(String.valueOf(countItem));

                                                                    mdataAll.child(itemPushID).child("starRev").setValue(String.valueOf(totalItemRevies));
                                                                    mdataAll.child(itemPushID).child("starRevCount").setValue(String.valueOf(countItem));

                                                                    mdata.child("customerReview").push().setValue(addMap);

                                                                    mdataRateMean.addListenerForSingleValueEvent(new ValueEventListener() {
                                                                        @Override
                                                                        public void onDataChange(@NonNull DataSnapshot dataSnapshotRM) {
                                                                            if(dataSnapshotRM.exists())
                                                                            {
                                                                                float meanRate=Float.valueOf(dataSnapshotRM.child("meanRate").getValue().toString());
                                                                                float count=Float.valueOf(dataSnapshotRM.child("count").getValue().toString());

                                                                                meanRate=(meanRate*count+star)/(count+1);

                                                                                mdataRateMean.child("meanRate").setValue(String.valueOf(meanRate));
                                                                                mdataRateMean.child("count").setValue(String.valueOf(count));
                                                                            }
                                                                            else
                                                                            {
                                                                                mdataRateMean.child("meanRate").setValue(String.valueOf(star));
                                                                                mdataRateMean.child("count").setValue("1");
                                                                            }
                                                                        }

                                                                        @Override
                                                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                                                        }
                                                                    });

                                                                    Toast.makeText(ratings.this,"Successful!",Toast.LENGTH_SHORT).show();

                                                                    Intent intent = new Intent(ratings.this, cViewOrderClick.class);
                                                                    intent.putExtra("tailorID",tailorID);
                                                                    intent.putExtra("orderKey",orderKey);
                                                                    startActivity(intent);
                                                                    finish();

                                                                }

                                                                else
                                                                {
                                                                    mdataItem.child(category).child(subCategory).child(itemName).child("itemRating").child("totalItemReviews").setValue(String.valueOf(star));
                                                                    mdataItem.child(category).child(subCategory).child(itemName).child("itemRating").child("countItem").setValue("1");

                                                                    mdataAll.child(itemPushID).child("starRev").setValue(String.valueOf(star));
                                                                    mdataAll.child(itemPushID).child("starRevCount").setValue("1");

                                                                    mdata.child("customerReview").push().setValue(addMap);

                                                                    mdataRateMean.addListenerForSingleValueEvent(new ValueEventListener() {
                                                                        @Override
                                                                        public void onDataChange(@NonNull DataSnapshot dataSnapshotRM) {
                                                                            if(dataSnapshotRM.exists())
                                                                            {
                                                                                float meanRate=Float.valueOf(dataSnapshotRM.child("meanRate").getValue().toString());
                                                                                float count=Float.valueOf(dataSnapshotRM.child("count").getValue().toString());

                                                                                meanRate=(meanRate*count+star)/(count+1);

                                                                                mdataRateMean.child("meanRate").setValue(String.valueOf(meanRate));
                                                                                mdataRateMean.child("count").setValue(String.valueOf(count));
                                                                            }
                                                                            else
                                                                            {
                                                                                mdataRateMean.child("meanRate").setValue(String.valueOf(star));
                                                                                mdataRateMean.child("count").setValue("1");
                                                                            }
                                                                        }

                                                                        @Override
                                                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                                                        }
                                                                    });

                                                                    Toast.makeText(ratings.this,"Successful!",Toast.LENGTH_SHORT).show();

                                                                    Intent intent = new Intent(ratings.this, cViewOrderClick.class);
                                                                    intent.putExtra("tailoruserID",tailorID);
                                                                    intent.putExtra("orderKey",orderKey);
                                                                    startActivity(intent);
                                                                    finish();
                                                                }



                                                            }

                                                            @Override
                                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                                            }
                                                        });
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                }
                                            });



                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });



                                }
                            });
                            progressDialog.dismiss();

                            Toast.makeText(ratings.this, "Succesful", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(ratings.this, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                    .getTotalByteCount());


                            progressDialog.setMessage("Successful " );
                        }
                    });
        }
        else
        {
            Toast.makeText(ratings.this, "no image!", Toast.LENGTH_SHORT).show();
            String text=comments.getText().toString();
            final HashMap <String,String> addMap=new HashMap<>();

            addMap.put("text",text);
            addMap.put("cutomerID",cUserID);

            final float star=rates.getRating();

            Log.d("star",String.valueOf(star));

            mdata.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshotR) {

                    float totalRevies= (float) 0.0;
                    int count=0;

                    if(dataSnapshotR.child("reviewData").child("totalReviews").exists())
                    {
                        totalRevies=Float.valueOf(dataSnapshotR.child("reviewData").child("totalReviews").getValue().toString());
                        count=Integer.parseInt(dataSnapshotR.child("reviewData").child("count").getValue().toString());

                        totalRevies=((totalRevies*count) + star)/(count+1);
                        count=count+1;

                        mdata.child("reviewData").child("totalReviews").setValue(String.valueOf(totalRevies));
                        mdata.child("reviewData").child("count").setValue(String.valueOf(count));

                    }
                    else
                    {
                        mdata.child("reviewData").child("totalReviews").setValue(String.valueOf(star));
                        mdata.child("reviewData").child("count").setValue("1");

                    }


                    mdataOrder.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshotO) {
                            if (dataSnapshotO.exists())
                            {

                                final String category=dataSnapshotO.child("category").getValue().toString();
                                final String subCategory=dataSnapshotO.child("subCategory").getValue().toString();
                                final String itemName=dataSnapshotO.child("itemName").getValue().toString();
                                final String itemPushID=dataSnapshotO.child("fullDreassPushID").getValue().toString();

                                mdataItem.child(category).child(subCategory).child(itemName).child("itemRating").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshotI) {

                                        float totalItemRevies= (float) 0.0;
                                        int countItem=0;

                                        if (dataSnapshotI.exists())
                                        {
                                            totalItemRevies=Float.valueOf(dataSnapshotI.child("totalItemReviews").getValue().toString());
                                            countItem=Integer.parseInt(dataSnapshotI.child("countItem").getValue().toString());

                                            totalItemRevies=((totalItemRevies*countItem) + star)/(countItem+1);
                                            countItem=countItem+1;

                                            mdataItem.child(category).child(subCategory).child(itemName).child("itemRating").child("totalItemReviews").setValue(String.valueOf(totalItemRevies));
                                            mdataItem.child(category).child(subCategory).child(itemName).child("itemRating").child("countItem").setValue(String.valueOf(countItem));

                                            mdataAll.child(itemPushID).child("starRev").setValue(String.valueOf(totalItemRevies));
                                            mdataAll.child(itemPushID).child("starRevCount").setValue(String.valueOf(countItem));

                                            mdata.child("customerReview").push().setValue(addMap);

                                            mdataRateMean.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshotRM) {
                                                    if(dataSnapshotRM.exists())
                                                    {
                                                        float meanRate=Float.valueOf(dataSnapshotRM.child("meanRate").getValue().toString());
                                                        float count=Float.valueOf(dataSnapshotRM.child("count").getValue().toString());

                                                        meanRate=(meanRate*count+star)/(count+1);

                                                        mdataRateMean.child("meanRate").setValue(String.valueOf(meanRate));
                                                        mdataRateMean.child("count").setValue(String.valueOf(count));
                                                    }
                                                    else
                                                    {
                                                        mdataRateMean.child("meanRate").setValue(String.valueOf(star));
                                                        mdataRateMean.child("count").setValue("1");
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                }
                                            });

                                            Toast.makeText(ratings.this,"Successful!",Toast.LENGTH_SHORT).show();

                                            Intent intent = new Intent(ratings.this, cViewOrderClick.class);
                                            intent.putExtra("tailoruserID",tailorID);
                                            intent.putExtra("orderKey",orderKey);
                                            startActivity(intent);
                                            finish();
                                        }

                                        else
                                        {
                                            mdataItem.child(category).child(subCategory).child(itemName).child("itemRating").child("totalItemReviews").setValue(String.valueOf(star));
                                            mdataItem.child(category).child(subCategory).child(itemName).child("itemRating").child("countItem").setValue("1");

                                            mdataAll.child(itemPushID).child("starRev").setValue(String.valueOf(star));
                                            mdataAll.child(itemPushID).child("starRevCount").setValue("1");

                                            mdata.child("customerReview").push().setValue(addMap);

                                            mdataRateMean.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshotRM) {
                                                    if(dataSnapshotRM.exists())
                                                    {
                                                        float meanRate=Float.valueOf(dataSnapshotRM.child("meanRate").getValue().toString());
                                                        float count=Float.valueOf(dataSnapshotRM.child("count").getValue().toString());

                                                        meanRate=(meanRate*count+star)/(count+1);

                                                        mdataRateMean.child("meanRate").setValue(String.valueOf(meanRate));
                                                        mdataRateMean.child("count").setValue(String.valueOf(count));
                                                    }
                                                    else
                                                    {
                                                        mdataRateMean.child("meanRate").setValue(String.valueOf(star));
                                                        mdataRateMean.child("count").setValue("1");
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                }
                                            });

                                            Toast.makeText(ratings.this,"Successful!",Toast.LENGTH_SHORT).show();

                                            Intent intent = new Intent(ratings.this, cViewOrderClick.class);
                                            intent.putExtra("tailorID",tailorID);
                                            intent.putExtra("orderKey",orderKey);
                                            startActivity(intent);
                                            finish();
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });


                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            mdata.child("customerReview").push().setValue(addMap);


        }


    }






}