package com.example.fashiondesigner.order;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fashiondesigner.Customer;
import com.example.fashiondesigner.HomeFragment;
import com.example.fashiondesigner.OrderFragment;
import com.example.fashiondesigner.R;
import com.example.fashiondesigner.measurements.placeOrder;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Date;

public class payments extends AppCompatActivity {

    String customerUserID,orderPushID,userID;
    String cost="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payments);

        Intent intent = getIntent();
        customerUserID = intent.getStringExtra("customerUserID");
        orderPushID=intent.getStringExtra("orderPushID");
        userID=intent.getStringExtra("userID");


        final Button payc=findViewById(R.id.pay);
        final TextView amountc=findViewById(R.id.amount);

        final DatabaseReference mdata= FirebaseDatabase.getInstance().getReference().child(customerUserID);

        mdata.child("order").child(orderPushID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                cost=dataSnapshot.child("price").getValue().toString();

                 amountc.setText("Payable Mout : $ "+ cost);



                payc.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {



                        mdata.child("order").child(orderPushID).child("orderState").setValue("1");


                        String timeKey=new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                        mdata.child("order").child(orderPushID).child("orderTime").setValue(timeKey);
                        mdata.child("order").child(orderPushID).child("tailorID").setValue(userID);


                        final DatabaseReference tailor=FirebaseDatabase.getInstance().getReference().child(userID);
                        tailor.child("orders").child(orderPushID).child("customerID").setValue(customerUserID);
                        tailor.child("orders").child(orderPushID).child("orderState").setValue("1");
                        tailor.child("orders").child(orderPushID).child("orderTime").setValue(timeKey);

                        DatabaseReference admin=FirebaseDatabase.getInstance().getReference().child("admin");
                        admin.child("orders").child(orderPushID).child("tailorId").setValue(userID);
                        admin.child("orders").child(orderPushID).child("customerID").setValue(customerUserID);
                        admin.child("orders").child(orderPushID).child("amount").setValue(cost);

                       mdata.child("order").child(orderPushID).child("suggestID").addListenerForSingleValueEvent(new ValueEventListener() {
                           @Override
                           public void onDataChange(@NonNull DataSnapshot dataSnapshotSID) {
                               if(dataSnapshotSID.exists())
                               {
                                   String suggestID=dataSnapshotSID.getValue().toString();
                                       mdata.child("suggestOrder").child(suggestID).removeValue();

                                       tailor.child("suggestOrder").child(suggestID).removeValue();

                                       mdata.child("order").child(orderPushID).child("suggestID").removeValue();
                                       mdata.child("order").child(orderPushID).child("suggest").setValue("1");
                               }
                           }

                           @Override
                           public void onCancelled(@NonNull DatabaseError databaseError) {

                           }
                       });


                        Toast.makeText(payments.this, "Ordered successfully!", Toast.LENGTH_SHORT).show();


                        Intent intent = new Intent(payments.this, Customer.class);
                        startActivity(intent);
                        finish();



                    }
                });


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });






    }
}
