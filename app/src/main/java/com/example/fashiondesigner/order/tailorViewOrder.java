package com.example.fashiondesigner.order;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.example.fashiondesigner.Adapter.Model.searchModel;
import com.example.fashiondesigner.Adapter.Model.tailorOrderModel;
import com.example.fashiondesigner.Adapter.orderAdupter;
import com.example.fashiondesigner.Adapter.tailorOrderAdupter;
import com.example.fashiondesigner.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class tailorViewOrder extends AppCompatActivity {

    ArrayList<tailorOrderModel> searchArray;
    tailorOrderAdupter adapter;
    RecyclerView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tailor_view_order);


        Intent intent = getIntent();
        String clickView = intent.getStringExtra("clickView");

        FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
        String tailorUserID=user.getUid();


        listView=findViewById(R.id.reView);

        searchArray=new ArrayList<>();

        listView.setHasFixedSize(true);
        listView.setLayoutManager(new LinearLayoutManager(tailorViewOrder.this,LinearLayoutManager.VERTICAL,false));

        adapter=new tailorOrderAdupter(tailorViewOrder.this,searchArray);
        listView.setAdapter(adapter);


        TextView clickTitlet=findViewById(R.id.clickTitle);

        if(clickView.equals("1"))
        {
            clickTitlet.setText("ONGOING ORDERS");
        }
        else if(clickView.equals("2"))
        {
            clickTitlet.setText("COMPLETED ORDERS");
        }


        final DatabaseReference tailorOrder=FirebaseDatabase.getInstance().getReference().child(tailorUserID).child("orders");

        Query findQuery1=tailorOrder.orderByChild("orderState").equalTo(clickView);

        findQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists())
                {
                    for(DataSnapshot ds : dataSnapshot.getChildren())
                    {
                        final String key = ds.getKey();

                        tailorOrder.child(key).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshotTD) {

                                final String customerID=dataSnapshotTD.child("customerID").getValue().toString();
                                final  String orderTime=dataSnapshotTD.child("orderTime").getValue().toString();

                                final DatabaseReference orderItemData=FirebaseDatabase.getInstance().getReference().child(customerID);

                                orderItemData.child("order").child(key).child("orderItemData").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull final DataSnapshot dataSnapshotOID) {

                                        orderItemData.child("order").child(key).child("suggest").addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshotSS) {

                                                if(dataSnapshotSS.exists())
                                                {
                                                    final tailorOrderModel oModel=new tailorOrderModel();

                                                    if(dataSnapshotOID.child("frontImage").exists())
                                                    {
                                                        String frontImage=dataSnapshotOID.child("frontImage").getValue().toString();
                                                        oModel.setImageUrl(frontImage);
                                                    }



                                                    orderItemData.child("customerData").addListenerForSingleValueEvent(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(@NonNull DataSnapshot dataSnapshotOCD) {

                                                            String cName=dataSnapshotOCD.child("name").getValue().toString();
                                                            String cemail=dataSnapshotOCD.child("email").getValue().toString();
                                                            String tele=dataSnapshotOCD.child("tele").getValue().toString();




                                                            oModel.setItemName("Orer ID : "+orderTime);
                                                            oModel.setCustomerName(cName);
                                                            oModel.setCemail(cemail);
                                                            oModel.setCtele(tele);
                                                            oModel.setCustomerID(customerID);
                                                            oModel.setOrderKey(key);
                                                            oModel.setSuggest(true);

                                                            searchArray.add(oModel);
                                                            adapter.notifyDataSetChanged();



                                                        }

                                                        @Override
                                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                                        }
                                                    });
                                                }

                                                else
                                                {
                                                    if(dataSnapshotOID.child("fullImageUrl").exists())
                                                    {
                                                        final String imageUrl=dataSnapshotOID.child("fullImageUrl").getValue().toString();

                                                        final String name=dataSnapshotOID.child("itemName").getValue().toString();


                                                        orderItemData.child("customerData").addListenerForSingleValueEvent(new ValueEventListener() {
                                                            @Override
                                                            public void onDataChange(@NonNull DataSnapshot dataSnapshotOCD) {

                                                                String cName=dataSnapshotOCD.child("name").getValue().toString();
                                                                String cemail=dataSnapshotOCD.child("email").getValue().toString();
                                                                String tele=dataSnapshotOCD.child("tele").getValue().toString();

                                                                tailorOrderModel oModel=new tailorOrderModel();

                                                                oModel.setImageUrl(imageUrl);
                                                                oModel.setItemName(name);
                                                                oModel.setCustomerName(cName);
                                                                oModel.setCemail(cemail.toLowerCase());
                                                                oModel.setCtele(tele);
                                                                oModel.setCustomerID(customerID);
                                                                oModel.setOrderKey(key);

                                                                searchArray.add(oModel);
                                                                adapter.notifyDataSetChanged();



                                                            }

                                                            @Override
                                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                                            }
                                                        });

                                                    }



                                                }



                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                            }
                                        });
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });







    }
}
