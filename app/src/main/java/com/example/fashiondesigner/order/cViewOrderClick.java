package com.example.fashiondesigner.order;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.fashiondesigner.R;
import com.example.fashiondesigner.chat.meassageActivity;
import com.example.fashiondesigner.suggest.suggestViewTClick;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class cViewOrderClick extends AppCompatActivity {

    String tailoruserID,cUserID,orderKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_c_view_order_click);


        Intent intent = getIntent();
        tailoruserID = intent.getStringExtra("tailoruserID");
        orderKey=intent.getStringExtra("orderKey");

        FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
        cUserID=user.getUid();

        final TextView shopName,tailorName,email,tele,address,city;
        shopName=findViewById(R.id.shopName);
        tailorName=findViewById(R.id.tailorName);
        email=findViewById(R.id.email);
        tele=findViewById(R.id.tele);
        address=findViewById(R.id.address);
        city=findViewById(R.id.city);


        final TextView categoryTv,subcategoryTv,itemNameTv,colourNameTv;
        final ImageView itemImage,fabricImage;

        categoryTv=findViewById(R.id.category);
        subcategoryTv=findViewById(R.id.subCategory);
        itemNameTv=findViewById(R.id.itemName);
        colourNameTv=findViewById(R.id.colourName);

        itemImage=findViewById(R.id.itemImage);
        fabricImage=findViewById(R.id.fabrciImage);
        final TextView track=findViewById(R.id.track);

        final Button placeOrder=findViewById(R.id.placeOrder);
        final Button rating=findViewById(R.id.rating);

        final TextView startDate=findViewById(R.id.startDate);
        final TextView endDate=findViewById(R.id.endDate);
        Button report=findViewById(R.id.reportOrder);

        rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(cViewOrderClick.this, ratings.class);
                intent.putExtra("tailorID",tailoruserID);
                intent.putExtra("orderKey",orderKey);
                startActivity(intent);

            }
        });

        report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(cViewOrderClick.this, meassageActivity.class);
                intent.putExtra("userID","5RczAgVRbARM7iGPLK9nZ0Z0Z853");
                startActivity(intent);

            }
        });


        DatabaseReference tailordata= FirebaseDatabase.getInstance().getReference().child(tailoruserID).child("tailorData");
        tailordata.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String shopNamet=dataSnapshot.child("businessName").getValue().toString();
                String tailorNamet=dataSnapshot.child("tailorName").getValue().toString();
                String emailt=dataSnapshot.child("email").getValue().toString();
                String telet=dataSnapshot.child("tele").getValue().toString();
                String addresst=dataSnapshot.child("address").getValue().toString();
                String countryt=dataSnapshot.child("country").getValue().toString();
                String cityt=dataSnapshot.child("city").getValue().toString();


                shopName.setText(shopNamet.toUpperCase());
                tailorName.setText("BY "+tailorNamet.toUpperCase());
                email.setText(emailt);
                tele.setText(telet);
                address.setText(addresst);
                city.setText(countryt+ " - "+ cityt );

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        final DatabaseReference orderData=FirebaseDatabase.getInstance().getReference().child(cUserID).child("order").child(orderKey);
        orderData.child("orderItemData").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshotID) {

                String subCategory=dataSnapshotID.child("subCategory").getValue().toString();
                String category=dataSnapshotID.child("category").getValue().toString();
                String itemName=dataSnapshotID.child("itemName").getValue().toString();
                String colourKey=dataSnapshotID.child("colourKey").getValue().toString();
                String fabricKey=dataSnapshotID.child("fabrciKey").getValue().toString();
                final String fullDreassPushID=dataSnapshotID.child("fullDreassPushID").getValue().toString();
                String fullImageUrl=dataSnapshotID.child("fullImageUrl").getValue().toString();
                String addFire=dataSnapshotID.child("addFire").getValue().toString();

                colourNameTv.setText(addFire);
                categoryTv.setText(category.toUpperCase());
                subcategoryTv.setText(subCategory.toUpperCase());
                itemNameTv.setText(itemName.toUpperCase());

                Glide.with(itemImage.getContext())
                        .load(fullImageUrl)
                        .into(itemImage);

               final DatabaseReference itemData=FirebaseDatabase.getInstance().getReference().child(tailoruserID);
               itemData.child("fabric").child("colours").child(fabricKey).child(colourKey).addListenerForSingleValueEvent(new ValueEventListener() {
                   @Override
                   public void onDataChange(@NonNull DataSnapshot dataSnapshotCI) {

                       String colourImage=dataSnapshotCI.child("imageUrl").getValue().toString();

                       Glide.with(fabricImage.getContext())
                               .load(colourImage)
                               .into(fabricImage);
                   }

                   @Override
                   public void onCancelled(@NonNull DatabaseError databaseError) {

                   }
               });




                orderData.child("orderState").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshotOS) {
                        if(dataSnapshotOS.exists())
                        {
                            String stateCheck=dataSnapshotOS.getValue().toString();

                            if(stateCheck.equals("2"))
                            {

                                orderData.child("price").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshotP) {

                                        String price=dataSnapshotP.getValue().toString();

                                        track.setText("PAID $ "+price);


                                        placeOrder.setVisibility(View.GONE);
                                        rating.setVisibility(View.VISIBLE);
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            }
                            else if(stateCheck.equals("1"))
                            {

                                placeOrder.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        orderData.child("orderState").setValue("2");

                                        itemData.child("orders").child(orderKey).child("orderState").setValue("2");

                                        orderData.child("price").addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshotP) {

                                                String price=dataSnapshotP.getValue().toString();

                                                track.setText("PAID $ "+price);


                                                placeOrder.setVisibility(View.GONE);

                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                            }
                                        });


                                    }
                                });

                                orderData.child("deliver").addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshotDL) {

                                        if(dataSnapshotDL.exists())
                                        {
                                            String deliverD=dataSnapshotDL.getValue().toString();

                                            if(deliverD.equals("1"))
                                            {
                                                track.setText("ON DELIVERING");
                                            }
                                        }
                                        else
                                        {
                                            DatabaseReference accept=FirebaseDatabase.getInstance().getReference().child(tailoruserID).child("orders").child(orderKey).child("acceptState");
                                            accept.addValueEventListener(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshotAcc) {
                                                    if(dataSnapshotAcc.exists())
                                                    {
                                                        track.setText("ON TAILERING");
                                                        placeOrder.setVisibility(View.GONE);
                                                    }
                                                    else
                                                    {
                                                        track.setText("Wait untile Tailor accept the order");
                                                        placeOrder.setVisibility(View.GONE);
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                }
                                            });


                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });



                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });




            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        final DatabaseReference deliverDate=FirebaseDatabase.getInstance().getReference().child(tailoruserID).child("orders").child(orderKey);
        deliverDate.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshotDD) {
                if(dataSnapshotDD.exists())
                {
                    if(dataSnapshotDD.child("startDate").exists())
                    {
                        String stDate=dataSnapshotDD.child("startDate").getValue().toString();
                        startDate.setText("Minimum Delivery Date : "+stDate);
                    }

                    if(dataSnapshotDD.child("endDate").exists())
                    {
                        String enDate=dataSnapshotDD.child("endDate").getValue().toString();
                        endDate.setText("Maximum Delivery Date : "+enDate);
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });




    }
}
