package com.example.fashiondesigner.order;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fashiondesigner.Adapter.Model.searchModel;
import com.example.fashiondesigner.Adapter.orderAdupter;
import com.example.fashiondesigner.Adapter.searchAdupter;
import com.example.fashiondesigner.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class viewOrder extends AppCompatActivity {

    ArrayList<searchModel> searchArray;
    orderAdupter adapter;
    RecyclerView listView;
    int tag=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_order);

        Intent intent = getIntent();
        String clickView = intent.getStringExtra("clickView");

        FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
        String cUserID=user.getUid();

        listView=findViewById(R.id.reView);

        searchArray=new ArrayList<>();

        listView.setHasFixedSize(true);
        listView.setLayoutManager(new LinearLayoutManager(viewOrder.this,LinearLayoutManager.VERTICAL,false));

        adapter=new orderAdupter(viewOrder.this,searchArray);
        listView.setAdapter(adapter);

        TextView clickTitlet=findViewById(R.id.clickTitle);

        if(clickView.equals("1"))
        {
            clickTitlet.setText("ONGOING ORDERS");
        }
        else if(clickView.equals("2"))
        {
            clickTitlet.setText("COMPLETED ORDERS");
        }



        final DatabaseReference mdata= FirebaseDatabase.getInstance().getReference().child(cUserID).child("order");

        Query findQuery1=mdata.orderByChild("orderState").equalTo(clickView);

        findQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                {
                    for(DataSnapshot ds : dataSnapshot.getChildren()) {
                        final String key = ds.getKey();

                        mdata.child(key).child("suggest").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshotSS) {


                                if(dataSnapshotSS.exists())
                                {

                                    mdata.child(key).addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull final DataSnapshot dataSnapshotTID) {

                                            if(dataSnapshotTID.exists())
                                            {
                                                final String tailorID=dataSnapshotTID.child("tailorID").getValue().toString();

                                                DatabaseReference tailorData=FirebaseDatabase.getInstance().getReference().child(tailorID).child("tailorData");
                                                tailorData.addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshotSD) {

                                                        String shopName=dataSnapshotSD.child("businessName").getValue().toString();
                                                        String tailorName=dataSnapshotSD.child("tailorName").getValue().toString();
                                                        String country=dataSnapshotSD.child("country").getValue().toString();
                                                        String city=dataSnapshotSD.child("city").getValue().toString();


                                                        String price=dataSnapshotTID.child("price").getValue().toString();

                                                        searchModel srModel=new searchModel();

                                                        if(dataSnapshotTID.child("orderItemData").child("frontImage").exists())
                                                        {
                                                            String image=dataSnapshotTID.child("orderItemData").child("frontImage").getValue().toString();
                                                            srModel.setImage(image);
                                                        }




                                                        srModel.setshopName(shopName);
                                                        srModel.setTailor(tailorName);
                                                        srModel.setCountry(country);
                                                        srModel.setCity(city);
                                                        srModel.setTotalPrice(price);
                                                        srModel.setUser_ID(tailorID);
                                                        srModel.setOrderKey(key);
                                                        srModel.setSuggest(true);

                                                        searchArray.add(srModel);
                                                        adapter.notifyDataSetChanged();



                                                    }

                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                                    }
                                                });
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });
                                }
                                else
                                {

                                    mdata.child(key).child("orderItemData").child("fullDreassPushID").addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshott) {

                                            String fullDressPushID=dataSnapshott.getValue().toString();


                                            DatabaseReference allItems=FirebaseDatabase.getInstance().getReference().child("allItems").child(fullDressPushID);

                                            allItems.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshotAll) {

                                                    if(dataSnapshotAll.exists())
                                                    {
                                                        final searchModel smodel=dataSnapshotAll.getValue(searchModel.class);


                                                        mdata.child(key).child("price").addListenerForSingleValueEvent(new ValueEventListener() {
                                                            @Override
                                                            public void onDataChange(@NonNull DataSnapshot dataSnapshotTP) {
                                                                String totPrice=dataSnapshotTP.getValue().toString();

                                                                smodel.setTotalPrice(totPrice);
                                                                smodel.setOrderKey(key);
                                                                searchArray.add(smodel);
                                                                adapter.notifyDataSetChanged();
                                                                tag=1;

                                                            }

                                                            @Override
                                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                                            }
                                                        });
                                                    }





                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                }
                                            });


                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });





                    }


                }
                else
                {
                    Toast.makeText(viewOrder.this,"No items to show",Toast.LENGTH_SHORT).show();
                }


            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        Toast.makeText(viewOrder.this,"Processing...",Toast.LENGTH_SHORT).show();


    }
}
