package com.example.fashiondesigner.order;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.fashiondesigner.R;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;

public class tViewOrderClick extends AppCompatActivity {

    String reBuild="";
    ArrayList measureData;
    PhotoView photoView;

    Button start,end;
    TextView stext,endtext;
    DatePickerDialog datePickerDialog;

    int year1,year2;
    int month1,month2;
    int dayOfMon1,dayOfMon2;
    Calendar calender1,calender2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_t_view_order_click);

        Intent intent = getIntent();
        final String customerID = intent.getStringExtra("customerID");
        final String orderKey=intent.getStringExtra("orderKey");
        final String cName=intent.getStringExtra("cName");
        final String email=intent.getStringExtra("email");
        final String tele=intent.getStringExtra("tele");

        FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
        final String userID=user.getUid();

        final TextView cNametv,cemailtv,cteletv,addresstv;

        cNametv=findViewById(R.id.cName);
        cemailtv=findViewById(R.id.cemail);
        cteletv=findViewById(R.id.ctele);
        addresstv=findViewById(R.id.address);


        final TextView categoryTv,subcategoryTv,itemNameTv,colourNameTv;
        final ImageView itemImage,fabricImage;

        categoryTv=findViewById(R.id.category);
        subcategoryTv=findViewById(R.id.subCategory);
        itemNameTv=findViewById(R.id.itemName);
        colourNameTv=findViewById(R.id.colourName);

        itemImage=findViewById(R.id.itemImage);
        fabricImage=findViewById(R.id.fabrciImage);

        cNametv.setText(cName.toUpperCase());
        cemailtv.setText(email);
        cteletv.setText(tele);

        final TextView addTextTV=findViewById(R.id.addText);
        final ImageView addImageIV=findViewById(R.id.addImage);

        final TextView mtextTV=findViewById(R.id.mtext);
        photoView = findViewById(R.id.photo);

        final Button deliver=findViewById(R.id.deliver);
        final TextView track=findViewById(R.id.track);

        start=findViewById(R.id.start);
        end=findViewById(R.id.end);

        stext=findViewById(R.id.stext);
        endtext=findViewById(R.id.endtext);

        measureData=new ArrayList();

        final DatabaseReference add= FirebaseDatabase.getInstance().getReference().child(customerID).child("order").child(orderKey);

        final DatabaseReference accept=FirebaseDatabase.getInstance().getReference().child(userID).child("orders").child(orderKey);
        accept.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshotAcc) {
                if(dataSnapshotAcc.child("orderState").exists())
                {
                    if (!dataSnapshotAcc.child("acceptState").exists())
                    {
                        deliver.setText("Accept Order");
                        track.setVisibility(View.GONE);
                    }


                }

                if(dataSnapshotAcc.child("startDate").exists())
                {
                    String std=dataSnapshotAcc.child("startDate").getValue().toString();
                    stext.setText(std);
                }

                if(dataSnapshotAcc.child("endDate").exists())
                {
                    String end=dataSnapshotAcc.child("endDate").getValue().toString();
                    endtext.setText(end);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });




        deliver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                accept.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshotAcc) {
                        if(dataSnapshotAcc.exists())
                        {
                            if(dataSnapshotAcc.child("acceptState").exists())
                            {
                                add.child("deliver").setValue("1");
                                track.setText("ON DELIVERING");
                                deliver.setVisibility(View.GONE);
                            }
                            else
                            {
                                accept.child("acceptState").setValue("5");
                                Intent intent = new Intent(tViewOrderClick.this, tViewOrderClick.class);
                                intent.putExtra("customerID",customerID);
                                intent.putExtra("orderKey",orderKey);
                                intent.putExtra("cName",cName);
                                intent.putExtra("email",email);
                                intent.putExtra("tele",tele);
                                startActivity(intent);
                                finish();
                            }

                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });



            }
        });


        add.child("orderState").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshotOS) {

                if(dataSnapshotOS.exists())
                {
                    String stateCheck=dataSnapshotOS.getValue().toString();

                    if(stateCheck.equals("2"))
                    {
                        add.child("price").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshotP) {

                                String price=dataSnapshotP.getValue().toString();

                                track.setText("PAID $ "+price);


                                deliver.setVisibility(View.GONE);

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }

                    else if(stateCheck.equals("1"))
                    {
                        add.child("deliver").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshotDL) {

                                if(dataSnapshotDL.exists())
                                {
                                    String deliverD=dataSnapshotDL.getValue().toString();

                                    if(deliverD.equals("1"))
                                    {
                                        track.setText("ON DELIVERING");
                                    }
                                }
                                else
                                {
                                    track.setText("ON TAILERING");
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



        add.child("deliverAddress").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String name=dataSnapshot.child("name").getValue().toString();
                String tele=dataSnapshot.child("tele").getValue().toString();
                String home=dataSnapshot.child("home").getValue().toString();
                String street=dataSnapshot.child("street").getValue().toString();
                String city=dataSnapshot.child("city").getValue().toString();
                String state=dataSnapshot.child("state").getValue().toString();
                String country=dataSnapshot.child("country").getValue().toString();
                String zip=dataSnapshot.child("zip").getValue().toString();

                String built=name.toUpperCase()+"\n\n"+tele+"\n\n"+home+"\n\n"+street+"\n\n"+city+"\n\n"+state+"\n\n"+country+"\n\n"+zip;
                addresstv.setText(built);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        add.child("orderItemData").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String category=dataSnapshot.child("category").getValue().toString();
                String subCategory=dataSnapshot.child("subCategory").getValue().toString();
                String itemName=dataSnapshot.child("itemName").getValue().toString();
                String fullImageUrl=dataSnapshot.child("fullImageUrl").getValue().toString();
                String addFire=dataSnapshot.child("addFire").getValue().toString();
                String fullDreassPushID=dataSnapshot.child("fullDreassPushID").getValue().toString();
                String fabrciKey=dataSnapshot.child("fabrciKey").getValue().toString();
                String colourKey=dataSnapshot.child("colourKey").getValue().toString();

                categoryTv.setText(category.toUpperCase());
                subcategoryTv.setText(subCategory.toUpperCase());
                itemNameTv.setText(itemName.toUpperCase());

                Glide.with(itemImage.getContext())
                        .load(fullImageUrl)
                        .into(itemImage);


                colourNameTv.setText(addFire);

                DatabaseReference colour=FirebaseDatabase.getInstance().getReference().child(userID).child("fabric").child("colours").child(fabrciKey).child(colourKey);
                colour.child("imageUrl").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshotC) {

                        String imageC=dataSnapshotC.getValue().toString();

                        Glide.with(fabricImage.getContext())
                                .load(imageC)
                                .into(fabricImage);

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


                DatabaseReference tmeasure=FirebaseDatabase.getInstance().getReference().child(userID).child("measurements").child(fullDreassPushID);


                tmeasure.child("gender").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshotG) {
                        String  gender=dataSnapshotG.getValue().toString();
                        if(gender.equals("0"))
                        {
                            photoView.setImageResource(R.drawable.women);
                        }
                        else if(gender.equals("1"))
                        {
                            photoView.setImageResource(R.drawable.men);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

                tmeasure.child("data").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshotTM) {


                        for(DataSnapshot ds1:dataSnapshotTM.getChildren())
                        {
                            String d=ds1.getValue().toString();

                            measureData.add(d);

                        }

                        add.child("mData").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshotMD) {

                                int k=0;

                                for(DataSnapshot ds2:dataSnapshotMD.getChildren())
                                {
                                    String s=ds2.getValue().toString();

                                    String p=measureData.get(k).toString();

                                    reBuild=reBuild+ p+ " : "+s+" cm \n\n";
                                    k++;
                                }

                                mtextTV.setText(reBuild);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });


                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        add.child("addAdditional").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshotx) {
                if(dataSnapshotx.exists())
                {
                    if(dataSnapshotx.child("text").exists())
                    {
                        String text=dataSnapshotx.child("text").getValue().toString();
                        addTextTV.setText(text);

                    }
                    if(dataSnapshotx.child("imageUrl").exists())
                    {
                        String cimage=dataSnapshotx.child("imageUrl").getValue().toString();
                        Glide.with(addImageIV.getContext())
                                .load(cimage)
                                .into(addImageIV);

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calender1=Calendar.getInstance();
                year1=calender1.get(Calendar.YEAR);
                month1=calender1.get(Calendar.MONTH);
                dayOfMon1=calender1.get(Calendar.DAY_OF_MONTH);
                datePickerDialog=new DatePickerDialog(tViewOrderClick.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                String s=day+ "/"+ (month + 1) +"/"+year;
                                stext.setText(s);
                                accept.child("startDate").setValue(s);
                            }
                        }, year1,month1,dayOfMon1);
                        datePickerDialog.show();


            }
        });

        end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calender2=Calendar.getInstance();
                year2=calender2.get(Calendar.YEAR);
                month2=calender2.get(Calendar.MONTH);
                dayOfMon2=calender2.get(Calendar.DAY_OF_MONTH);
                datePickerDialog=new DatePickerDialog(tViewOrderClick.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                String s=day+ "/"+ (month + 1) +"/"+year;
                                endtext.setText(s);
                                accept.child("endDate").setValue(s);
                            }
                        }, year2,month2,dayOfMon2);
                datePickerDialog.show();

            }
        });



    }
}
