package com.example.fashiondesigner.order;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.fashiondesigner.R;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class tSuggestViewOrderClick extends AppCompatActivity {

    ArrayList measureData;
    PhotoView photoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_t_suggest_view_order_click);


        Intent intent = getIntent();
        String customerID = intent.getStringExtra("customerID");
        String orderKey = intent.getStringExtra("orderKey");
        String cName = intent.getStringExtra("cName");
        String email = intent.getStringExtra("email");
        String tele = intent.getStringExtra("tele");

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        final String userID = user.getUid();

        final TextView cNametv, cemailtv, cteletv, addresstv;

        cNametv = findViewById(R.id.cName);
        cemailtv = findViewById(R.id.cemail);
        cteletv = findViewById(R.id.ctele);
        addresstv = findViewById(R.id.address);

        final TextView orderID = findViewById(R.id.orderID);
        final TextView textViewFront = findViewById(R.id.frontText);
        final TextView textViewBack = findViewById(R.id.backText);
        final TextView addText = findViewById(R.id.textData);
        final TextView colourName = findViewById(R.id.colourName);

        final ImageView imageFront = findViewById(R.id.front);
        final ImageView imageBack = findViewById(R.id.back);
        final ImageView fabricImage = findViewById(R.id.fabrciImage);


        cNametv.setText(cName.toUpperCase());
        cemailtv.setText(email);
        cteletv.setText(tele);

        final TextView addTextTV = findViewById(R.id.addText);
        final ImageView addImageIV = findViewById(R.id.addImage);

        final TextView mtextTV = findViewById(R.id.mtext);
        photoView = findViewById(R.id.photo);

        final Button deliver = findViewById(R.id.deliver);
        final TextView track = findViewById(R.id.track);

        measureData = new ArrayList();


        final DatabaseReference add = FirebaseDatabase.getInstance().getReference().child(customerID).child("order").child(orderKey);

        deliver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                add.child("deliver").setValue("1");
                track.setText("ON DELIVERING");

            }
        });


        add.child("orderState").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshotOS) {

                if (dataSnapshotOS.exists()) {
                    String stateCheck = dataSnapshotOS.getValue().toString();

                    if (stateCheck.equals("2")) {
                        add.child("price").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshotP) {

                                String price = dataSnapshotP.getValue().toString();

                                track.setText("PAID $ " + price);


                                deliver.setVisibility(View.GONE);

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    } else if (stateCheck.equals("1")) {
                        add.child("deliver").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshotDL) {

                                if (dataSnapshotDL.exists()) {
                                    String deliverD = dataSnapshotDL.getValue().toString();

                                    if (deliverD.equals("1")) {
                                        track.setText("ON DELIVERING");
                                    }
                                } else {
                                    track.setText("ON TAILERING");
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        add.child("deliverAddress").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String name = dataSnapshot.child("name").getValue().toString();
                String tele = dataSnapshot.child("tele").getValue().toString();
                String home = dataSnapshot.child("home").getValue().toString();
                String street = dataSnapshot.child("street").getValue().toString();
                String city = dataSnapshot.child("city").getValue().toString();
                String state = dataSnapshot.child("state").getValue().toString();
                String country = dataSnapshot.child("country").getValue().toString();
                String zip = dataSnapshot.child("zip").getValue().toString();

                String built = name.toUpperCase() + "\n\n" + tele + "\n\n" + home + "\n\n" + street + "\n\n" + city + "\n\n" + state + "\n\n" + country + "\n\n" + zip;
                addresstv.setText(built);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        add.child("orderItemData").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshotOID) {

                if (dataSnapshotOID.exists()) {
                    if (dataSnapshotOID.child("frontImage").exists()) {
                        String imagef = dataSnapshotOID.child("frontImage").getValue().toString();
                        Glide.with(imageFront.getContext())
                                .load(imagef)
                                .into(imageFront);


                    } else {
                        imageFront.setVisibility(View.GONE);
                        textViewFront.setVisibility(View.GONE);
                    }

                    if (dataSnapshotOID.child("backImage").exists()) {
                        String imageb = dataSnapshotOID.child("backImage").getValue().toString();
                        Glide.with(imageBack.getContext())
                                .load(imageb)
                                .into(imageBack);
                    } else {
                        imageBack.setVisibility(View.GONE);
                        textViewBack.setVisibility(View.GONE);
                    }

                    if (dataSnapshotOID.child("text").exists()) {
                        String text = dataSnapshotOID.child("text").getValue().toString();
                        addText.setText(text);

                    } else {
                        addText.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        add.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshotOD) {

                if (dataSnapshotOD.exists()) {
                    String fabricKey = dataSnapshotOD.child("fabricData").child("fabrciKey").getValue().toString();
                    String colourKey = dataSnapshotOD.child("fabricData").child("colourKey").getValue().toString();

                    String colour = dataSnapshotOD.child("fabricData").child("addFire").getValue().toString();

                    String orderTime = dataSnapshotOD.child("orderTime").getValue().toString();


                    orderID.setText("Oder ID : " + orderTime);

                    colourName.setText(colour);

                    String tailoruserID=dataSnapshotOD.child("tailorID").getValue().toString();


                    final DatabaseReference itemData = FirebaseDatabase.getInstance().getReference().child(tailoruserID);
                    itemData.child("fabric").child("colours").child(fabricKey).child(colourKey).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshotCI) {

                            String colourImage = dataSnapshotCI.child("imageUrl").getValue().toString();

                            Glide.with(fabricImage.getContext())
                                    .load(colourImage)
                                    .into(fabricImage);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                    add.child("gender").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshotG) {
                            String  gender=dataSnapshotG.getValue().toString();
                            if(gender.equals("0"))
                            {
                                photoView.setImageResource(R.drawable.women);
                            }
                            else if(gender.equals("1"))
                            {
                                photoView.setImageResource(R.drawable.men);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });


                    String finalMeasurementData=dataSnapshotOD.child("finalMeasurementData").getValue().toString();

                    mtextTV.setText(finalMeasurementData);



                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        add.child("addAdditional").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshotx) {
                if(dataSnapshotx.exists())
                {
                    if(dataSnapshotx.child("text").exists())
                    {
                        String text=dataSnapshotx.child("text").getValue().toString();
                        addTextTV.setText(text);

                    }
                    if(dataSnapshotx.child("imageUrl").exists())
                    {
                        String cimage=dataSnapshotx.child("imageUrl").getValue().toString();
                        Glide.with(addImageIV.getContext())
                                .load(cimage)
                                .into(addImageIV);

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

}
