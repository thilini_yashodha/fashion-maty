package com.example.fashiondesigner.Adapter.Model;

import java.util.ArrayList;

public class searchModel {


    public String name="",shopName,country,city,priceRange,imageUrl,tailorName,category,subCategory,user_ID,featuresKey,pushID,commonItemName,totalPrice,measureState;

    public String check,orderKey,businessName,starRev="0",starRevCount="0.0";
    float starCount= (float) 0.0;

    boolean suggest=false,showStar=false;

    public ArrayList<Integer> featuresArr;

    public ArrayList<Integer> getFeaturesArr() {
        return featuresArr;
    }

    public void setFeaturesArr(ArrayList<Integer> featuresArr) {
        this.featuresArr = featuresArr;
    }


    public String getPushID() {
        return pushID;
    }

    public void setPushID(String pushID) {
        this.pushID = pushID;
    }


    public String getTitle() {
        return name;
    }

    public void setTitle(String name) {
        this.name = name;
    }

    public String getImage() {
        return imageUrl;
    }

    public void setImage(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getShopName() {
        return shopName;
    }

    public void setshopName(String shopName) {
        this.shopName = shopName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPrice() {
        return priceRange;
    }

    public void setPrice(String priceRange) {
        this.priceRange = priceRange;
    }

    public String getTailor() {
        return tailorName;
    }

    public void setTailor(String tailorName) {
        this.tailorName = tailorName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getUser_ID() {
        return user_ID;
    }

    public void setUser_ID(String user_ID) {
        this.user_ID = user_ID;
    }

    public String getCheck() {
        return check;
    }

    public void setCheck(String check) {
        this.check = check;
    }


    public String getFeaturesKey() {
        return featuresKey;
    }

    public void setFeaturesKey(String featuresKey) {
        this.featuresKey = featuresKey;
    }


    public String getCommonItemName() {
        return commonItemName;
    }

    public void setCommonItemName(String commonItemName) {
        this.commonItemName = commonItemName;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getOrderKey() {
        return orderKey;
    }

    public void setOrderKey(String orderKey) {
        this.orderKey = orderKey;
    }

    public boolean getSuggest() {
        return suggest;
    }

    public void setSuggest(boolean suggest) {
        this.suggest = suggest;
    }

    public String getMeasureState() {
        return measureState;
    }

    public void setMeasureState(String measureState) {
        this.measureState = measureState;
    }

    public String getStarRev() {
        return starRev;
    }

    public void setStarRev(String starRev) {
        this.starRev = starRev;
    }

    public float getStarCount() {
        return starCount;
    }

    public void setStarCount(float starCount) {
        this.starCount = starCount;
    }

    public boolean getShowStar() {
        return showStar;
    }

    public void setShowStar(boolean showStar) {
        this.showStar = showStar;
    }

    public String getStarRevCount() {
        return starRevCount;
    }

    public void setStarRevCount(String starRevCount) {
        this.starRevCount = starRevCount;
    }
}
