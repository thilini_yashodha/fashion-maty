package com.example.fashiondesigner.Adapter.Model;

public class measurement {

    String question, answer;

    boolean getMeasurement=false,suggestMnt=false;

    public boolean getGetMeasurement() {
        return getMeasurement;
    }

    public void setGetMeasurement(boolean getMeasurement) {
        this.getMeasurement = getMeasurement;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public boolean getSuggestMnt() {
        return suggestMnt;
    }

    public void setSuggestMnt(boolean suggestMnt) {
        this.suggestMnt = suggestMnt;
    }
}
