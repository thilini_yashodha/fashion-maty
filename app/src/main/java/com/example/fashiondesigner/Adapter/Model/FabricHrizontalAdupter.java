package com.example.fashiondesigner.Adapter.Model;

import android.content.Context;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.fashiondesigner.Adapter.HorizontalREcyclerViewAdapter;
import com.example.fashiondesigner.R;
import com.example.fashiondesigner.fabric;

import java.util.ArrayList;

public class FabricHrizontalAdupter extends RecyclerView.Adapter<FabricHrizontalAdupter.HorizontalViewHolder>
{

    Context context;
    ArrayList<HorizontalModel> arrayList;


    public  FabricHrizontalAdupter(Context context,ArrayList<HorizontalModel> arrayList)
    {
        this.context=context;
        this.arrayList=arrayList;
    }



    @Override
    public FabricHrizontalAdupter.HorizontalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_horizontal,parent,false);
        return new FabricHrizontalAdupter.HorizontalViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final FabricHrizontalAdupter.HorizontalViewHolder holder, int position) {

        final HorizontalModel horizontalModel=arrayList.get(position);

        String str = horizontalModel.getName();
        String[] strArray = str.split(" ");
        StringBuilder builder = new StringBuilder();
        for (String s : strArray) {
            String cap = s.substring(0, 1).toUpperCase() + s.substring(1);
            builder.append(cap + " ");
        }
        String data=builder.toString();

        if(horizontalModel.getPattern()!=null)
        {
            String str1 = horizontalModel.getPattern();
            String[] strArray1 = str1.split(" ");
            StringBuilder builder1 = new StringBuilder();
            for (String s1 : strArray1) {
                String cap1 = s1.substring(0, 1).toUpperCase() + s1.substring(1);
                builder1.append(cap1 + " ");
            }

            data=data+"\n"+builder1.toString();
        }


        holder.textViewTitle.setText(data);

        Glide.with(holder.imageViewThumb.getContext())
                .load(horizontalModel.getImageUrl())
                .into(holder.imageViewThumb);

        holder.imageViewThumb.setScaleType(ImageView.ScaleType.CENTER_CROP);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horizontalModel.setSelectHorizontal(true);
                Toast.makeText(context,horizontalModel.getName(),Toast.LENGTH_SHORT).show();
                ((fabric) context).functionToRun(horizontalModel.getPushID());

            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class HorizontalViewHolder extends RecyclerView.ViewHolder
    {

        TextView textViewTitle;
        ImageView imageViewThumb;


        public HorizontalViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewTitle=itemView.findViewById(R.id.txtTitle2);
            imageViewThumb=itemView.findViewById(R.id.ivThumb);
        }
    }


}
