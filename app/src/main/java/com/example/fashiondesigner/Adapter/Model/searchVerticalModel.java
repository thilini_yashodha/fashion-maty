package com.example.fashiondesigner.Adapter.Model;

import java.util.ArrayList;

public class searchVerticalModel {

    String title,click,clickItemName,ID,subClick,key;
    ArrayList<searchHorizontalModel> arrayList;

    Boolean selectingHorizontal=false;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getClick() {
        return click;
    }

    public void setClick(String click) {
        this.click = click;
    }

    public String getClickItemName() {
        return clickItemName;
    }

    public void setClickItemName(String clickItemName) {
        this.clickItemName = clickItemName;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getsubClick() {
        return subClick;
    }

    public void setsubClick(String subClick) {
        this.subClick = subClick;
    }

    public ArrayList<searchHorizontalModel> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<searchHorizontalModel> arrayList) {
        this.arrayList = arrayList;
    }

    public void setSelectingHorizontal(boolean selectingHorizontal){
        this.selectingHorizontal = selectingHorizontal;
    }

    public boolean getSelectingHorizontal()
    {
        return selectingHorizontal;
    }
}
