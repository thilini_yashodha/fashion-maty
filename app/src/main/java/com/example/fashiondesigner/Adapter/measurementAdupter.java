package com.example.fashiondesigner.Adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fashiondesigner.Adapter.Model.measurement;
import com.example.fashiondesigner.Adapter.Model.searchModel;
import com.example.fashiondesigner.R;
import com.example.fashiondesigner.measurements.cMeasurement;
import com.example.fashiondesigner.suggest.customerMeasurement;

import java.util.ArrayList;

public class measurementAdupter extends RecyclerView.Adapter<measurementAdupter.measurementViewHolder>
{
    Context context;
    ArrayList<measurement> arrayList;

    public measurementAdupter(Context context, ArrayList<measurement> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public measurementViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_measurement,parent,false);
        return new measurementAdupter.measurementViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final measurementViewHolder holder, int position) {

        final measurement mModel=arrayList.get(position);


        holder.question.setText(mModel.getQuestion());



        holder.answer.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                String ans = holder.answer.getText().toString();
                if(mModel.getSuggestMnt())
                {
                    ((customerMeasurement)context).goTOGetMeasuremet(ans,mModel.getQuestion());
                }
                else
                {
                    ((cMeasurement)context).goTOGetMeasuremet(ans,mModel.getQuestion());
                }


            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class measurementViewHolder extends RecyclerView.ViewHolder
    {
        TextView question;
        EditText answer;

        public measurementViewHolder(@NonNull View itemView) {
            super(itemView);

            question=itemView.findViewById(R.id.question);
            answer=itemView.findViewById(R.id.answer);

        }
    }
}
