package com.example.fashiondesigner.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fashiondesigner.Adapter.Model.HorizontalModel;
import com.example.fashiondesigner.Adapter.Model.VerticalModel;
import com.example.fashiondesigner.R;
import com.example.fashiondesigner.customerSearch.recommendColour;
import com.example.fashiondesigner.customerSearch.searchColour;
import com.example.fashiondesigner.suggest.selectFabric;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class searchColouAdupter extends RecyclerView.Adapter<searchColouAdupter.scolourViewHolder> {


    Context context;
    ArrayList<VerticalModel> arrayList;

    public searchColouAdupter(Context context, ArrayList<VerticalModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public scolourViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_vertcal,parent,false);
        return new searchColouAdupter.scolourViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final scolourViewHolder holder, int position) {

        final VerticalModel verticalModel=arrayList.get(position);
        String title=verticalModel.getTitle();

        holder.textViewTitle.setText(title);

        DatabaseReference mdataRef= FirebaseDatabase.getInstance().getReference().child(verticalModel.getID()).child("fabric").child("colours").child(verticalModel.getFabricPushID());
        final ArrayList<HorizontalModel> a = new ArrayList<>();

        mdataRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                a.clear();
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    HorizontalModel data= dataSnapshot1.getValue(HorizontalModel.class);

                    a.add(data);

                }

                verticalModel.setArrayList(a);

               HorizontalREcyclerViewAdapter horizontalREcyclerViewAdapter=new HorizontalREcyclerViewAdapter(context,verticalModel.getArrayList(),"1");

                holder.recyclerView.setHasFixedSize(true);
                holder.recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false));
                holder.recyclerView.setAdapter(horizontalREcyclerViewAdapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

       // verticalModel.setArrayList(a);

       // final HorizontalREcyclerViewAdapter horizontalREcyclerViewAdapter=new HorizontalREcyclerViewAdapter(context,verticalModel.getArrayList());


      //  holder.recyclerView.setAdapter(horizontalREcyclerViewAdapter);


        holder.select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i=0;i<a.size();i++)
                {
                    if(a.get(i).getSelectHorizontal())
                    {
                        verticalModel.setKeySel(true);
                        verticalModel.setKey(a.get(i).getPushID());

                        if(verticalModel.getrColour())
                        {
                            ((recommendColour) context).SelectToRun();
                        }
                        else if(verticalModel.getFselect())
                        {
                            ((selectFabric) context).SelectToRun();
                        }
                        else
                        {
                            ((searchColour) context).SelectToRun(a.get(i).getImageUrl());
                        }

                        a.get(i).setSelectHorizontal(false);
                        break;
                    }
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class scolourViewHolder extends RecyclerView.ViewHolder
    {
        RecyclerView recyclerView;
        TextView textViewTitle;
        Button select;
        public scolourViewHolder(@NonNull View itemView) {
            super(itemView);

            recyclerView=itemView.findViewById(R.id.recyclerview2);
            textViewTitle=itemView.findViewById(R.id.textTitle);
            select=itemView.findViewById(R.id.Select);
        }
    }
}
