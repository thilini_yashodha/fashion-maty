package com.example.fashiondesigner.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.fashiondesigner.Adapter.Model.searchHorizontalModel;
import com.example.fashiondesigner.R;
import com.example.fashiondesigner.customerSearch.searchColour;

import java.util.ArrayList;

public class searchHorizontalAdupter extends RecyclerView.Adapter<searchHorizontalAdupter.sHorizontalViewHolder> {

    Context context;
    ArrayList<searchHorizontalModel> arrayList;

    public searchHorizontalAdupter(Context context, ArrayList<searchHorizontalModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public sHorizontalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_horizontal,parent,false);
        return new searchHorizontalAdupter.sHorizontalViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull sHorizontalViewHolder holder, int position) {

        final searchHorizontalModel sHorizontalModel=arrayList.get(position);

        String str = sHorizontalModel.getName();
        String[] strArray = str.split(" ");
        StringBuilder builder = new StringBuilder();
        for (String s : strArray) {
            String cap = s.substring(0, 1).toUpperCase() + s.substring(1);
            builder.append(cap + " ");
        }

        holder.textViewTitle.setText(builder.toString());

        Glide.with(holder.imageViewThumb.getContext())
                .load(sHorizontalModel.getImageUrl())
                .into(holder.imageViewThumb);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sHorizontalModel.getSelect())
                {
                    sHorizontalModel.setSelectHorizontal(true);
                    Toast.makeText(context,sHorizontalModel.getName(),Toast.LENGTH_SHORT).show();
                    ((searchColour) context).functionToRun(sHorizontalModel.getPushID());
                }
                else
                {
                    sHorizontalModel.setSelectHorizontal(true);
                    Toast.makeText(context,sHorizontalModel.getName(),Toast.LENGTH_SHORT).show();
                }
            }
        });





    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class sHorizontalViewHolder extends RecyclerView.ViewHolder
    {
        TextView textViewTitle;
        ImageView imageViewThumb;

        public sHorizontalViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewTitle=itemView.findViewById(R.id.txtTitle2);
            imageViewThumb=itemView.findViewById(R.id.ivThumb);
        }
    }
}
