package com.example.fashiondesigner.Adapter.Model;

import android.graphics.Bitmap;
import android.net.Uri;

import java.util.ArrayList;

public class VerticalModel {

    String title,pushID,fabricPushID;

    ArrayList<HorizontalModel> arrayList;
    boolean selecting=false,more=false,rColour=false,fselect=false;

    boolean moreAduter=false;

    String click,clickItemName,ID,subClick;
    Uri uri;

    Bitmap bitmap;

   String key;
   boolean keySel=false;

   String path;

    public boolean getrColour() {
        return rColour;
    }

    public void setrColour(boolean rColour) {
        this.rColour = rColour;
    }

    public boolean getMoreAduter() {
        return moreAduter;
    }

    public void setMoreAduter(boolean moreAduter) {
        this.moreAduter = moreAduter;
    }

    public VerticalModel(){
        arrayList = new ArrayList<>();
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<HorizontalModel> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<HorizontalModel> arrayList) {
        this.arrayList = arrayList;
    }
    public void setSelecting(boolean selecting){
        this.selecting = selecting;
    }

    public boolean getSelecting()
    {
        return selecting;
    }


    public String getClick() {
        return click;
    }

    public void setClick(String click) {
        this.click = click;
    }

    public String getClickItemName() {
        return clickItemName;
    }

    public void setClickItemName(String clickItemName) {
        this.clickItemName = clickItemName;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setKeySel(boolean keySel){
        this.keySel = keySel;
    }

    public boolean getKeySel()
    {
        return keySel;
    }


    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getsubClick() {
        return subClick;
    }

    public void setsubClick(String subClick) {
        this.subClick = subClick;
    }


    public void setMore(boolean more){
        this.more = more;
    }

    public boolean getMore()
    {
        return more;
    }

    public String getFabricPushID() {
        return fabricPushID;
    }

    public void setFabricPushID(String fabricPushID) {
        this.fabricPushID = fabricPushID;
    }

    public String getPushID() {
        return pushID;
    }

    public void setPushID(String pushID) {
        this.pushID = pushID;
    }

    public boolean getFselect() {
        return fselect;
    }

    public void setFselect(boolean fselect) {
        this.fselect = fselect;
    }
}
