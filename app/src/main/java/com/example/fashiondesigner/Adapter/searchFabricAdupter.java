package com.example.fashiondesigner.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.fashiondesigner.Adapter.Model.searchHorizontalModel;
import com.example.fashiondesigner.R;
import com.example.fashiondesigner.customerSearch.recommendColour;
import com.example.fashiondesigner.customerSearch.searchColour;
import com.example.fashiondesigner.suggest.selectFabric;

import java.util.ArrayList;

public class searchFabricAdupter extends RecyclerView.Adapter<searchFabricAdupter.fViewHolder>
{
    Context context;
    ArrayList<searchHorizontalModel> arrayList;

    public searchFabricAdupter(Context context, ArrayList<searchHorizontalModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public fViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fabrci_row,parent,false);
        return new searchFabricAdupter.fViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull fViewHolder holder, int position) {

        final searchHorizontalModel sHorizontalModel=arrayList.get(position);

        String str = sHorizontalModel.getName();
        String[] strArray = str.split(" ");
        StringBuilder builder = new StringBuilder();
        for (String s : strArray) {
            String cap = s.substring(0, 1).toUpperCase() + s.substring(1);
            builder.append(cap + " ");
        }

        holder.fabric.setText(builder.toString());

        String fgd="jf";

        String strPattern = sHorizontalModel.getPattern();
        String[] strArrPattern = strPattern.split(" ");
        StringBuilder builderPattern = new StringBuilder();
        for (String s1 : strArrPattern) {
            String cap2 = s1.substring(0, 1).toUpperCase() + s1.substring(1);
            builderPattern.append(cap2 + " ");
        }

        holder.pattern.setText(builderPattern.toString());

        holder.price.setText("Cost for 1m : $ " +sHorizontalModel.getPrice());

        Glide.with(holder.image.getContext())
                .load(sHorizontalModel.getImageUrl())
                .into(holder.image);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(sHorizontalModel.getSelect())
                {
                    sHorizontalModel.setSelectHorizontal(true);
                    Toast.makeText(context,sHorizontalModel.getName(),Toast.LENGTH_SHORT).show();
                    ((recommendColour) context).functionToRun(sHorizontalModel.getPushID());
                }
                else if(sHorizontalModel.getSelectFabric())
                {
                    sHorizontalModel.setSelectHorizontal(true);
                    Toast.makeText(context,sHorizontalModel.getName(),Toast.LENGTH_SHORT).show();
                    ((selectFabric) context).functionToRun(sHorizontalModel.getPushID());
                }
                else
                {
                    sHorizontalModel.setSelectHorizontal(true);
                    Toast.makeText(context,sHorizontalModel.getName(),Toast.LENGTH_SHORT).show();
                    ((searchColour) context).functionToRun(sHorizontalModel.getPushID());

                }


            }
        });




    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class fViewHolder extends RecyclerView.ViewHolder
    {
        ImageView image;
        TextView fabric,pattern,price;

        public fViewHolder(@NonNull View itemView) {
            super(itemView);

            image=itemView.findViewById(R.id.image);
            fabric=itemView.findViewById(R.id.fabric);
            pattern=itemView.findViewById(R.id.pattern);
            price=itemView.findViewById(R.id.price);

        }
    }
}
