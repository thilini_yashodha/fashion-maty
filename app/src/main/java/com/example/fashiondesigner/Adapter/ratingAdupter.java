package com.example.fashiondesigner.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.fashiondesigner.Adapter.Model.HorizontalModel;
import com.example.fashiondesigner.Adapter.Model.searchModel;
import com.example.fashiondesigner.R;

import java.util.ArrayList;

public class ratingAdupter extends RecyclerView.Adapter<ratingAdupter.rateViewHolder> {

    Context context;
    ArrayList<HorizontalModel> arrayList;

    public ratingAdupter(Context context, ArrayList<HorizontalModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public rateViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rating_row,parent,false);
        return new ratingAdupter.rateViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull rateViewHolder holder, int position) {

        final HorizontalModel sModel=arrayList.get(position);

        if(sModel.getImageBack()!=null)
        {
            Glide.with(holder.commentImage.getContext())
                    .load(sModel.getImageBack())
                    .into(holder.commentImage);
            holder.commentImage.setVisibility(View.VISIBLE);
        }

        Glide.with(holder.profileImage.getContext())
                .load(sModel.getImageUrl())
                .into(holder.profileImage);

        holder.name.setText(sModel.getName());
        holder.comment.setText(sModel.getPattern());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class rateViewHolder extends RecyclerView.ViewHolder
    {
        ImageView commentImage,profileImage;
        TextView name,comment;

        public rateViewHolder(@NonNull View itemView) {
            super(itemView);

            commentImage=itemView.findViewById(R.id.rImage);
            profileImage=itemView.findViewById(R.id.imageView1);
            name=itemView.findViewById(R.id.name);
            comment=itemView.findViewById(R.id.comment);
        }
    }
}
