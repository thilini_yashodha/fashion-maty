package com.example.fashiondesigner.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.fashiondesigner.Adapter.Model.searchModel;
import com.example.fashiondesigner.Adapter.Model.tailorOrderModel;
import com.example.fashiondesigner.R;
import com.example.fashiondesigner.order.cViewOrderClick;
import com.example.fashiondesigner.order.tSuggestViewOrderClick;
import com.example.fashiondesigner.order.tViewOrderClick;

import java.util.ArrayList;

public class tailorOrderAdupter extends RecyclerView.Adapter<tailorOrderAdupter.tailorOrderViewHolder>
{

    Context context;
    ArrayList<tailorOrderModel> arrayList;

    public tailorOrderAdupter(Context context, ArrayList<tailorOrderModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }


    @NonNull
    @Override
    public tailorOrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_row,parent,false);
        return new tailorOrderAdupter.tailorOrderViewHolder(view);
    }




    @Override
    public void onBindViewHolder(@NonNull tailorOrderViewHolder holder, int position) {

        final tailorOrderModel sModel=arrayList.get(position);

        holder.title.setText(sModel.getItemName());
        holder.cName.setText(sModel.getCustomerName());
        holder.email.setText(sModel.getCemail()+"\n");
        holder.tele.setText(sModel.getCtele());


        Glide.with(holder.image.getContext())
                .load(sModel.getImageUrl())
                .into(holder.image);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (sModel.getSuggest())
                {
                    Intent intent = new Intent(v.getContext(), tSuggestViewOrderClick.class);
                    intent.putExtra("customerID",sModel.getCustomerID());
                    intent.putExtra("orderKey",sModel.getOrderKey());
                    intent.putExtra("cName",sModel.getCustomerName());
                    intent.putExtra("email",sModel.getCemail());
                    intent.putExtra("tele",sModel.getCtele());
                    context.startActivity(intent);
                }

                else
                {
                    Intent intent = new Intent(v.getContext(), tViewOrderClick.class);
                    intent.putExtra("customerID",sModel.getCustomerID());
                    intent.putExtra("orderKey",sModel.getOrderKey());
                    intent.putExtra("cName",sModel.getCustomerName());
                    intent.putExtra("email",sModel.getCemail());
                    intent.putExtra("tele",sModel.getCtele());
                    context.startActivity(intent);
                }


            }
        });

    }



    @Override
    public int getItemCount() {
        return arrayList.size();
    }




    public class tailorOrderViewHolder extends RecyclerView.ViewHolder
    {
        ImageView image;
        TextView title,cName,email,tele;


        public tailorOrderViewHolder(@NonNull View itemView) {
            super(itemView);

            image= itemView.findViewById(R.id.searchImage);
            title= itemView.findViewById(R.id.search_title);
            cName= itemView.findViewById(R.id.search_item_shopName);
            email= itemView.findViewById(R.id.search_item_tailor);
            tele=itemView.findViewById(R.id.search_item_country);

        }
    }
}
