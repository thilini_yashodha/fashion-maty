package com.example.fashiondesigner.Adapter;

import android.content.Context;
import android.net.Uri;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.fashiondesigner.Adapter.Model.HorizontalModel;
import com.example.fashiondesigner.R;
import com.example.fashiondesigner.verticalHorizontalList;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HorizontalREcyclerViewAdapter extends RecyclerView.Adapter<HorizontalREcyclerViewAdapter.HorizontalViewHolder>
{

    Context context;
    ArrayList<HorizontalModel> arrayList;
    String fabCheck="";


    public  HorizontalREcyclerViewAdapter(Context context,ArrayList<HorizontalModel> arrayList)
    {
        this.context=context;
        this.arrayList=arrayList;
    }

    public  HorizontalREcyclerViewAdapter(Context context,ArrayList<HorizontalModel> arrayList,String x)
    {
        this.context=context;
        this.arrayList=arrayList;
        this.fabCheck=x;
    }



    @Override
    public HorizontalViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {

       View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_horizontal,parent,false);
        return new HorizontalViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final HorizontalViewHolder holder, int position) {

        final HorizontalModel horizontalModel=arrayList.get(position);

        String str = horizontalModel.getName();
        String[] strArray = str.split(" ");
        StringBuilder builder = new StringBuilder();
        for (String s : strArray) {
            String cap = s.substring(0, 1).toUpperCase() + s.substring(1);
            builder.append(cap + " ");
        }

        holder.textViewTitle.setText(builder.toString());

        Glide.with(holder.imageViewThumb.getContext())
                .load(horizontalModel.getImageUrl())
                .into(holder.imageViewThumb);

        if(fabCheck.equals("1"))
        {
            holder.imageViewThumb.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }
        else
        {
            holder.imageViewThumb.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horizontalModel.setSelectHorizontal(true);
                Toast.makeText(context,horizontalModel.getName(),Toast.LENGTH_SHORT).show();



            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class HorizontalViewHolder extends RecyclerView.ViewHolder
    {

        TextView textViewTitle;
        ImageView imageViewThumb;


        public HorizontalViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewTitle=itemView.findViewById(R.id.txtTitle2);
            imageViewThumb=itemView.findViewById(R.id.ivThumb);

        }
    }
}
