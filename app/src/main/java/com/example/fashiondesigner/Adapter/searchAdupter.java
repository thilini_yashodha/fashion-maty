package com.example.fashiondesigner.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.fashiondesigner.Adapter.Model.searchModel;
import com.example.fashiondesigner.R;
import com.example.fashiondesigner.customerSearch.searchClickItem;
import com.example.fashiondesigner.customerSearch.searchTailor;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import java.util.ArrayList;

public class searchAdupter extends RecyclerView.Adapter<searchAdupter.searchViewHolder>
{


    Context context;
    ArrayList<searchModel> arrayList;

    public  searchAdupter(Context context,ArrayList<searchModel> arrayList)
    {
        this.context=context;
        this.arrayList=arrayList;
    }


    @NonNull
    @Override
    public searchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_row,parent,false);
        return new searchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final searchViewHolder holder, int position) {

        final searchModel sModel=arrayList.get(position);

        final String x=sModel.getCheck();


        if((x.equals("shop")))
        {

                final DatabaseReference mdata= FirebaseDatabase.getInstance().getReference().child(sModel.getUser_ID()).child("tailorData");
                mdata.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String imageUrl=dataSnapshot.child("imageuri").getValue().toString();
                        Glide.with(holder.image.getContext())
                                .load(imageUrl)
                                .into(holder.image);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

                holder.title.setText(sModel.getShopName().toUpperCase());
                holder.shopName.setText("BY " +sModel.getTailor().toUpperCase());
                holder.tailor.setText(sModel.getCountry().toUpperCase());
                holder.country.setText(sModel.getCity());
                holder.city.setVisibility(View.GONE);
                holder.priceRange.setVisibility(View.GONE);
                holder.gender.setVisibility(View.GONE);

            if (sModel.getShowStar())
            {
                holder.starBar2.setVisibility(View.VISIBLE);
                holder.starBar2.setRating(sModel.getStarCount());
                holder.starBar.setVisibility(View.GONE);

            }

        }

       else if((x.equals("subCategory")))
        {
            if (sModel.getShowStar())
            {
                holder.starBar.setVisibility(View.VISIBLE);
                holder.starBar.setRating(sModel.getStarCount());

            }

            holder.title.setText(sModel.getCommonItemName());
            holder.shopName.setText(sModel.getShopName().toUpperCase());
            holder.tailor.setText("BY " +sModel.getTailor().toUpperCase());
            holder.country.setText(sModel.getCountry());
            holder.city.setText(" - "+sModel.getCity());
            holder.priceRange.setText("MEASURING COST : $ "+sModel.getPrice());

            Glide.with(holder.image.getContext())
                    .load(sModel.getImage())
                    .into(holder.image);


            DatabaseReference mdata=FirebaseDatabase.getInstance().getReference().child(sModel.getUser_ID()).child("measurements").child(sModel.getPushID()).child("gender");
            mdata.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists())
                    {
                        String genders=dataSnapshot.getValue().toString();

                        if(genders.equals("0"))
                        {
                            holder.gender.setVisibility(View.VISIBLE);
                            holder.gender.setText("FOR WOMEN");
                        }
                        else if(genders.equals("1"))
                        {
                            holder.gender.setVisibility(View.VISIBLE);
                            holder.gender.setText("FOR MEN");
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });


        }


       holder.itemView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

               if((x.equals("shop")))
               {
                   Intent intent = new Intent(v.getContext(), searchTailor.class);
                   intent.putExtra("userID",sModel.getUser_ID());
                   context.startActivity(intent);
               }

               else if((x.equals("subCategory")))
               {
                   Intent intent = new Intent(v.getContext(), searchClickItem.class);
                   intent.putExtra("imageUrl",sModel.getImage());

                  // String user=sModel.getUser_ID();
                   intent.putExtra("userID",sModel.getUser_ID());
                   intent.putExtra("category",sModel.getCategory());
                   intent.putExtra("subCategory",sModel.getSubCategory());
                  // intent.putExtra("itemName",sModel.getTitle());
                   intent.putExtra("itemName",sModel.getCommonItemName());
                   intent.putExtra("priceRange",sModel.getPrice());
                   intent.putExtra("pushID",sModel.getPushID());
                   context.startActivity(intent);
               }



           }
       });








    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class searchViewHolder extends RecyclerView.ViewHolder
    {

        ImageView image;
        TextView title,shopName,tailor,country,city,priceRange,gender;
        RatingBar starBar,starBar2;


        public searchViewHolder(@NonNull View itemView) {
            super(itemView);


            image= itemView.findViewById(R.id.searchImage);
            title= itemView.findViewById(R.id.search_title);
            shopName= itemView.findViewById(R.id.search_item_shopName);
            tailor= itemView.findViewById(R.id.search_item_tailor);
            country=itemView.findViewById(R.id.search_item_country);
            city=itemView.findViewById(R.id.city);
            priceRange=itemView.findViewById(R.id.price);
            starBar=itemView.findViewById(R.id.ratingStar);
            starBar2=itemView.findViewById(R.id.ratingStar2);
            gender=itemView.findViewById(R.id.gender);


        }
    }

}

