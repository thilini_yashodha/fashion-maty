package com.example.fashiondesigner.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.fashiondesigner.Adapter.Model.searchViewModel;
import com.example.fashiondesigner.R;
import com.example.fashiondesigner.customerSearch.searchColour;
import com.example.fashiondesigner.customerSearch.showColourFulldress;

import java.util.ArrayList;


public class searchFulldressAdupter extends RecyclerView.Adapter<searchFulldressAdupter.searchFulldressViewHolder> {

    Context context;
    ArrayList<searchViewModel> arrayList;

    public searchFulldressAdupter(Context context, ArrayList<searchViewModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public searchFulldressViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_view_item,parent,false);
        return new searchFulldressAdupter.searchFulldressViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull searchFulldressViewHolder holder, int position) {

        final searchViewModel sModel=arrayList.get(position);

        holder.title.setText(sModel.getName().toUpperCase());

        if(sModel.getColour())
        {
            holder.price.setText(sModel.getPrice());
        }
        else
        {
            holder.price.setText("$ " + sModel.getPrice());
        }

        Glide.with(holder.image.getContext())
                .load(sModel.getImageUrl())
                .into(holder.image);


            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(sModel.getColour())
                    {

                      //  ((showColourFulldress)context).goToOrder(sModel.getImageUrl());
                        ((showColourFulldress)context).goToOrder(sModel.getImageUrl(),sModel.getFabricKey(),sModel.getColourKey());
                    }
                    else if(sModel.gettColourFull())
                    {
                        
                    }
                    else
                    {
                        Intent intent = new Intent(v.getContext(), searchColour.class);
                        intent.putExtra("userID",sModel.getID());
                        intent.putExtra("category",sModel.getCategory());
                        intent.putExtra("subCategory",sModel.getSubCategory());
                        intent.putExtra("itemName",sModel.getItemName());
                        intent.putExtra("pushID",sModel.getPushID());
                       // intent.putExtra("fullDressImage",sModel.getImageUrl());
                        context.startActivity(intent);
                    }


                }
            });




    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class searchFulldressViewHolder extends RecyclerView.ViewHolder
    {
        ImageView image;
        TextView title,price;

        public searchFulldressViewHolder(@NonNull View itemView) {
            super(itemView);


            image= itemView.findViewById(R.id.imageView);
            title= itemView.findViewById(R.id.textView);
            price= itemView.findViewById(R.id.textPrice);
        }
    }
}


