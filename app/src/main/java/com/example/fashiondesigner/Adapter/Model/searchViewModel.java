package com.example.fashiondesigner.Adapter.Model;

import java.util.ArrayList;

public class searchViewModel {

   String imageUrl,name,price,pushID,fabricKey,colourKey;
   String category,itemName,ID,subCategory;

    boolean search=false,colour=false,tColourFull=false;

    public boolean gettColourFull() {
        return tColourFull;
    }

    public void settColourFull(boolean tColourFull) {
        this.tColourFull = tColourFull;
    }

    public String getFabricKey() {
        return fabricKey;
    }

    public void setFabricKey(String fabricKey) {
        this.fabricKey = fabricKey;
    }

    public String getColourKey() {
        return colourKey;
    }

    public void setColourKey(String colourKey) {
        this.colourKey = colourKey;
    }


    public boolean getColour() {
        return colour;
    }

    public void setColour(boolean colour) {
        this.colour = colour;
    }

    public boolean getSearch() {
        return search;
    }

    public void setSearch(boolean search) {
        this.search = search;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }





    public String getPushID() {
        return pushID;
    }

    public void setPushID(String pushID) {
        this.pushID = pushID;
    }

    ArrayList<String> featuresArray;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public ArrayList<String> getFeaturesArray() {
        return featuresArray;
    }

    public void setFeaturesArray(ArrayList<String> featuresArray) {
        this.featuresArray = featuresArray;
    }
}
