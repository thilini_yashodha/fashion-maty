package com.example.fashiondesigner.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fashiondesigner.Adapter.Model.HorizontalModel;
import com.example.fashiondesigner.Adapter.Model.VerticalModel;
import com.example.fashiondesigner.Adapter.Model.searchVerticalModel;
import com.example.fashiondesigner.R;
import com.example.fashiondesigner.fabric;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class FabricVerticalAdupter extends RecyclerView.Adapter<FabricVerticalAdupter.fVerticalViewHolder> {

    Context context;
    ArrayList<VerticalModel> arrayList;

    public FabricVerticalAdupter(Context context, ArrayList<VerticalModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }


    @NonNull
    @Override
    public fVerticalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_vertical,parent,false);
        return new FabricVerticalAdupter.fVerticalViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final fVerticalViewHolder holder, int position) {

        final VerticalModel verticalModel=arrayList.get(position);
        String title=verticalModel.getTitle();

        holder.textViewTitle.setText(title);

       DatabaseReference mdataRef= FirebaseDatabase.getInstance().getReference().child(verticalModel.getID()).child("fabric").child("colours").child(verticalModel.getFabricPushID());
        final ArrayList<HorizontalModel> a = new ArrayList<>();

        mdataRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                a.clear();
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    HorizontalModel data= dataSnapshot1.getValue(HorizontalModel.class);

                    a.add(data);

                }
                verticalModel.setArrayList(a);

                HorizontalREcyclerViewAdapter horizontalREcyclerViewAdapter=new HorizontalREcyclerViewAdapter(context,verticalModel.getArrayList(),"1");
                holder.recyclerView.setHasFixedSize(true);
                 holder.recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false));

                holder.recyclerView.setAdapter(horizontalREcyclerViewAdapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        verticalModel.setArrayList(a);

      //  final HorizontalREcyclerViewAdapter horizontalREcyclerViewAdapter=new HorizontalREcyclerViewAdapter(context,verticalModel.getArrayList(),"1");

     //   holder.recyclerView.setHasFixedSize(true);
      //  holder.recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false));
      //  holder.recyclerView.setAdapter(horizontalREcyclerViewAdapter);



        holder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                verticalModel.setMore(true);
                ((fabric) context).MoreToRun(verticalModel.getFabricPushID());

            }
        });

        holder.select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i=0;i<a.size();i++)
                {
                    if(a.get(i).getSelectHorizontal())
                    {
                        verticalModel.setKeySel(true);
                        verticalModel.setKey(a.get(i).getPushID());
                        ((fabric) context).SelectToRun(verticalModel.getFabricPushID(),a.get(i).getPushID());
                        verticalModel.setKeySel(false);
                    }
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class fVerticalViewHolder extends RecyclerView.ViewHolder
    {
        RecyclerView recyclerView;
        TextView textViewTitle;

        Button more,select;


        public fVerticalViewHolder(@NonNull View itemView) {
            super(itemView);

            recyclerView=itemView.findViewById(R.id.recyclerview2);
            textViewTitle=itemView.findViewById(R.id.textTitle);
            more=itemView.findViewById(R.id.btnMore);
            select=itemView.findViewById(R.id.select);

            select.setText("SET");
        }
    }


}
