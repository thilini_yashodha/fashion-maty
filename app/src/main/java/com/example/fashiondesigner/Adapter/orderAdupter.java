package com.example.fashiondesigner.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.fashiondesigner.Adapter.Model.searchModel;
import com.example.fashiondesigner.R;
import com.example.fashiondesigner.customerSearch.searchTailor;
import com.example.fashiondesigner.order.cSuggestViewOrderClick;
import com.example.fashiondesigner.order.cViewOrderClick;

import java.util.ArrayList;

public class orderAdupter extends RecyclerView.Adapter<orderAdupter.orderViewHolder>
{
    Context context;
    ArrayList<searchModel> arrayList;


    public orderAdupter(Context context, ArrayList<searchModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }



    @NonNull
    @Override
    public orderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_row,parent,false);
        return new orderAdupter.orderViewHolder(view);
    }




    @Override
    public void onBindViewHolder(@NonNull orderViewHolder holder, int position) {

        final searchModel sModel=arrayList.get(position);

        if(sModel.getTitle().equals(""))
        {
            holder.title.setText(sModel.getCommonItemName());
        }
        else
        {
            holder.title.setText(sModel.getTitle());
        }

        holder.shopName.setText(sModel.getShopName().toUpperCase());
        holder.tailor.setText("BY " +sModel.getTailor().toUpperCase());
        holder.country.setText(sModel.getCountry().toUpperCase());
        holder.city.setText(" - "+sModel.getCity().toUpperCase());
        holder.priceRange.setText("$ "+sModel.getTotalPrice());


        Glide.with(holder.image.getContext())
                .load(sModel.getImage())
                .into(holder.image);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(sModel.getSuggest())
                {
                    Intent intent = new Intent(v.getContext(), cSuggestViewOrderClick.class);
                    intent.putExtra("tailoruserID",sModel.getUser_ID());
                    intent.putExtra("orderKey",sModel.getOrderKey());
                    context.startActivity(intent);
                }
                else
                {
                    Intent intent = new Intent(v.getContext(), cViewOrderClick.class);
                    intent.putExtra("tailoruserID",sModel.getUser_ID());
                    intent.putExtra("orderKey",sModel.getOrderKey());
                    context.startActivity(intent);
                }


            }
        });

    }





    @Override
    public int getItemCount() {
        return arrayList.size();
    }




    public class orderViewHolder extends RecyclerView.ViewHolder
    {
        ImageView image;
        TextView title,shopName,tailor,country,city,priceRange;

        public orderViewHolder(@NonNull View itemView) {
            super(itemView);

            image= itemView.findViewById(R.id.searchImage);
            title= itemView.findViewById(R.id.search_title);
            shopName= itemView.findViewById(R.id.search_item_shopName);
            tailor= itemView.findViewById(R.id.search_item_tailor);
            country=itemView.findViewById(R.id.search_item_country);
            city=itemView.findViewById(R.id.city);
            priceRange=itemView.findViewById(R.id.price);
        }
    }

}
