package com.example.fashiondesigner.Adapter.Model;

public class HorizontalModel {

    String name,imageUrl,pushID,price,pattern="",imageBack=null;

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPushID() {
        return pushID;
    }

    public void setPushID(String pushID) {
        this.pushID = pushID;
    }

    Boolean selectHorizontal=false;



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setSelectHorizontal(boolean selectHorizontal){
        this.selectHorizontal = selectHorizontal;
    }

    public boolean getSelectHorizontal()
    {
        return selectHorizontal;
    }

    public String getImageBack() {
        return imageBack;
    }

    public void setImageBack(String imageBack) {
        this.imageBack = imageBack;
    }
}
