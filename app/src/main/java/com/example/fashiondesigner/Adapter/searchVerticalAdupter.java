package com.example.fashiondesigner.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fashiondesigner.Adapter.Model.searchHorizontalModel;
import com.example.fashiondesigner.Adapter.Model.searchVerticalModel;
import com.example.fashiondesigner.R;
import com.example.fashiondesigner.customerSearch.searchView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class searchVerticalAdupter extends RecyclerView.Adapter<searchVerticalAdupter.searchVerticalViewHolder> {

    Context context;
    ArrayList<searchVerticalModel> arrayList;
    boolean data;

    public searchVerticalAdupter(Context context, ArrayList<searchVerticalModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public searchVerticalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.search_vertcal,parent,false);
        return new searchVerticalAdupter.searchVerticalViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final searchVerticalViewHolder holder, int position) {

        final searchVerticalModel sVerticalModel=arrayList.get(position);
        holder.textViewTitle.setText(sVerticalModel.getTitle());

        DatabaseReference mdataRef= FirebaseDatabase.getInstance().getReference().child(sVerticalModel.getID()).child("item").child(sVerticalModel.getClick()).child(sVerticalModel.getsubClick()).child(sVerticalModel.getClickItemName());
        final ArrayList<searchHorizontalModel> a = new ArrayList<>();

        mdataRef.child(sVerticalModel.getTitle().toLowerCase()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                a.clear();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    searchHorizontalModel data= dataSnapshot1.getValue(searchHorizontalModel.class);

                    a.add(data);

                }
                sVerticalModel.setArrayList(a);

                searchHorizontalAdupter sHorizontalAdupter=new searchHorizontalAdupter(context,sVerticalModel.getArrayList());

               holder.recyclerView.setAdapter(sHorizontalAdupter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        sVerticalModel.setArrayList(a);

        final searchHorizontalAdupter shorizontalViewAdapter=new searchHorizontalAdupter(context,sVerticalModel.getArrayList());

        holder.recyclerView.setHasFixedSize(true);
        holder.recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false));
        holder.recyclerView.setAdapter(shorizontalViewAdapter);

        data=false;

        holder.select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i=0;i<a.size();i++)
                {
                    if(a.get(i).getSelectHorizontal())
                    {
                        String x=a.get(i).getName();
                        sVerticalModel.setKey(x);
                        sVerticalModel.setSelectingHorizontal(true);
                        ((searchView)context) .goSetect();
                        data=true;
                        a.get(i).setSelectHorizontal(false);
                        break;
                    }

                }

                if(!data)
                {
                    Toast.makeText(context,"Select a feature firs!",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class searchVerticalViewHolder extends RecyclerView.ViewHolder
    {

        RecyclerView recyclerView;
        TextView textViewTitle;
        Button select;

        public searchVerticalViewHolder(@NonNull View itemView) {
            super(itemView);

            recyclerView=itemView.findViewById(R.id.recyclerview2);
            textViewTitle=itemView.findViewById(R.id.textTitle);
            select=itemView.findViewById(R.id.Select);

        }
    }
}
