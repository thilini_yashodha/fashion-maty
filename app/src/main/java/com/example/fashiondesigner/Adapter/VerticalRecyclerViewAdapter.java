package com.example.fashiondesigner.Adapter;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fashiondesigner.Adapter.Model.HorizontalModel;
import com.example.fashiondesigner.Adapter.Model.VerticalModel;
import com.example.fashiondesigner.R;
import com.example.fashiondesigner.verticalHorizontalList;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.app.Activity.RESULT_CANCELED;

public class VerticalRecyclerViewAdapter extends RecyclerView.Adapter<VerticalRecyclerViewAdapter.VerticalViewHolder> {

    Context context;
    ArrayList<VerticalModel> arrayList;

   // private static final String IMAGE_DIRECTORY = "/demonuts";
    private int GALLERY = 1, CAMERA = 2;
    public Uri filePath;


    public VerticalRecyclerViewAdapter(Context context,ArrayList<VerticalModel> arrayList)
    {
        this.arrayList=arrayList;
        this.context=context;

    }



    @Override
    public VerticalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_vertical,parent,false);
       return new VerticalViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final VerticalViewHolder holder, int position) {

        final VerticalModel verticalModel=arrayList.get(position);
        final String title=verticalModel.getTitle();

        holder.textViewTitle.setText(title);

        final DatabaseReference mdataRef= FirebaseDatabase.getInstance().getReference().child(verticalModel.getID()).child("item").child(verticalModel.getClick()).child(verticalModel.getsubClick()).child(verticalModel.getClickItemName());
        final ArrayList<HorizontalModel> a = new ArrayList<>();

        mdataRef.child(verticalModel.getTitle().toLowerCase()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                a.clear();
                int i=0;
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    HorizontalModel data= dataSnapshot1.getValue(HorizontalModel.class);

                    a.add(data);


                }
                verticalModel.setArrayList(a);

                HorizontalREcyclerViewAdapter horizontalREcyclerViewAdapter=new HorizontalREcyclerViewAdapter(context,verticalModel.getArrayList());
                holder.recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false));

                holder.recyclerView.setAdapter(horizontalREcyclerViewAdapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });




        verticalModel.setArrayList(a);

        final HorizontalREcyclerViewAdapter horizontalREcyclerViewAdapter=new HorizontalREcyclerViewAdapter(context,verticalModel.getArrayList());

       holder.recyclerView.setHasFixedSize(true);
        holder.recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false));
        holder.recyclerView.setAdapter(horizontalREcyclerViewAdapter);




        holder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                verticalModel.setMoreAduter(true);
                verticalModel.setSelecting(true);
                ((verticalHorizontalList) context).featureMoreToRun();
               // alert(verticalModel);


            }
        });


        holder.select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             for(int i=0;i<a.size();i++)
             {
                if(a.get(i).getSelectHorizontal())
                {

                   String x=a.get(i).getName();
                   a.get(i).setSelectHorizontal(false);
                   verticalModel.setKeySel(true);
                   verticalModel.setKey(x);
                   ((verticalHorizontalList) context).functionToRun();
                    break;
                }
             }

            }
        });


        if(verticalModel.getFselect())
        {
            holder.remove.setVisibility(View.VISIBLE);

            holder.remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    for(int i=0;i<a.size();i++)
                    {
                        if(a.get(i).getSelectHorizontal())
                        {
                            String x=a.get(i).getName();
                            a.get(i).setSelectHorizontal(false);
                            verticalModel.setKeySel(true);
                            verticalModel.setKey(x);
                            ((verticalHorizontalList) context).functionToRemove();
                            break;

                        }
                    }
                }
            });
        }
    }




    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class VerticalViewHolder extends RecyclerView.ViewHolder {
        RecyclerView recyclerView;
        TextView textViewTitle;

        Button more,select,remove;



        public VerticalViewHolder(View itemView)
        {
            super(itemView);
            recyclerView=itemView.findViewById(R.id.recyclerview2);
            textViewTitle=itemView.findViewById(R.id.textTitle);
            more=itemView.findViewById(R.id.btnMore);
            select=itemView.findViewById(R.id.select);
            remove=itemView.findViewById(R.id.remove);
        }




    }





}
