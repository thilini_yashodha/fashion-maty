package com.example.fashiondesigner.customerSearch;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.example.fashiondesigner.Adapter.FabricVerticalAdupter;
import com.example.fashiondesigner.Adapter.Model.FabricHrizontalAdupter;
import com.example.fashiondesigner.Adapter.Model.HorizontalModel;
import com.example.fashiondesigner.Adapter.Model.VerticalModel;
import com.example.fashiondesigner.Adapter.Model.searchViewModel;
import com.example.fashiondesigner.Adapter.searchFulldressAdupter;
import com.example.fashiondesigner.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class colourFull extends AppCompatActivity {

    String click,clickItemName,user_ID,subClick,itempushID,fabricType;
    DatabaseReference mdata;

    ArrayList<searchViewModel> arrayListHorizontal;
    RecyclerView fabricReView;
    searchFulldressAdupter adapter;

    ArrayList<String> fabricArr;
    int k=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_colour_full);


        Intent intent = getIntent();
        click = intent.getStringExtra("clickCategory").toLowerCase();
        subClick = intent.getStringExtra("subClickCategory").toLowerCase();
        clickItemName=intent.getStringExtra("clickItemName").toLowerCase();
        itempushID=intent.getStringExtra("itempushID");
       // fabricType=intent.getStringExtra("fabricType");

        TextView title=findViewById(R.id.clickItem);
        title.setText(clickItemName);

       // TextView subTitle=findViewById(R.id.yourList);
       // subTitle.setText(fabricType.toUpperCase());

        FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
        user_ID=user.getUid();
        mdata= FirebaseDatabase.getInstance().getReference().child(user_ID);

        arrayListHorizontal = new ArrayList<>();

        fabricReView=findViewById(R.id.itemList);
        fabricReView.setHasFixedSize(true);

        fabricReView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        adapter=new searchFulldressAdupter(this,arrayListHorizontal);
        fabricReView.setAdapter(adapter);



        mdata.child("fabric").child("fullFabricColours").child(itempushID).child("fabric").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()) {

                    final String data2= dataSnapshot2.getValue().toString();
                    Log.d("fabricKey",data2);
                   // tempImage.add(data2);

                    mdata.child("fabric").child("fullFabricColours").child(itempushID).child("colour").child(data2).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                                String data1=dataSnapshot1.getValue().toString();
                                Log.d("colourKey",data1);

                                mdata.child("fabric").child("fabricFulldress").child(itempushID).child(data2).child(data1).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        for (DataSnapshot dataSnapshot3 : dataSnapshot.getChildren()) {

                                            searchViewModel data= dataSnapshot3.getValue(searchViewModel.class);

                                            arrayListHorizontal.add(data);
                                            arrayListHorizontal.get(k).settColourFull(true);
                                            adapter.notifyDataSetChanged();
                                            k++;

                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });

                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });







    }
}
