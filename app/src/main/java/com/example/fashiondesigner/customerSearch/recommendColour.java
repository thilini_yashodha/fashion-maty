package com.example.fashiondesigner.customerSearch;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fashiondesigner.Adapter.Model.VerticalModel;
import com.example.fashiondesigner.Adapter.Model.searchHorizontalModel;
import com.example.fashiondesigner.Adapter.searchColouAdupter;
import com.example.fashiondesigner.Adapter.searchFabricAdupter;
import com.example.fashiondesigner.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class recommendColour extends AppCompatActivity {

    String category,subCategory,itemName,userID,fulldressPushID;

    DatabaseReference mdata;

    ArrayList<searchHorizontalModel> arrayListHorizontal;
    RecyclerView fabricReView;
    searchFabricAdupter adapter;
    int p=0;


    RecyclerView verticalRecyclerView;
    searchColouAdupter adapterColour;
    ArrayList<VerticalModel> arrayListVertical;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recommend_colour);


        Intent intent = getIntent();
        category = intent.getStringExtra("category").toLowerCase();
        subCategory = intent.getStringExtra("subCategory").toLowerCase();
        itemName = intent.getStringExtra("itemName").toLowerCase();
        userID = intent.getStringExtra("userID");
        fulldressPushID=intent.getStringExtra("pushID");

        TextView clickItem=findViewById(R.id.clickItem);
        clickItem.setText(itemName.toUpperCase());

       // Button addDress=findViewById(R.id.order);
        final Button recommend=findViewById(R.id.recommend);

        mdata= FirebaseDatabase.getInstance().getReference().child(userID).child("fabric");

        arrayListHorizontal = new ArrayList<>();

        fabricReView=findViewById(R.id.fabricRe);
        fabricReView.setHasFixedSize(true);

        fabricReView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));

        adapter=new searchFabricAdupter(this,arrayListHorizontal);
        fabricReView.setAdapter(adapter);


        arrayListVertical=new ArrayList<>();

        verticalRecyclerView=findViewById(R.id.colourRe);
        verticalRecyclerView.setHasFixedSize(true);

        verticalRecyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));


        adapterColour=new searchColouAdupter(this,arrayListVertical);
        verticalRecyclerView.setAdapter(adapterColour);


         recommend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(coulourKeyArr.size()>0)
                {
                    Intent intent = new Intent(recommendColour.this, showColourFulldress.class);
                    intent.putExtra("userID",userID);
                    intent.putExtra("category",category);
                    intent.putExtra("subCategory",subCategory);
                    intent.putExtra("itemName",itemName);
                    intent.putExtra("pushID",fulldressPushID);
                    intent.putExtra("fabricKeyArr",fabricPushkeyArr);
                    intent.putExtra("colourKeyArr",coulourKeyArr);
                    startActivity(intent);
                    fabricPushkeyArr.clear();
                    coulourKeyArr.clear();
                }
                else
                {
                    Toast.makeText(recommendColour.this,"Select fabric first",Toast.LENGTH_SHORT).show();

                }




            }
        });




        mdata.child("fullFabricColours").child(fulldressPushID).child("fabric").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot ds:dataSnapshot.getChildren())
                {
                    String data=ds.getValue().toString();

                    mdata.child("fabricDetails").child(data).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot2) {

                            searchHorizontalModel data1=dataSnapshot2.getValue(searchHorizontalModel.class);
                            arrayListHorizontal.add(data1);
                            arrayListHorizontal.get(p).setSelect(true);
                            p++;
                            adapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });


                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }


    String select,fabricType;
    public void functionToRun(String fabricPushID)
    {
        for(int i=0;i<arrayListHorizontal.size();i++)
        {
            if(arrayListHorizontal.get(i).getSelectHorizontal())
            {

                select=arrayListHorizontal.get(i).getPushID();
                fabricType=arrayListHorizontal.get(i).getName();

                VerticalModel mVerticalModel=new VerticalModel();

                mVerticalModel.setID(userID);
                mVerticalModel.setClick(category);
                mVerticalModel.setsubClick(subCategory);
                mVerticalModel.setClickItemName(itemName);
                mVerticalModel.setPushID(fulldressPushID);
                mVerticalModel.setFabricPushID(fabricPushID);
                mVerticalModel.setTitle(fabricType.toUpperCase());
                mVerticalModel.setrColour(true);


                arrayListVertical.clear();

                arrayListVertical.add(mVerticalModel);

                adapterColour.notifyDataSetChanged();

                arrayListHorizontal.get(i).setSelectHorizontal(false);
                break;
            }
        }

    }

    String colourKey,fabricPushkey;
    ArrayList<String> coulourKeyArr=new ArrayList<>();
    ArrayList<String> fabricPushkeyArr=new ArrayList<>();

    public void SelectToRun() {

        for(int i=0;i<arrayListVertical.size();i++)
        {
            if(arrayListVertical.get(i).getKeySel())
            {
                colourKey=colourKey+arrayListVertical.get(i).getTitle()+";"+arrayListVertical.get(i).getKey()+";";
                colourKey=arrayListVertical.get(i).getKey();
                coulourKeyArr.add(colourKey);
                fabricPushkey=arrayListVertical.get(i).getFabricPushID();
                if(!(fabricPushkeyArr.contains(fabricPushkey)))
                {
                    fabricPushkeyArr.add(fabricPushkey);
                }


                arrayListVertical.get(i).setKeySel(false);
                break;

            }





        }
    }
}
