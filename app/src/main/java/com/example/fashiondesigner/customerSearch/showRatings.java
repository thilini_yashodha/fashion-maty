package com.example.fashiondesigner.customerSearch;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.fashiondesigner.Adapter.Model.HorizontalModel;
import com.example.fashiondesigner.Adapter.Model.searchHorizontalModel;
import com.example.fashiondesigner.Adapter.Model.searchModel;
import com.example.fashiondesigner.Adapter.ratingAdupter;
import com.example.fashiondesigner.Adapter.searchAdupter;
import com.example.fashiondesigner.R;
import com.example.fashiondesigner.ViewItem;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class showRatings extends AppCompatActivity {

    String userID;
    ArrayList<HorizontalModel> ratingArr;
    RecyclerView listView;
    ratingAdupter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_ratings);

        Intent intent = getIntent();
        userID = intent.getStringExtra("userID");

        final ImageView tailorImage=findViewById(R.id.profileImage);
        final TextView bName=findViewById(R.id.shopName);
        final TextView tName=findViewById(R.id.tName);
        listView=findViewById(R.id.listView);


        DatabaseReference mdata= FirebaseDatabase.getInstance().getReference().child(userID).child("reviews").child("customerReview");
        final DatabaseReference mdataCust=FirebaseDatabase.getInstance().getReference();
        ratingArr=new ArrayList<>();

        listView.setHasFixedSize(true);
        listView.setLayoutManager(new LinearLayoutManager(showRatings.this,LinearLayoutManager.VERTICAL,false));

        adapter=new ratingAdupter(showRatings.this,ratingArr);
        listView.setAdapter(adapter);

        DatabaseReference mdataT=FirebaseDatabase.getInstance().getReference();

        mdataT.child(userID).child("tailorData").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshotT) {

                String tailorProfile=dataSnapshotT.child("imageuri").getValue().toString();
                String tailorName=dataSnapshotT.child("tailorName").getValue().toString();
                String businessName=dataSnapshotT.child("businessName").getValue().toString();

                Glide.with(showRatings.this).load(tailorProfile).into(tailorImage);
                bName.setText(businessName.toUpperCase());
                tName.setText("BY "+tailorName.toUpperCase());

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mdata.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists())
                {
                    for(final DataSnapshot ds:dataSnapshot.getChildren())
                    {

                        final HorizontalModel smodel= new HorizontalModel();

                        String cID=ds.child("cutomerID").getValue().toString();
                        mdataCust.child(cID).child("customerData").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshotCD) {
                                String profileImage=dataSnapshotCD.child("imageUrl").getValue().toString();
                                String name=dataSnapshotCD.child("name").getValue().toString();

                                smodel.setImageUrl(profileImage);
                                smodel.setName(name);

                                String cImage=null;
                                if(ds.child("commentImageUrl").exists())
                                {
                                    cImage=ds.child("commentImageUrl").getValue().toString();
                                    smodel.setImageBack(cImage);
                                }

                                String comment=ds.child("text").getValue().toString();
                                smodel.setPattern(comment);

                                ratingArr.add(smodel);
                                adapter.notifyDataSetChanged();


                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });


                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }



}