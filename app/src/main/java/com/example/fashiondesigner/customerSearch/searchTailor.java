package com.example.fashiondesigner.customerSearch;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.example.fashiondesigner.R;
import com.example.fashiondesigner.category;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class searchTailor extends AppCompatActivity {

    String userID;
    ListView listView;
    List<String> name;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_tailor);

        Intent intent = getIntent();
        userID = intent.getStringExtra("userID");

        final TextView profile=findViewById(R.id.profile);

        listView=findViewById(R.id.gotoShow);
        name = new ArrayList<>();
        adapter=new ArrayAdapter<String>(searchTailor.this, R.layout.raw_item,name);

        Button addDesign=findViewById(R.id.addDesign);
        addDesign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(searchTailor.this, suggestDesign.class);
                intent.putExtra("userID",userID);
                startActivity(intent);
            }
        });

        Button review=findViewById(R.id.review);
        review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(searchTailor.this, showRatings.class);
                intent.putExtra("userID",userID);
                startActivity(intent);
            }
        });


        DatabaseReference mdata= FirebaseDatabase.getInstance().getReference();
        mdata.child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String name=dataSnapshot.child("tailorData").child("businessName").getValue().toString();

                profile.setText(name);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mdata.child(userID).child("category").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshotA) {

                for(DataSnapshot ds:dataSnapshotA.getChildren())
                {
                    String nm=ds.getValue().toString();

                    name.add(nm.toUpperCase()); // add all data into list.
                    adapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String click =(String) parent.getItemAtPosition(position);
                Intent intent = new Intent(searchTailor.this,searchTailorSub.class);
                intent.putExtra("click",click);
                intent.putExtra("tailorID",userID);
                startActivity(intent);

            }
        });


    }
}
