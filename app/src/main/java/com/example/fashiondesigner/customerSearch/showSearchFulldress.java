package com.example.fashiondesigner.customerSearch;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.example.fashiondesigner.Adapter.Model.searchViewModel;
import com.example.fashiondesigner.Adapter.searchFulldressAdupter;
import com.example.fashiondesigner.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class showSearchFulldress extends AppCompatActivity {

    String category,subCategory,itemName,userID;

    RecyclerView listView;
    ArrayList<searchViewModel> searchArray;
    searchFulldressAdupter adapter;

    DatabaseReference mdata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_search_fulldress);

        Intent intent = getIntent();
        category = intent.getStringExtra("category").toLowerCase();
        subCategory = intent.getStringExtra("subCategory").toLowerCase();
        itemName = intent.getStringExtra("itemName").toLowerCase();
        userID = intent.getStringExtra("userID");
        ArrayList<String> pushID=(ArrayList<String>) getIntent().getSerializableExtra("pushID");


       mdata= FirebaseDatabase.getInstance().getReference().child(userID).child("item").child(category).child(subCategory).child(itemName).child("fullDress");

        listView=findViewById(R.id.result_list);
        TextView title=findViewById(R.id.clickItem);

        title.setText(itemName.toUpperCase());

        searchArray=new ArrayList<>();

        listView.setHasFixedSize(true);
        listView.setLayoutManager(new LinearLayoutManager(showSearchFulldress.this,LinearLayoutManager.VERTICAL,false));

         adapter=new searchFulldressAdupter(showSearchFulldress.this,searchArray);
        listView.setAdapter(adapter);


        for(int i=0;i<pushID.size();i++)
        {

            mdata.child(pushID.get(i)).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                   String name=dataSnapshot.child("name").getValue().toString();
                   String imageUrl=dataSnapshot.child("imageUrl").getValue().toString();
                   String price=dataSnapshot.child("price").getValue().toString();
                   String pushID=dataSnapshot.child("pushID").getValue().toString();

                   searchViewModel smodel=new searchViewModel();
                   smodel.setName(name);
                   smodel.setImageUrl(imageUrl);
                   smodel.setPrice(price);
                   smodel.setPushID(pushID);
                   smodel.setID(userID);
                   smodel.setCategory(category);
                   smodel.setSubCategory(subCategory);
                   smodel.setItemName(itemName);
                 //  smodel.setSearch(true);

                   searchArray.add(smodel);

                   adapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }



    }
}
