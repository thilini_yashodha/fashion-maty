package com.example.fashiondesigner.customerSearch;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.fashiondesigner.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class searchTailorSub extends AppCompatActivity {

    ListView listView;
    List<String> name;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_tailor_sub);

        Intent intent = getIntent();
        final String  tailorID = intent.getStringExtra("tailorID");
        final String category=intent.getStringExtra("click").toLowerCase();

        listView=findViewById(R.id.list);

        name = new ArrayList<>();
        adapter=new ArrayAdapter<String>(searchTailorSub.this, R.layout.raw_item,name);

        DatabaseReference mdata= FirebaseDatabase.getInstance().getReference().child(tailorID).child("subCategory").child("sub "+category);

        mdata.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot ds:dataSnapshot.getChildren())
                {
                    String nm=ds.getValue().toString();

                    name.add(nm.toUpperCase()); // add all data into list.
                    adapter.notifyDataSetChanged();
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String click1 =(String) parent.getItemAtPosition(position);
                Intent intent = new Intent(searchTailorSub.this,searchTailorList.class);
                intent.putExtra("subCategory",click1);
                intent.putExtra("tailorID",tailorID);
                intent.putExtra("category",category);
                startActivity(intent);

            }
        });

    }
}