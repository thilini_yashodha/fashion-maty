package com.example.fashiondesigner.customerSearch;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.fashiondesigner.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

public class fullImage extends AppCompatActivity {

    OutputStream outputStream;
    String clickImage;
    Bitmap bitmap;
    ImageView setImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_image);

        Intent intent = getIntent();
        clickImage = intent.getStringExtra("imageUrl");

        ImageButton download=findViewById(R.id.dounloadBtn);
        setImage=findViewById(R.id.setImage);

        Glide.with(setImage.getContext())
                .load(clickImage)
                .into(setImage);


                download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BitmapDrawable drawable= (BitmapDrawable) setImage.getDrawable();
                bitmap=drawable.getBitmap();



                SaveImage(bitmap);


              /*  File filepath= Environment.getExternalStorageDirectory();
                File dir=new File(filepath.getAbsolutePath()+"/FashionMaty/");
                dir.mkdir();
                File file=new File(dir,System.currentTimeMillis()+".jpg");
                MediaScannerConnection.scanFile(fullImage.this, new String[]{file.toString()}, null, null);
                try{
                    outputStream=new FileOutputStream(file);

                }catch (FileNotFoundException e)
                {
                    e.printStackTrace();
                }


               bitmap.compress(Bitmap.CompressFormat.JPEG,100,outputStream);
                Toast.makeText(getApplicationContext(),"Image save to Internal!",Toast.LENGTH_SHORT).show();

                try {
                    outputStream.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }


               */



            }
        });

    }

    /*
    public static Bitmap getBitmapFromURL(final String clickImage) {
        try {
            URL url = new URL(clickImage);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            String dhj="jfd";
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            Log.d("bitmap","bitmap is null");
            return null;
        }



    }

     */

    private void SaveImage(Bitmap finalBitmap) {
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/saved_images");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-"+ n +".jpg";
        File file = new File (myDir, fname);
        if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            Toast.makeText(getApplicationContext(),"Image save to Internal!",Toast.LENGTH_SHORT).show();
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }




}
