package com.example.fashiondesigner.customerSearch;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fashiondesigner.Adapter.Model.VerticalModel;
import com.example.fashiondesigner.Adapter.Model.searchHorizontalModel;
import com.example.fashiondesigner.Adapter.searchColouAdupter;
import com.example.fashiondesigner.Adapter.searchFabricAdupter;
import com.example.fashiondesigner.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class searchColour extends AppCompatActivity {

    String category,subCategory,itemName,userID,fulldressPushID,fulldressImageUrl;

    DatabaseReference mdata;

    ArrayList<searchHorizontalModel> arrayListHorizontal;
    RecyclerView fabricReView;
    searchFabricAdupter adapter;


    RecyclerView verticalRecyclerView;
    searchColouAdupter adapterColour;
    ArrayList<VerticalModel> arrayListVertical;

    int k=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_colour);


        Intent intent = getIntent();
        category = intent.getStringExtra("category").toLowerCase();
        subCategory = intent.getStringExtra("subCategory").toLowerCase();
        itemName = intent.getStringExtra("itemName").toLowerCase();
        userID = intent.getStringExtra("userID");
        fulldressPushID=intent.getStringExtra("pushID");



        TextView clickItem=findViewById(R.id.clickItem);
        clickItem.setText(itemName.toUpperCase());

        Button addDress=findViewById(R.id.order);
        final Button recommend=findViewById(R.id.recommend);

        mdata= FirebaseDatabase.getInstance().getReference().child(userID).child("fabric");



        final ArrayList<String> recommendFabric=new ArrayList<>();
        recommend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mdata.child("fullFabricColours").child(fulldressPushID).child("fabric").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        for(DataSnapshot ds:dataSnapshot.getChildren())
                        {
                            String data=ds.getValue().toString();
                            recommendFabric.add(data);

                        }

                        if(recommendFabric.size()>0)
                        {
                            Intent intent = new Intent(searchColour.this, recommendColour.class);
                            intent.putExtra("userID",userID);
                            intent.putExtra("category",category);
                            intent.putExtra("subCategory",subCategory);
                            intent.putExtra("itemName",itemName);
                            intent.putExtra("pushID",fulldressPushID);
                        //    intent.putExtra("recommendFabric",recommendFabric);
                            startActivity(intent);

                        }
                        else
                        {
                            Toast.makeText(searchColour.this,"No reccomended Fabric for this item",Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });

        arrayListHorizontal = new ArrayList<>();

        fabricReView=findViewById(R.id.fabricRe);
        fabricReView.setHasFixedSize(true);

        fabricReView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));

        adapter=new searchFabricAdupter(this,arrayListHorizontal);
        fabricReView.setAdapter(adapter);



        arrayListVertical=new ArrayList<>();

        verticalRecyclerView=findViewById(R.id.colourRe);
        verticalRecyclerView.setHasFixedSize(true);

        verticalRecyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));


        adapterColour=new searchColouAdupter(this,arrayListVertical);
        verticalRecyclerView.setAdapter(adapterColour);

        final ArrayList<String> availableFabric=new ArrayList<>();

        mdata.child("fabricDetails").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                arrayListHorizontal.clear();
                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren())
                {
                    searchHorizontalModel data=dataSnapshot1.getValue(searchHorizontalModel.class);

                    arrayListHorizontal.add(data);
                    adapter.notifyDataSetChanged();

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });





    }


    String select,fabricType;
    public void functionToRun(String fabricPushID) {

        for(int i=0;i<arrayListHorizontal.size();i++)
        {
            if(arrayListHorizontal.get(i).getSelectHorizontal())
            {

                select=arrayListHorizontal.get(i).getPushID();
                fabricType=arrayListHorizontal.get(i).getName();

                VerticalModel mVerticalModel=new VerticalModel();

                mVerticalModel.setID(userID);
                mVerticalModel.setClick(category);
                mVerticalModel.setsubClick(subCategory);
                mVerticalModel.setClickItemName(itemName);
                mVerticalModel.setPushID(fulldressPushID);
                mVerticalModel.setFabricPushID(fabricPushID);
                mVerticalModel.setTitle(fabricType.toUpperCase());

               // mVerticalModel.setTitle(arrayListHorizontal.get(i).getName().toUpperCase());

                arrayListVertical.clear();

                arrayListVertical.add(mVerticalModel);

                adapterColour.notifyDataSetChanged();

                arrayListHorizontal.get(i).setSelectHorizontal(false);
                break;
            }
        }



    }


  //  ArrayList<String> coulourKeyArr=new ArrayList<>();
  //  ArrayList<String> fabricPushkeyArr=new ArrayList<>();
    public void SelectToRun(String aimageUrl) {

        String colourKey,fabricPushkey,image;

        for(int i=0;i<arrayListVertical.size();i++)
        {
            if(arrayListVertical.get(i).getKeySel())
            {
                // colourKey=colourKey+arrayListVertical.get(i).getTitle()+";"+arrayListVertical.get(i).getKey()+";";
                colourKey=arrayListVertical.get(i).getKey();
              //  coulourKeyArr.add(colourKey);
                fabricPushkey=arrayListVertical.get(i).getFabricPushID();

               // image=arrayListVertical.get(i).get
             /*   if(!(fabricPushkeyArr.contains(fabricPushkey)))
                {
                    fabricPushkeyArr.add(fabricPushkey);
                }

              */

                Intent intent = new Intent(searchColour.this, viewSeaechColour.class);
                intent.putExtra("userID",userID);
                intent.putExtra("category",category);
                intent.putExtra("subCategory",subCategory);
                intent.putExtra("itemName",itemName);
                intent.putExtra("fulldressPushID",fulldressPushID);
                intent.putExtra("fabricKey",fabricPushkey);
                intent.putExtra("colourKey",colourKey);
                intent.putExtra("imageUrl",aimageUrl);

                startActivity(intent);

                arrayListVertical.get(i).setKeySel(false);
                break;

            }





        }
    }
}
