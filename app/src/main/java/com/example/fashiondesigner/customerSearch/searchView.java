package com.example.fashiondesigner.customerSearch;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fashiondesigner.Adapter.Model.searchVerticalModel;
import com.example.fashiondesigner.Adapter.Model.searchViewModel;
import com.example.fashiondesigner.Adapter.searchVerticalAdupter;
import com.example.fashiondesigner.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

public class searchView extends AppCompatActivity {

    String category,subCategory,itemName,imageUrl,userID;

    RecyclerView verticalRecyclerView;
    searchVerticalAdupter adapter;
    ArrayList<searchVerticalModel> arrayListVertical;

    DatabaseReference mdata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_view);


        Intent intent = getIntent();
        category = intent.getStringExtra("category").toLowerCase();
        subCategory = intent.getStringExtra("subCategory").toLowerCase();
        itemName = intent.getStringExtra("itemName").toLowerCase();
        imageUrl = intent.getStringExtra("imageUrl");
        userID = intent.getStringExtra("userID");

        mdata= FirebaseDatabase.getInstance().getReference().child(userID).child("item").child(category).child(subCategory).child(itemName);

       // ImageView image=findViewById(R.id.seachImage);
        TextView title=findViewById(R.id.clickItem);
        Button searchDress=findViewById(R.id.searchDress);

        searchDress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search();
            }
        });


        title.setText(itemName.toUpperCase());


      /*  Glide.with(image.getContext())
                .load(imageUrl)
                .into(image);

       */

      /*  image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(searchView.this, fullImage.class);
                intent.putExtra("imageUrl",imageUrl);
                startActivity(intent);
            }
        });

       */



        arrayListVertical=new ArrayList<>();

        verticalRecyclerView=findViewById(R.id.recyclerView);
        verticalRecyclerView.setHasFixedSize(true);

        verticalRecyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        adapter=new searchVerticalAdupter(searchView.this,arrayListVertical);
        verticalRecyclerView.setAdapter(adapter);


        mdata.child("features").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot dataSnapshot1: dataSnapshot.getChildren())
                {
                   String data=dataSnapshot1.getValue().toString();
                  searchVerticalModel mverticalModel=new searchVerticalModel();

                    mverticalModel.setTitle(data.toUpperCase());
                    mverticalModel.setID(userID);
                    mverticalModel.setClick(category);
                    mverticalModel.setsubClick(subCategory);
                    mverticalModel.setClickItemName(itemName);

                    arrayListVertical.add(mverticalModel);

                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }



    ArrayList<String> checkStringKey=new ArrayList<>();
    public void goSetect()
    {
        for(int i=0;i<arrayListVertical.size();i++)
        {
            if(arrayListVertical.get(i).getSelectingHorizontal())
            {
                checkStringKey.add(arrayListVertical.get(i).getKey());
                arrayListVertical.get(i).setSelectingHorizontal(false);
                break;
            }


        }
    }

    private void search() {

        if(checkStringKey.size()!=0)
        {
            final ArrayList<searchViewModel> searchViewArray = new ArrayList<>();

            mdata.child("fullDress").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for(DataSnapshot dataSnapshot2:dataSnapshot.getChildren())
                    {
                        searchViewModel data=dataSnapshot2.getValue(searchViewModel.class);
                        searchViewArray.add(data);

                        String jhdf="jf";

                    }

                    ArrayList<ArrayList<Integer>> allRows = new ArrayList<>();
                    ArrayList<Integer> indexData=new ArrayList<>();
                    for(int i=0;i<searchViewArray.size();i++)
                    {
                        Integer count=0;
                        ArrayList<Integer> row=new ArrayList<>();
                        for(int j=0;j<checkStringKey.size();j++)
                        {
                            if(searchViewArray.get(i).getFeaturesArray().contains(checkStringKey.get(j)))
                            {
                                count++;

                            }
                        }

                        row.add(i);
                        row.add(count);
                        allRows.add(row);

                        if(count>0)
                        {
                            if(!(indexData.contains(count)))
                            {
                                indexData.add(count);
                            }
                        }


                    }

                    ArrayList<Integer> indexList=new ArrayList<>();
                    if(indexData.size()>1)
                    {
                        Collections.sort(indexData);

                        int first=indexData.get(indexData.size()-1);
                        int two=indexData.get(indexData.size()-2);


                        for(int i=0;i<allRows.size();i++)
                        {
                            ArrayList<Integer> temp=new ArrayList<>();

                            temp.add(allRows.get(i).get(0));
                            temp.add(allRows.get(i).get(1));

                            if((temp.get(1)==first) || (temp.get(1)==two))
                            {
                                indexList.add(temp.get(0));
                            }
                        }


                    }
                    else
                    {

                        for(int i=0;i<allRows.size();i++)
                        {
                            ArrayList<Integer> temp=new ArrayList<>();

                            temp.add(allRows.get(i).get(0));
                            temp.add(allRows.get(i).get(1));

                            if(temp.get(1)==indexData.get(0))
                            {
                                indexList.add(temp.get(0));
                            }
                        }


                    }


                    ArrayList<String> pushID=new ArrayList<>();


                    for(int j=0;j<indexList.size();j++)
                    {
                        pushID.add(searchViewArray.get(indexList.get(j)).getPushID());
                        String jd="fd";

                    }

                    checkStringKey.clear();

                    Intent intent = new Intent(searchView.this, showSearchFulldress.class);
                    intent.putExtra("userID",userID);
                    intent.putExtra("category",category);
                    intent.putExtra("subCategory",subCategory);
                    intent.putExtra("itemName",itemName);
                    intent.putExtra("pushID",pushID);
                    startActivity(intent);

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });


        }

        else
        {
            Toast.makeText(searchView.this,"Please sealect the features first!",Toast.LENGTH_SHORT).show();
        }


    }



}
