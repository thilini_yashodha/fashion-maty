package com.example.fashiondesigner.customerSearch;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.fashiondesigner.Adapter.Model.searchModel;
import com.example.fashiondesigner.Adapter.searchAdupter;
import com.example.fashiondesigner.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.core.Tag;

import java.util.ArrayList;

public class searchTailorList extends AppCompatActivity {

    RecyclerView listView;
    ArrayList<searchModel> searchArray;
    searchAdupter adapter;
    int  i=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_tailor_list);

        Intent intent = getIntent();
        final String  tailorID = intent.getStringExtra("tailorID");
        final String category=intent.getStringExtra("category").toLowerCase();
        final String subCategory=intent.getStringExtra("subCategory").toLowerCase();

        listView=findViewById(R.id.result_list);

        searchArray=new ArrayList<>();

        listView.setHasFixedSize(true);
        listView.setLayoutManager(new LinearLayoutManager(searchTailorList.this,LinearLayoutManager.VERTICAL,false));

        adapter=new searchAdupter(searchTailorList.this,searchArray);
        listView.setAdapter(adapter);

        DatabaseReference mdata= FirebaseDatabase.getInstance().getReference().child("allItems");

        mdata.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                for (DataSnapshot dataSnapshot2:dataSnapshot.getChildren())
                {

                    searchModel smodel;
                    smodel=dataSnapshot2.getValue(searchModel.class);


                   if((smodel.getUser_ID().equals(tailorID)) && (smodel.getCategory().equals(category)) && (smodel.getSubCategory().equals(subCategory))  && (smodel.getMeasureState().equals("1")))
                   {

                       searchArray.add(smodel);
                       searchArray.get(i).setCheck("subCategory");
                       adapter.notifyDataSetChanged();
                       i++;

                   }


                }


                if(searchArray.size()==0)
                {
                    Toast.makeText(searchTailorList.this,"No items to show",Toast.LENGTH_SHORT).show();
                }



            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });






    }
}