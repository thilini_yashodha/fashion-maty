package com.example.fashiondesigner.customerSearch;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.fashiondesigner.R;
import com.squareup.picasso.Picasso;

public class searchClickItem extends AppCompatActivity {

    String category,subCategory,itemName,imageUrl,userID,price,pushID;
    TextView priceText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_click_item);


        Intent intent = getIntent();
        category = intent.getStringExtra("category").toLowerCase();
        subCategory = intent.getStringExtra("subCategory").toLowerCase();
        itemName = intent.getStringExtra("itemName").toLowerCase();
        imageUrl = intent.getStringExtra("imageUrl");
        userID = intent.getStringExtra("userID");
        price=intent.getStringExtra("priceRange");
        pushID=intent.getStringExtra("pushID");
        String gjdf="jf";


        TextView title=findViewById(R.id.title);
        TextView upper=findViewById(R.id.clickItem);
        ImageView image=findViewById(R.id.image);
        priceText=findViewById(R.id.price);
        Button features=findViewById(R.id.features);
        Button order=findViewById(R.id.order);


        upper.setText(subCategory.toUpperCase());
        title.setText(itemName.toUpperCase());
        Picasso.get().load(imageUrl).into(image);
        priceText.setText("$ "+price);


        features.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(searchClickItem.this , searchView.class);
                intent.putExtra("category",category);
                intent.putExtra("subCategory",subCategory);
                intent.putExtra("itemName",itemName);
                intent.putExtra("imageUrl",imageUrl);
                intent.putExtra("userID",userID);
                startActivity(intent);
            }
        });

        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(searchClickItem.this , searchColour.class);
                intent.putExtra("category",category);
                intent.putExtra("subCategory",subCategory);
                intent.putExtra("itemName",itemName);
                intent.putExtra("pushID",pushID);
                intent.putExtra("userID",userID);
               // intent.putExtra("fullDressImage",imageUrl);
                startActivity(intent);
            }
        });




        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(searchClickItem.this, fullImage.class);
                intent.putExtra("imageUrl",imageUrl);
                startActivity(intent);
            }
        });

    }
}
