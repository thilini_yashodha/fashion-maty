package com.example.fashiondesigner.customerSearch;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.example.fashiondesigner.Adapter.Model.HorizontalModel;
import com.example.fashiondesigner.Adapter.Model.searchViewModel;
import com.example.fashiondesigner.Adapter.searchFulldressAdupter;
import com.example.fashiondesigner.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class showColourFulldress extends AppCompatActivity {

    String category,subCategory,itemName,userID,fulldresssPushID;

    DatabaseReference mdata;

    RecyclerView listView;
    ArrayList<searchViewModel> searchArray;
    searchFulldressAdupter adapter;

    int k=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_colour_fulldress);


        Intent intent = getIntent();
        category = intent.getStringExtra("category").toLowerCase();
        subCategory = intent.getStringExtra("subCategory").toLowerCase();
        itemName = intent.getStringExtra("itemName").toLowerCase();
        userID = intent.getStringExtra("userID");
        fulldresssPushID=intent.getStringExtra("pushID");
        final ArrayList<String> fabricKeyArr=(ArrayList<String>) getIntent().getSerializableExtra("fabricKeyArr");
        final ArrayList<String> colourKeyArr=(ArrayList<String>) getIntent().getSerializableExtra("colourKeyArr");

        mdata= FirebaseDatabase.getInstance().getReference().child(userID).child("fabric").child("fabricFulldress").child(fulldresssPushID);

        listView=findViewById(R.id.result_list);

        searchArray=new ArrayList<>();

        listView.setHasFixedSize(true);
        listView.setLayoutManager(new LinearLayoutManager(showColourFulldress.this,LinearLayoutManager.VERTICAL,false));

        adapter=new searchFulldressAdupter(showColourFulldress.this,searchArray);
        listView.setAdapter(adapter);

        final ArrayList<HorizontalModel> dataArr=new ArrayList<>();

        for(int i=0;i<fabricKeyArr.size();i++)
        {
            for(int j=0;j<colourKeyArr.size();j++)
            {

                final int finalJ = j;
                final int finalI = i;
                mdata.child(fabricKeyArr.get(i)).child(colourKeyArr.get(j)).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                            HorizontalModel data=dataSnapshot1.getValue(HorizontalModel.class);

                            dataArr.add(data);

                            String name=dataArr.get(k).getName();
                            String imageUrl=dataArr.get(k).getImageUrl();
                            String price=dataArr.get(k).getPrice();

                            if(imageUrl!=null)
                            {
                                searchViewModel smodel=new searchViewModel();
                                smodel.setName(name);
                                smodel.setImageUrl(imageUrl);
                                smodel.setPrice("$ "+price);
                                smodel.setColour(true);
                                smodel.setColourKey(colourKeyArr.get(finalJ));
                               smodel.setFabricKey(fabricKeyArr.get(finalI));


                                searchArray.add(smodel);

                                adapter.notifyDataSetChanged();
                            }
                            k++;


                        }


                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        }
    }

    public void goToOrder(String aimageUrl, String fabricKey, String colourKey) {

        Intent intent = new Intent(showColourFulldress.this, viewSeaechColour.class);
        intent.putExtra("imageUrl",aimageUrl);
        intent.putExtra("userID",userID);
        intent.putExtra("category",category);
        intent.putExtra("subCategory",subCategory);
        intent.putExtra("itemName",itemName);
        intent.putExtra("fulldressPushID",fulldresssPushID);
        intent.putExtra("fabricKey",fabricKey);
        intent.putExtra("colourKey",colourKey);

        startActivity(intent);
    }
}
