package com.example.fashiondesigner.customerSearch;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.fashiondesigner.R;
import com.example.fashiondesigner.measurements.cMeasurement;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class viewSeaechColour extends AppCompatActivity {

    String category,subCategory,itemName,imageUrl,userID,fabricKey,colourKey,fullDRessPushID;

    DatabaseReference mdata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_seaech_colour);

        Intent intent = getIntent();
        category = intent.getStringExtra("category").toLowerCase();
        subCategory = intent.getStringExtra("subCategory").toLowerCase();
        itemName = intent.getStringExtra("itemName").toLowerCase();
        imageUrl = intent.getStringExtra("imageUrl");
        userID = intent.getStringExtra("userID");
        fabricKey = intent.getStringExtra("fabricKey");
        colourKey = intent.getStringExtra("colourKey");
        fullDRessPushID=intent.getStringExtra("fulldressPushID");

        mdata= FirebaseDatabase.getInstance().getReference().child(userID).child("fabric");

        TextView title=findViewById(R.id.clickItem);
        title.setText(subCategory.toUpperCase());

        TextView subtitle=findViewById(R.id.title);
        subtitle.setText(itemName.toUpperCase());

        ImageView image=findViewById(R.id.image);
        Glide.with(image.getContext())
                .load(imageUrl)
                .into(image);

        final TextView fabric=findViewById(R.id.fabric);
        final TextView pattern=findViewById(R.id.pattern);
        final TextView price=findViewById(R.id.price);
        final TextView colour=findViewById(R.id.colour);

        Button measurement=findViewById(R.id.measure);



        mdata.child("fabricDetails").child(fabricKey).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String fabricType=dataSnapshot.child("name").getValue().toString();
                String fabricPattern=dataSnapshot.child("pattern").getValue().toString();
                String pricef=dataSnapshot.child("price").getValue().toString();

                fabric.setText(fabricType.toUpperCase());
                pattern.setText(fabricPattern.toUpperCase());
                price.setText("Coset for 1 m : $ " +pricef);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

       mdata.child("colours").child(fabricKey).child(colourKey).addValueEventListener(new ValueEventListener() {
           @Override
           public void onDataChange(@NonNull DataSnapshot dataSnapshot2) {

               String fabricColour=dataSnapshot2.child("name").getValue().toString();

               colour.setText(fabricColour.toUpperCase());
           }

           @Override
           public void onCancelled(@NonNull DatabaseError databaseError) {

           }
       });


       measurement.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

               DatabaseReference mdataget=FirebaseDatabase.getInstance().getReference().child(userID).child("measurements").child(fullDRessPushID);
               mdataget.child("state").addListenerForSingleValueEvent(new ValueEventListener() {
                   @Override
                   public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                       String state=dataSnapshot.getValue().toString();

                       if(state.equals("1"))
                       {
                           Intent intent = new Intent(viewSeaechColour.this, cMeasurement.class);
                           intent.putExtra("userID",userID);
                           intent.putExtra("category",category);
                           intent.putExtra("subCategory",subCategory);
                           intent.putExtra("itemName",itemName);
                           intent.putExtra("fulldressPushID",fullDRessPushID);
                           intent.putExtra("fabricKey",fabricKey);
                           intent.putExtra("colourKey",colourKey);
                           intent.putExtra("imageUrl",imageUrl);

                           startActivity(intent);
                       }

                       else if(state.equals("0"))
                       {
                          /* Intent intent = new Intent(viewSeaechColour.this, cMeasurement.class);
                           intent.putExtra("userID",userID);
                           intent.putExtra("category",category);
                           intent.putExtra("subCategory",subCategory);
                           intent.putExtra("itemName",itemName);
                           intent.putExtra("fulldressPushID",fullDRessPushID);
                           intent.putExtra("fabricKey",fabricKey);
                           intent.putExtra("colourKey",colourKey);
                           intent.putExtra("imageUrl",imageUrl);

                           startActivity(intent);

                           */

                       }
                   }

                   @Override
                   public void onCancelled(@NonNull DatabaseError databaseError) {

                   }
               });


           }
       });


    }
}
