package com.example.fashiondesigner.customerSearch;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.fashiondesigner.R;
import com.example.fashiondesigner.chat.meassageActivity;
import com.example.fashiondesigner.measurements.placeOrder;
import com.example.fashiondesigner.order.payments;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

public class suggestDesign extends AppCompatActivity {

    String userID;

    ImageView getImageFront;
    ImageView getImageBack;
    private int GALLERY = 1, CAMERA = 2;
    public Uri filePathFront,filePathBack;
    StorageReference mstorage;
    Boolean front=false,back=false;

    EditText textData;
    DatabaseReference mdata,mtailorData;
    String currentUserID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggest_design);


        Intent intent = getIntent();
        userID = intent.getStringExtra("userID");

        getImageFront=findViewById(R.id.front);
        getImageBack=findViewById(R.id.back);

        ImageButton btnFront=findViewById(R.id.btnFront);
        ImageButton btnBack=findViewById(R.id.btnBack);

        textData=findViewById(R.id.textData);
        Button conform=findViewById(R.id.conform);
        Button chatTailor=findViewById(R.id.chatTailor);

        mstorage= FirebaseStorage.getInstance().getReference();

        FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
        currentUserID=user.getUid();

        mdata= FirebaseDatabase.getInstance().getReference().child(currentUserID);
        mtailorData=FirebaseDatabase.getInstance().getReference().child(userID);

        chatTailor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(suggestDesign.this, meassageActivity.class);
                intent.putExtra("userID",userID);
                startActivity(intent);
            }
        });

        btnFront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                front=true;
                showPictureDialog();

            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                back=true;
                showPictureDialog();

            }
        });


        conform.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                upload();

            }
        });



    }



    private void showPictureDialog() {

        androidx.appcompat.app.AlertDialog.Builder pictureDialog = new androidx.appcompat.app.AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                dispathTakePictureIntent();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);


    }


    private  void dispathTakePictureIntent()
    {
        Intent takePictureIntent= new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(takePictureIntent.resolveActivity(getPackageManager())!= null) {
            File photoFile = null;
            try {
                {
                    photoFile = createImageFile();
                }
            } catch (IOException ex) {

            }

            if (photoFile != null) {
                Uri photoUri = FileProvider.getUriForFile(this, "com.example.android.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(takePictureIntent, CAMERA);
            }
        }
    }

    String currentPhotoPath;

    private  File createImageFile() throws IOException
    {
        String timeStamp=new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName="JPEG_"+timeStamp+"_";
        File storageDir= Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image=File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        currentPhotoPath=image.getAbsolutePath();
        return  image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_CANCELED) {
            return;
        }

        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();

                if(front)
                {
                    filePathFront = contentURI;
                }
                else if(back)
                {
                    filePathBack=contentURI;
                }


                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    Toast.makeText(suggestDesign.this, "Image Saved!", Toast.LENGTH_SHORT).show();

                    if(front)
                    {
                        getImageFront.setImageBitmap(bitmap);
                        front=false;
                    }
                    else if(back)
                    {
                        getImageBack.setImageBitmap(bitmap);
                        back=false;
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(suggestDesign.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {

            File f=new File(currentPhotoPath);

            if(front)
            {
                getImageFront.setImageURI(Uri.fromFile(f));
                filePathFront = Uri.fromFile(f);
                front=false;

            }
            else if(back)
            {
                getImageBack.setImageURI(Uri.fromFile(f));
                filePathBack=Uri.fromFile(f);
                back=false;
            }




            Intent mediaScanIntent=new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri=Uri.fromFile(f);
            mediaScanIntent.setData(contentUri);
            this.sendBroadcast(mediaScanIntent);

        }
    }


    private void upload() {

        if (filePathFront != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Wait...");
            progressDialog.show();

            final StorageReference ref = mstorage.child("Suggest/" + UUID.randomUUID().toString());
            ref.putFile(filePathFront)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    final String savedUri = uri.toString();



                                    if(filePathBack!=null)
                                    {
                                        final StorageReference refBack = mstorage.child("Suggest/" + UUID.randomUUID().toString());
                                        refBack.putFile(filePathBack)
                                                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                                    @Override
                                                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                                                        refBack.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                            @Override
                                                            public void onSuccess(Uri uriBack) {

                                                                String savedUriBack=uriBack.toString();


                                                                String text=textData.getText().toString();
                                                                HashMap<String,String> addMap=new HashMap<>();

                                                                addMap.put("text",text);
                                                                addMap.put("imageUrlFront",savedUri);
                                                                addMap.put("imageUrlBack",savedUriBack);
                                                                addMap.put("tailorID",userID);
                                                                addMap.put("customerID",currentUserID);

                                                                String timeKey=new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

                                                                addMap.put("pushID",timeKey);

                                                                mdata.child("suggestOrder").child(timeKey).setValue(addMap);

                                                                mtailorData.child("suggestOrder").child(timeKey).setValue(addMap);



                                                            }
                                                        });
                                                    }
                                                });
                                    }

                                    else {

                                        String text=textData.getText().toString();
                                        HashMap<String,String> addMap=new HashMap<>();

                                        addMap.put("text",text);
                                        addMap.put("imageUrlFront",savedUri);
                                        addMap.put("tailorID",userID);
                                        addMap.put("customerID",currentUserID);

                                        String timeKey=new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

                                        addMap.put("pushID",timeKey);

                                        mdata.child("suggestOrder").child(timeKey).setValue(addMap);

                                        mtailorData.child("suggestOrder").child(timeKey).setValue(addMap);



                                    }


                                }
                            });
                            progressDialog.dismiss();

                            Toast.makeText(suggestDesign.this, "Succesful", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(suggestDesign.this, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                    .getTotalByteCount());


                            progressDialog.setMessage("Successful " );
                        }
                    });
        }
        else
        {
            Toast.makeText(suggestDesign.this, "no image!", Toast.LENGTH_SHORT).show();
            String text=textData.getText().toString();
            HashMap <String,String> addMap=new HashMap<>();

            addMap.put("text",text);
            addMap.put("tailorID",userID);
            addMap.put("customerID",currentUserID);

            String timeKey=new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

            addMap.put("pushID",timeKey);

            mdata.child("suggestOrder").child(timeKey).setValue(addMap);

            mtailorData.child("suggestOrder").child(timeKey).setValue(addMap);


        }


    }

}
