package com.example.fashiondesigner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.webkit.PermissionRequest;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

public class spdetail extends AppCompatActivity {


    ImageView imageView;
    Button button, set;
    public Uri iuri;
    EditText businessName, tailorName, tele, email, city, discription, add;
    public FirebaseAuth mFireAuth;
    FirebaseDatabase database;
    DatabaseReference mdata0;
    StorageReference mStorageRef1;
    FirebaseUser user1;
    Spinner country;

    private int GALLERY = 1, CAMERA = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spdetail);

        set = findViewById(R.id.btnset);
        businessName=findViewById(R.id.etbname);
        tailorName=findViewById(R.id.ettname);
        tele=findViewById(R.id.evteleNO);
        email=findViewById(R.id.evemail);
        country=findViewById(R.id.spiner);
        city=findViewById(R.id.evcity);
        discription=findViewById(R.id.evdiscript);
        add=findViewById(R.id.etadd);
        mStorageRef1 = FirebaseStorage.getInstance().getReference();
        user1 = FirebaseAuth.getInstance().getCurrentUser();
        database=FirebaseDatabase.getInstance();
        mdata0=FirebaseDatabase.getInstance().getReference();


        Locale[] locales = Locale.getAvailableLocales();
        ArrayList<String> countries = new ArrayList<String>();
        for (Locale locale : locales) {
            String country = locale.getDisplayCountry();
            if (country.trim().length() > 0 && !countries.contains(country)) {
                countries.add(country);
            }
        }
        Collections.sort(countries);


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, countries);
        // set the view for the Drop down list
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // set the ArrayAdapter to the spinner
        country.setAdapter(dataAdapter);
        country.setSelection(1);



            imageView = findViewById(R.id.imageView2);
            button = findViewById(R.id.btn);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    showPictureDialog();
                }
            });


            set.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    upload();

                }
            });




    }


    private void showPictureDialog() {

        androidx.appcompat.app.AlertDialog.Builder pictureDialog = new androidx.appcompat.app.AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                dispathTakePictureIntent();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);


    }


    private  void dispathTakePictureIntent()
    {
        Intent takePictureIntent= new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(takePictureIntent.resolveActivity(getPackageManager())!= null) {
            File photoFile = null;
            try {
                {
                    photoFile = createImageFile();
                }
            } catch (IOException ex) {

            }

            if (photoFile != null) {
                Uri photoUri = FileProvider.getUriForFile(this, "com.example.android.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(takePictureIntent, CAMERA);
            }
        }
    }

    String currentPhotoPath;

    private  File createImageFile() throws IOException
    {
        String timeStamp=new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName="JPEG_"+timeStamp+"_";
        File storageDir= Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image=File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        currentPhotoPath=image.getAbsolutePath();
        return  image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_CANCELED) {
            return;
        }

        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                iuri = contentURI;
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    Toast.makeText(spdetail.this, "Image Saved!", Toast.LENGTH_SHORT).show();

                   // imageView.setImageBitmap(bitmap);
                    Picasso.get().load(iuri).into(imageView);


                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(spdetail.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {

            File f=new File(currentPhotoPath);
            //imageView.setImageURI(Uri.fromFile(f));
            iuri=Uri.fromFile(f);
            Picasso.get().load(iuri).into(imageView);

            Intent mediaScanIntent=new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri=Uri.fromFile(f);
            mediaScanIntent.setData(contentUri);
            this.sendBroadcast(mediaScanIntent);

        }
    }






    public void upload() {
        if (iuri != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

           final String user_id;
           user_id = user1.getUid();

            final StorageReference ref =mStorageRef1.child("tailor_profile").child(user_id);
            ref.putFile(iuri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    final String duri = uri.toString();

                                    mdata0.addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                            HashMap<String, String> hashMap = new HashMap<String, String>();

                                            hashMap.put("imageuri",duri);
                                            hashMap.put("businessName", businessName.getText().toString());
                                            hashMap.put("tailorName", tailorName.getText().toString());
                                            hashMap.put("tele", tele.getText().toString());
                                            hashMap.put("email", email.getText().toString());
                                            hashMap.put("country", country.getSelectedItem().toString());
                                            hashMap.put("city", city.getText().toString());
                                            hashMap.put("discription", discription.getText().toString());
                                            hashMap.put("address", add.getText().toString());

                                            mdata0.child(user_id).child("tailorData").setValue(hashMap);
                                            mdata0.child(user_id).child("tag").setValue("1");


                                            Intent i = new Intent(spdetail.this,Tailor2.class);
                                            startActivity(i);
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {


                                        }
                                    });






                                }
                            });
                            progressDialog.dismiss();

                            Toast.makeText(spdetail.this, "Uploaded", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(spdetail.this, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setMessage("Uploaded " + (int) progress + "%");
                        }
                    });

        }

        else
        {
            Toast.makeText(spdetail.this, "Select an image first!", Toast.LENGTH_SHORT).show();
        }



    }



}
