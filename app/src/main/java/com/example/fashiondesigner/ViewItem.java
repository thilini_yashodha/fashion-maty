package com.example.fashiondesigner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import static android.widget.AdapterView.*;

public class ViewItem extends AppCompatActivity {

    ListView listView;

    List<String> name;
    List<String> price;
    List<String> pushID;

    List<String> imageString;
    String data,data2;
    ViewItem.MyAdapter adapter;

    DatabaseReference mdata;
    StorageReference mstorage;
    String user_ID,click,subClick,country,city,shopName,tailorName;


    private int GALLERY = 1, CAMERA = 2;
    public Uri filePath;
    ImageView imageView;

    boolean back=false;
    public Uri filepathBack;
    ImageView imageViewBack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_item);

        Intent intent = getIntent();
        click = intent.getStringExtra("click").toLowerCase();
        subClick = intent.getStringExtra("subClick").toLowerCase();



        listView=findViewById(R.id.itemList);
        Button addItem=findViewById(R.id.addItem);

        FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
        user_ID=user.getUid();
        mdata=FirebaseDatabase.getInstance().getReference();
        mstorage= FirebaseStorage.getInstance().getReference();

        name = new ArrayList<>();
        price = new ArrayList<>();
        imageString=new ArrayList<>();
        pushID=new ArrayList<>();
        adapter=new ViewItem.MyAdapter(this,name,price,imageString,pushID);

        TextView textTile=findViewById(R.id.clickItem);
        textTile.setText(subClick.toUpperCase());


        final ArrayList<String> tempImage=new ArrayList<>();

        mdata.child(user_ID).child("item").child(click).child(subClick).child("images").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()) {

                    data2= dataSnapshot2.getValue().toString();
                    tempImage.add(data2);


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        final ArrayList<String> tempName=new ArrayList<>();

        mdata.child(user_ID).child("item").child(click).child(subClick).child("names").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    data= dataSnapshot1.getValue().toString();
                    tempName.add(data.toUpperCase());


                }



            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        final ArrayList<String> tempPushID=new ArrayList<>();

        mdata.child(user_ID).child("item").child(click).child(subClick).child("pushID").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    data= dataSnapshot1.getValue().toString();
                    tempPushID.add(data);


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        mdata.child(user_ID).child("item").child(click).child(subClick).child("prices").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                int i=0;
                for (DataSnapshot dataSnapshot3 : dataSnapshot.getChildren()) {

                    data= dataSnapshot3.getValue().toString();
                    price.add(data);
                    imageString.add(tempImage.get(i));
                    name.add(tempName.get(i));
                    pushID.add(tempPushID.get(i));
                    adapter.notifyDataSetChanged();
                    i++;

                }



            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



        mdata.child(user_ID).child("tailorData").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                country=dataSnapshot.child("country").getValue().toString();
                city=dataSnapshot.child("city").getValue().toString();
                shopName=dataSnapshot.child("businessName").getValue().toString();
                tailorName=dataSnapshot.child("tailorName").getValue().toString();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });







        listView.setAdapter(adapter);


       listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String clickName =name.get(position);
                String imageUrl=imageString.get(position);
                String sposition=Integer.toHexString(position);
                String selectPrice=price.get(position);
                String selectPushID=pushID.get(position);
                Intent intent = new Intent(ViewItem.this ,viewItemClick.class);
                intent.putExtra("clickCategory",click);
                intent.putExtra("subClickCategory",subClick);
                intent.putExtra("clickItemName",clickName);
                intent.putExtra("imageUrl",imageUrl);
                intent.putExtra("price",selectPrice);
                intent.putExtra("user_ID",user_ID);
                intent.putExtra("position",sposition);
                intent.putExtra("pushID",selectPushID);
                startActivity(intent);

            }
        });





        addItem.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

               alert();




            }
        });

    }


    class MyAdapter  extends ArrayAdapter<String>
    {
        Context context;
        List<String> mTitle;
        List<String> mPrice;
        List<String> mImages;
        List<String> mpID;


        MyAdapter(Context c,List<String> title,List<String> sPrice,List<String> image,List<String> pID)
        {
            super(c,R.layout.row_view_item,title);
            this.context=c;
            this.mTitle=title;
            this.mPrice=sPrice;
            this.mImages=image;
            this.mpID=pID;
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            LayoutInflater layoutInflater=(LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row=layoutInflater.inflate(R.layout.row_view_item,parent,false);
            ImageView images= row.findViewById(R.id.imageView);
            TextView titles=row.findViewById(R.id.textView);
            TextView prices=row.findViewById(R.id.textPrice);

            final ImageView backImage=row.findViewById(R.id.imageViewBack);

            mdata.child(user_ID).child("item").child(click).child(subClick).child("backImage").child(mpID.get(position)).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshotB) {
                    if(dataSnapshotB.exists())
                    {
                        String backUrl=dataSnapshotB.getValue().toString();
                        Glide.with(ViewItem.this).load(backUrl).into(backImage);
                       /* Picasso.get().load(backUrl).memoryPolicy(MemoryPolicy.NO_STORE)
                                .networkPolicy(NetworkPolicy.NO_STORE).into(backImage);

                        */
                        backImage.setVisibility(View.VISIBLE);

                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });


           /* Picasso.get().load(mImages.get(position)).memoryPolicy(MemoryPolicy.NO_STORE)
                    .networkPolicy(NetworkPolicy.NO_STORE).into(images);

            */
            Glide.with(ViewItem.this).load(mImages.get(position)).into(images);

            titles.setText(mTitle.get(position).toUpperCase());
            prices.setText("$ "+ mPrice.get(position));

            return row;
        }
    }


    private void alert() {


        View view1=(LayoutInflater.from(ViewItem.this)).inflate(R.layout.itemdialogboxnew,null);
        AlertDialog.Builder alertdialog=new AlertDialog.Builder(ViewItem.this);
        alertdialog.setView(view1);


        final EditText getData=view1.findViewById(R.id.itemName);
        final EditText getPrice=view1.findViewById(R.id.itemPrice);
        final Button btn=view1.findViewById(R.id.itemAdd);
        Button btnBack=view1.findViewById(R.id.itemAddBack);
        imageView=view1.findViewById(R.id.itemSelect);
        imageViewBack=view1.findViewById(R.id.imageBack);

        getPrice.setHint("measurement cost");

        btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                showPictureDialog();
            }
        });

        btnBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                back=true;
                showPictureDialog();
            }
        });

        alertdialog.setCancelable(false)
                .setPositiveButton("ADD", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String s=getData.getText().toString();
                        String p=getPrice.getText().toString();
                        upload(s,p);



                    }

                })
                .setNegativeButton("CANCLE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });





        alertdialog.create();
        alertdialog.show();

    }

    private void showPictureDialog() {
        androidx.appcompat.app.AlertDialog.Builder pictureDialog = new androidx.appcompat.app.AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:

                                dispathTakePictureIntent();

                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {

                if(back)
                {
                    final Uri contentURIBack = data.getData();
                    filepathBack = contentURIBack;

                    Glide.with(ViewItem.this).load(contentURIBack).into(imageViewBack);
                    //Toast.makeText(ViewItem.this, "Image Saved!", Toast.LENGTH_SHORT).show();


                    back=false;
                }
                else
                {
                     Uri contentURI = data.getData();
                    filePath = contentURI;


                   // Toast.makeText(ViewItem.this, "Image Saved!", Toast.LENGTH_SHORT).show();


                    Glide.with(ViewItem.this).load(contentURI).into(imageView);


                }

            }

        } else if (requestCode == CAMERA) {


            if (back)
            {
                File fBack=new File(currentPhotoPath);
               // imageViewBack.setImageURI(Uri.fromFile(fBack));
                filepathBack=Uri.fromFile(fBack);

                Intent mediaScanIntent=new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                Uri contentUriBack=Uri.fromFile(fBack);
                Glide.with(ViewItem.this).load(contentUriBack).into(imageViewBack);
                mediaScanIntent.setData(contentUriBack);
                this.sendBroadcast(mediaScanIntent);

                back=false;
            }
            else
            {
                File f=new File(currentPhotoPath);
               // imageView.setImageURI(Uri.fromFile(f));
                filePath=Uri.fromFile(f);

                Intent mediaScanIntent=new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                Uri contentUri=Uri.fromFile(f);
                Glide.with(ViewItem.this).load(contentUri).into(imageView);
                mediaScanIntent.setData(contentUri);
                this.sendBroadcast(mediaScanIntent);

            }



        }
    }


    String currentPhotoPath;

    private  File createImageFile() throws IOException
    {
        String timeStamp=new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName="JPEG_"+timeStamp+"_";
        File storageDir=Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image=File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        currentPhotoPath=image.getAbsolutePath();
        return  image;
    }


    private  void dispathTakePictureIntent()
    {
        Intent takePictureIntent= new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(takePictureIntent.resolveActivity(getPackageManager())!= null) {
            File photoFile = null;
            try {
                {
                    photoFile = createImageFile();
                }
            } catch (IOException ex) {

            }

            if (photoFile != null) {
                Uri photoUri = FileProvider.getUriForFile(this, "com.example.android.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(takePictureIntent, CAMERA);
            }
        }
    }



    public void upload(final String s,final String p) {
        if (filePath != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Wait...");
            progressDialog.show();

            final StorageReference ref = mstorage.child("item/" + UUID.randomUUID().toString());
            ref.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    final String duri = uri.toString();


                                    if(filepathBack!=null)
                                    {
                                        final StorageReference refBack = mstorage.child("item/" + UUID.randomUUID().toString());
                                        refBack.putFile(filepathBack)
                                                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                                    @Override
                                                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                                        refBack.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                            @Override
                                                            public void onSuccess(Uri uriBack) {
                                                                String duriBack = uriBack.toString();


                                                                name.add(s.toUpperCase());
                                                                price.add(p);
                                                                imageString.add(duri);

                                                                String timeKey=new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                                                                pushID.add(timeKey);


                                                                mdata.child(user_ID).child("item").child(click).child(subClick).child("names").setValue(name);
                                                                mdata.child(user_ID).child("item").child(click).child(subClick).child("prices").setValue(price);
                                                                mdata.child(user_ID).child("item").child(click).child(subClick).child("images").setValue(imageString);
                                                                mdata.child(user_ID).child("item").child(click).child(subClick).child("pushID").setValue(pushID);

                                                                mdata.child(user_ID).child("item").child(click).child(subClick).child("backImage").child(timeKey).setValue(duriBack);

                                                                mdata.child(user_ID).child("item").child(click).child(subClick).child(s.toLowerCase()).child("featureState").setValue("0");


                                                                mdata.child(user_ID).child("measurements").child(timeKey).child("state").setValue("0");


                                                                mdata.child(user_ID).child("item").child(click).child(subClick).child(s.toLowerCase()).child("commenKey").setValue(timeKey);


                                                                HashMap<String,String> map=new HashMap();
                                                                map.put("subCategory",subClick.toLowerCase());
                                                                map.put("category",click.toLowerCase());
                                                                map.put("country",country.toLowerCase());
                                                                map.put("city",city.toLowerCase());
                                                                map.put("commonItemName",s.toLowerCase());
                                                                map.put("image",duri);
                                                                map.put("imageBack",duriBack);
                                                                map.put("tailorName",tailorName.toLowerCase());
                                                                map.put("user_ID",user_ID);
                                                                map.put("shopName",shopName.toLowerCase());
                                                                map.put("priceRange",p);
                                                                map.put("pushID",timeKey);
                                                                map.put("measureState","0");


                                                                //  DatabaseReference pushRef = mdata.child("allItems").push();
                                                                //   String key_ID = pushRef.getKey();
                                                                //   pushRef.setValue(map);

                                                                mdata.child("allItems").child(timeKey).setValue(map);
                                                                //    mdata.child(user_ID).child("item").child(click).child(subClick).child(s.toLowerCase()).child("itemKey").setValue(key_ID);


                                                                name.remove(name.size()-1);
                                                                price.remove(price.size()-1);
                                                                imageString.remove(imageString.size()-1);

                                                                int count=name.size();

                                                                if(count==0)
                                                                {
                                                                    listView.setAdapter(adapter);
                                                                }

                                                                else
                                                                {
                                                                    for(int i=name.size()-1;i>=0;i--)
                                                                    {
                                                                        name.remove(i);
                                                                        adapter.notifyDataSetChanged();
                                                                        price.remove(i);
                                                                        adapter.notifyDataSetChanged();
                                                                        imageString.remove(i);
                                                                        adapter.notifyDataSetChanged();
                                                                    }
                                                                }

                                                                final ArrayList<String> arr=new ArrayList<>();
                                                                final ArrayList<String> arrPrice=new ArrayList<>();

                                                                mdata.child(user_ID).child("item").child(click).child(subClick).child("names").addListenerForSingleValueEvent(new ValueEventListener() {
                                                                    @Override
                                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                                                                            data= dataSnapshot1.getValue().toString();
                                                                            arr.add(data.toUpperCase());


                                                                        }



                                                                    }

                                                                    @Override
                                                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                                                    }
                                                                });

                                                                mdata.child(user_ID).child("item").child(click).child(subClick).child("prices").addListenerForSingleValueEvent(new ValueEventListener() {
                                                                    @Override
                                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                        for (DataSnapshot dataSnapshot3 : dataSnapshot.getChildren()) {

                                                                            data= dataSnapshot3.getValue().toString();
                                                                            arrPrice.add(data);


                                                                        }



                                                                    }

                                                                    @Override
                                                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                                                    }
                                                                });





                                                                mdata.child(user_ID).child("item").child(click).child(subClick).child("images").addListenerForSingleValueEvent(new ValueEventListener() {
                                                                    @Override
                                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                        int i=0;
                                                                        for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()) {

                                                                            data2= dataSnapshot2.getValue().toString();
                                                                            name.add(arr.get(i));
                                                                            price.add(arrPrice.get(i));
                                                                            imageString.add(data2);
                                                                            adapter.notifyDataSetChanged();
                                                                            i++;

                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                                                    }
                                                                });



                                                            }
                                                        });
                                                    }
                                                });


                                    }

                                    else
                                    {
                                        name.add(s.toUpperCase());
                                        price.add(p);
                                        imageString.add(duri);

                                        String timeKey=new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                                        pushID.add(timeKey);


                                        mdata.child(user_ID).child("item").child(click).child(subClick).child("names").setValue(name);
                                        mdata.child(user_ID).child("item").child(click).child(subClick).child("prices").setValue(price);
                                        mdata.child(user_ID).child("item").child(click).child(subClick).child("images").setValue(imageString);
                                        mdata.child(user_ID).child("item").child(click).child(subClick).child("pushID").setValue(pushID);


                                        mdata.child(user_ID).child("item").child(click).child(subClick).child(s.toLowerCase()).child("featureState").setValue("0");


                                        mdata.child(user_ID).child("measurements").child(timeKey).child("state").setValue("0");


                                        mdata.child(user_ID).child("item").child(click).child(subClick).child(s.toLowerCase()).child("commenKey").setValue(timeKey);


                                        HashMap<String,String> map=new HashMap();
                                        map.put("subCategory",subClick.toLowerCase());
                                        map.put("category",click.toLowerCase());
                                        map.put("country",country.toLowerCase());
                                        map.put("city",city.toLowerCase());
                                        map.put("commonItemName",s.toLowerCase());
                                        map.put("image",duri);
                                        map.put("tailorName",tailorName.toLowerCase());
                                        map.put("user_ID",user_ID);
                                        map.put("shopName",shopName.toLowerCase());
                                        map.put("priceRange",p);
                                        map.put("pushID",timeKey);
                                        map.put("measureState","0");


                                        //  DatabaseReference pushRef = mdata.child("allItems").push();
                                        //   String key_ID = pushRef.getKey();
                                        //   pushRef.setValue(map);

                                        mdata.child("allItems").child(timeKey).setValue(map);
                                        //    mdata.child(user_ID).child("item").child(click).child(subClick).child(s.toLowerCase()).child("itemKey").setValue(key_ID);


                                        name.remove(name.size()-1);
                                        price.remove(price.size()-1);
                                        imageString.remove(imageString.size()-1);

                                        int count=name.size();

                                        if(count==0)
                                        {
                                            listView.setAdapter(adapter);
                                        }

                                        else
                                        {
                                            for(int i=name.size()-1;i>=0;i--)
                                            {
                                                name.remove(i);
                                                adapter.notifyDataSetChanged();
                                                price.remove(i);
                                                adapter.notifyDataSetChanged();
                                                imageString.remove(i);
                                                adapter.notifyDataSetChanged();
                                            }
                                        }

                                        final ArrayList<String> arr=new ArrayList<>();
                                        final ArrayList<String> arrPrice=new ArrayList<>();

                                        mdata.child(user_ID).child("item").child(click).child(subClick).child("names").addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                                                    data= dataSnapshot1.getValue().toString();
                                                    arr.add(data.toUpperCase());


                                                }



                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                            }
                                        });

                                        mdata.child(user_ID).child("item").child(click).child(subClick).child("prices").addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                for (DataSnapshot dataSnapshot3 : dataSnapshot.getChildren()) {

                                                    data= dataSnapshot3.getValue().toString();
                                                    arrPrice.add(data);


                                                }



                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                            }
                                        });





                                        mdata.child(user_ID).child("item").child(click).child(subClick).child("images").addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                int i=0;
                                                for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()) {

                                                    data2= dataSnapshot2.getValue().toString();
                                                    name.add(arr.get(i));
                                                    price.add(arrPrice.get(i));
                                                    imageString.add(data2);
                                                    adapter.notifyDataSetChanged();
                                                    i++;

                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                            }
                                        });

                                    }




                                }
                            });
                            progressDialog.dismiss();

                            Toast.makeText(ViewItem.this, "Succesful", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(ViewItem.this, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                            progressDialog.setMessage("Successful " );
                        }
                    });
        }



    }

}
