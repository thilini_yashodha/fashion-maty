package com.example.fashiondesigner;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.renderscript.ScriptGroup;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.model.Model;
import com.example.fashiondesigner.Adapter.Model.HorizontalModel;
import com.example.fashiondesigner.Adapter.Model.searchModel;
import com.example.fashiondesigner.Adapter.VerticalRecyclerViewAdapter;
import com.example.fashiondesigner.Adapter.searchAdupter;
import com.example.fashiondesigner.measurements.gentMeasuremet;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class HomeFragment extends Fragment {


    RecyclerView listView;
    private ImageButton mSEarchBtn;
    private AutoCompleteTextView mSearchField;
    private DatabaseReference mdata,mdataRateMean,mdataTRev;

    String uid;

    ArrayList<searchModel> searchArray;
    searchAdupter adapter;

    String c="";
    String result="r";
    Boolean flag=false;
    TextView filterText;
    int count=0,carrier=0;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragmnet_search,null);

        listView=view.findViewById(R.id.result_list);
        mSEarchBtn=view.findViewById(R.id.buttonSearch);
        mSearchField=view.findViewById(R.id.searchField);
        ImageButton filter=view.findViewById(R.id.filter);
        filterText=view.findViewById(R.id.filterText);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        uid=user.getUid();

        mdata=FirebaseDatabase.getInstance().getReference().child("allItems");
        mdataRateMean=FirebaseDatabase.getInstance().getReference().child("rateMean");
        mdataTRev=FirebaseDatabase.getInstance().getReference();

        searchArray=new ArrayList<>();

        listView.setHasFixedSize(true);
        listView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));

        adapter=new searchAdupter(getActivity(),searchArray);
        listView.setAdapter(adapter);

        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert();

            }
        });


        String[] dress = {"frock","blouse","coat","jeans","skirt","shirt","tshirt","trousers",
                           "casual blouse","office blouse","over coat","rain coat","party frock","casual frock","maxi frock",
                           "tight jeans,bell bottom","casual shirt","dress shirt","office shirt","layered skirt,line skirt",
                           "slim fit trousers","cargo pants","casual tshirt","printed tshirt","logo tshirt","long tshirt"};

        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (getActivity(),android.R.layout.simple_list_item_1,dress);
        mSearchField.setAdapter(adapter);

        mSEarchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d("result",result);



                if(flag)
                {

                    searchByShop();
                }

                else
                {

                    searchByMainCategory();


                }



            }
        });




        return view;

    }

    private void searchByMainCategory() {

        filterText.setText("");
        count=0;

        if(searchArray.size()!=0)
        {
            for(int i=searchArray.size();i>0;i--)
            {
                searchArray.remove(i-1);
                adapter.notifyDataSetChanged();
            }
        }


        final String s=mSearchField.getText().toString().toLowerCase();

        if(!s.equals(""))
        {
            Query findQuery1=mdata.orderByChild("category").startAt(s).endAt(s+"\uf8ff");

            findQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists())
                    {
                        int i=0,j=0;
                        carrier=0;


                        if ((result.equals("r")) || (result.equals("-no special-")))
                        {
                            for (DataSnapshot dataSnapshot2:dataSnapshot.getChildren())
                            {

                                searchModel smodel;
                                smodel=dataSnapshot2.getValue(searchModel.class);

                                if(dataSnapshot2.child("measureState").exists())
                                {
                                    String measureState=smodel.getMeasureState();

                                    if(measureState.equals("1"))
                                    {
                                        searchArray.add(smodel);
                                        searchArray.get(i).setCheck("subCategory");

                                        searchArray.get(i).setShowStar(true);
                                        searchArray.get(i).setStarCount(Float.valueOf(searchArray.get(i).getStarRev()));

                                        if(searchArray.get(i).getStarRev().equals("0"))
                                        {
                                            carrier=i;
                                            mdataRateMean.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshotRM) {
                                                    if(dataSnapshotRM.exists())
                                                    {
                                                        String rateMean=dataSnapshotRM.child("meanRate").getValue().toString();
                                                        searchArray.get(carrier).setStarRev(rateMean);

                                                    }
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                }
                                            });

                                        }


                                        i++;

                                    }

                                }

                                if(searchArray.size()==0)
                                {
                                    searchByItemName(s);
                                }

                            }


                            Collections.sort(searchArray, new Comparator<searchModel>() {
                                @Override
                                public int compare(searchModel lhs, searchModel rhs) {
                                    return rhs.getStarRev().compareTo(lhs.getStarRev());

                                }
                            });

                            for (int k=0;k<searchArray.size();k++)
                            {
                                adapter.notifyDataSetChanged();
                            }


                        }
                        else
                        {
                            if (c.equals(""))
                            {
                                String t= "COUNTRY : "+result.toUpperCase();
                                filterText.setText(t);
                                ArrayList<searchModel> temp=new ArrayList<>();

                                for (DataSnapshot dataSnapshot2:dataSnapshot.getChildren())
                                {
                                    searchModel smodel;
                                    smodel=dataSnapshot2.getValue(searchModel.class);
                                    temp.add(smodel);

                                    String ct = temp.get(j).getCountry().toLowerCase();

                                    if(ct.equals(result.toLowerCase()))
                                    {
                                        if(dataSnapshot2.child("measureState").exists())
                                        {
                                            String measureState=smodel.getMeasureState();

                                            if(measureState.equals("1"))
                                            {
                                                searchArray.add(smodel);
                                                searchArray.get(i).setCheck("subCategory");

                                                searchArray.get(i).setShowStar(true);
                                                searchArray.get(i).setStarCount(Float.valueOf(searchArray.get(i).getStarRev()));

                                                if(searchArray.get(i).getStarRev().equals("0"))
                                                {
                                                    carrier=i;
                                                    mdataRateMean.addListenerForSingleValueEvent(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(@NonNull DataSnapshot dataSnapshotRM) {
                                                            if(dataSnapshotRM.exists())
                                                            {
                                                                String rateMean=dataSnapshotRM.child("meanRate").getValue().toString();
                                                                searchArray.get(carrier).setStarRev(rateMean);
                                                            }
                                                        }

                                                        @Override
                                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                                        }
                                                    });

                                                }

                                                i++;

                                            }

                                        }
                                    }

                                    j++;

                                }


                                if(searchArray.size()==0)
                                {

                                    searchByItemName(s);

                                }

                                Collections.sort(searchArray, new Comparator<searchModel>() {
                                    @Override
                                    public int compare(searchModel lhs, searchModel rhs) {
                                        return rhs.getStarRev().compareTo(lhs.getStarRev());

                                    }
                                });

                                for (int k=0;k<searchArray.size();k++)
                                {
                                    adapter.notifyDataSetChanged();
                                }


                            }

                            else
                            {
                                String t="COUNTRY : "+result.toUpperCase()+" - CITY :"+ c.toUpperCase();
                                filterText.setText(t);
                                ArrayList<searchModel> temp=new ArrayList<>();

                                for (DataSnapshot dataSnapshot2:dataSnapshot.getChildren())
                                {
                                    searchModel smodel;
                                    smodel=dataSnapshot2.getValue(searchModel.class);
                                    temp.add(smodel);

                                    String ct = temp.get(j).getCountry().toLowerCase();
                                    String tt=temp.get(j).getCity().toLowerCase();

                                    if(ct.equals(result.toLowerCase()) && tt.equals(c.toLowerCase()) )
                                    {
                                        if(dataSnapshot2.child("measureState").exists())
                                        {
                                            String measureState=smodel.getMeasureState();

                                            if(measureState.equals("1"))
                                            {
                                                searchArray.add(smodel);
                                                searchArray.get(i).setCheck("subCategory");

                                                searchArray.get(i).setShowStar(true);
                                                searchArray.get(i).setStarCount(Float.valueOf(searchArray.get(i).getStarRev()));

                                                if(searchArray.get(i).getStarRev().equals("0"))
                                                {
                                                    carrier=i;
                                                    mdataRateMean.addListenerForSingleValueEvent(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(@NonNull DataSnapshot dataSnapshotRM) {
                                                            if(dataSnapshotRM.exists())
                                                            {
                                                                String rateMean=dataSnapshotRM.child("meanRate").getValue().toString();
                                                                searchArray.get(carrier).setStarRev(rateMean);
                                                            }
                                                        }

                                                        @Override
                                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                                        }
                                                    });

                                                }

                                                i++;
                                                carrier++;
                                            }

                                        }
                                    }

                                    j++;

                                }

                                if(searchArray.size()==0)
                                {
                                    searchByItemName(s);
                                }

                                Collections.sort(searchArray, new Comparator<searchModel>() {
                                    @Override
                                    public int compare(searchModel lhs, searchModel rhs) {
                                        return rhs.getStarRev().compareTo(lhs.getStarRev());

                                    }
                                });

                                for (int k=0;k<searchArray.size();k++)
                                {
                                    adapter.notifyDataSetChanged();
                                }



                            }

                        }



                    }
                    else
                    {
                        searchByItemName(s);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }


    }

    private void searchByShop() {

        filterText.setText("");

        if(searchArray.size()!=0)
        {
            for(int i=searchArray.size();i>0;i--)
            {
                searchArray.remove(i-1);
                adapter.notifyDataSetChanged();
            }
        }

        String s=mSearchField.getText().toString().toLowerCase();

       final ArrayList<String> ID=new ArrayList<>();
        final ArrayList<searchModel> tempArr=new ArrayList<>();


        if(!s.equals(""))
        {
            Query findQuery1=mdata.orderByChild("shopName").startAt(s).endAt(s+"\uf8ff");

            findQuery1.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        int i = 0, j = 0;

                        if ((result.equals("r")) || (result.equals("-no special-"))) {

                                for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()) {

                                    searchModel smodel;
                                    smodel = dataSnapshot2.getValue(searchModel.class);

                                    tempArr.add(smodel);


                                    if (!ID.contains(tempArr.get(i).getUser_ID())) {
                                        searchArray.add(smodel);
                                        ID.add(tempArr.get(i).getUser_ID());
                                        searchArray.get(j).setCheck("shop");
                                        carrier=j;

                                        mdataTRev.child(tempArr.get(i).getUser_ID()).child("reviews").child("reviewData").child("totalReviews").addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshotTR) {
                                                if(dataSnapshotTR.exists())
                                                {
                                                    float tStar=Float.valueOf(dataSnapshotTR.getValue().toString());
                                                    searchArray.get(carrier).setShowStar(true);
                                                    searchArray.get(carrier).setStarCount(tStar);

                                                    adapter.notifyDataSetChanged();

                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                            }
                                        });

                                        j++;
                                    }


                                    i++;
                                }



                        }

                        else {

                            if (c.equals(""))
                            {
                                String t= "COUNTRY : "+result.toUpperCase();
                                filterText.setText(t);

                                for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()) {

                                    searchModel smodel;
                                    smodel = dataSnapshot2.getValue(searchModel.class);

                                    tempArr.add(smodel);

                                    String ct = tempArr.get(i).getCountry().toLowerCase();

                                    if (ct.equals(result.toLowerCase())) {
                                        if (!ID.contains(tempArr.get(i).getUser_ID())) {
                                            searchArray.add(smodel);
                                            ID.add(tempArr.get(i).getUser_ID());
                                            searchArray.get(j).setCheck("shop");

                                            carrier=j;

                                            mdataTRev.child(tempArr.get(i).getUser_ID()).child("reviews").child("totalReviews").addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshotTR) {
                                                    if(dataSnapshotTR.exists())
                                                    {
                                                        float tStar=Float.valueOf(dataSnapshotTR.getValue().toString());
                                                        searchArray.get(carrier).setShowStar(true);
                                                        searchArray.get(carrier).setStarCount(tStar);
                                                        adapter.notifyDataSetChanged();
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                }
                                            });


                                            j++;
                                        }


                                        i++;
                                    }

                                }

                            }

                            else {

                                String t="COUNTRY : "+result.toUpperCase()+" - CITY :"+ c.toUpperCase();
                                filterText.setText(t);

                                for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()) {

                                    searchModel smodel;
                                    smodel = dataSnapshot2.getValue(searchModel.class);

                                    tempArr.add(smodel);

                                    String ct = tempArr.get(i).getCountry().toLowerCase();
                                    String tt = tempArr.get(i).getCity().toLowerCase();

                                    if (ct.equals(result.toLowerCase()) && tt.equals(c.toLowerCase())) {
                                        if (!ID.contains(tempArr.get(i).getUser_ID())) {
                                            searchArray.add(smodel);
                                            ID.add(tempArr.get(i).getUser_ID());
                                            searchArray.get(j).setCheck("shop");

                                            carrier=j;

                                            mdataTRev.child(tempArr.get(i).getUser_ID()).child("reviews").child("totalReviews").addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshotTR) {
                                                    if(dataSnapshotTR.exists())
                                                    {
                                                        float tStar=Float.valueOf(dataSnapshotTR.getValue().toString());
                                                        searchArray.get(carrier).setShowStar(true);
                                                        searchArray.get(carrier).setStarCount(tStar);
                                                        adapter.notifyDataSetChanged();
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                }
                                            });


                                            j++;
                                        }


                                        i++;

                                    }

                                }


                            }

                        }

                        if(searchArray.size()==0)
                        {
                            while (count<=0)
                            {
                                final Toast toast=Toast.makeText(getActivity(),"No item to show",Toast.LENGTH_SHORT);
                                toast.show();
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        toast.cancel();
                                    }
                                }, 5000);


                                count=count+1;

                            }
                        }

                        flag = false;
                        result = "r";
                    }

                    else
                    {
                        if(searchArray.size()==0)
                        {
                            while (count<=0)
                            {
                                final Toast toast=Toast.makeText(getActivity(),"No item to show",Toast.LENGTH_SHORT);
                                toast.show();
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        toast.cancel();
                                    }
                                }, 5000);


                                count=count+1;

                            }
                        }

                        flag = false;
                        result = "r";
                    }


                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }



    }

    private void searchByItemName(String ss) {

        count=0;

        if(searchArray.size()!=0)
        {
            for(int i=searchArray.size();i>0;i--)
            {
                searchArray.remove(i-1);
                adapter.notifyDataSetChanged();
            }
        }

       Log.d("fff","fff");
        search(ss);

    }


    private void search(final String s) {

        Query findQuery=mdata.orderByChild("subCategory").startAt(s).endAt(s+"\uf8ff");

       findQuery.addListenerForSingleValueEvent(new ValueEventListener() {
           @Override
           public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
               if(dataSnapshot.exists())
               {
                   int i=0,j=0;


                   if ((result.equals("r")) || (result.equals("-no special-")))
                   {
                       for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren())
                       {
                           searchModel smodel ;
                           smodel=dataSnapshot1.getValue(searchModel.class);

                           if(dataSnapshot1.child("measureState").exists())
                           {
                               String measureState=smodel.getMeasureState();

                               if(measureState.equals("1"))
                               {
                                   searchArray.add(smodel);
                                   searchArray.get(i).setCheck("subCategory");

                                   searchArray.get(i).setShowStar(true);
                                   searchArray.get(i).setStarCount(Float.valueOf(searchArray.get(i).getStarRev()));

                                   if(searchArray.get(i).getStarRev().equals("0"))
                                   {
                                       carrier=i;
                                       mdataRateMean.addListenerForSingleValueEvent(new ValueEventListener() {
                                           @Override
                                           public void onDataChange(@NonNull DataSnapshot dataSnapshotRM) {
                                               if(dataSnapshotRM.exists())
                                               {
                                                   String rateMean=dataSnapshotRM.child("meanRate").getValue().toString();
                                                   searchArray.get(carrier).setStarRev(rateMean);
                                               }
                                           }

                                           @Override
                                           public void onCancelled(@NonNull DatabaseError databaseError) {

                                           }
                                       });

                                   }

                                   i++;

                               }
                           }

                       }

                       Collections.sort(searchArray, new Comparator<searchModel>() {
                           @Override
                           public int compare(searchModel lhs, searchModel rhs) {
                               return rhs.getStarRev().compareTo(lhs.getStarRev());

                           }
                       });

                       for (int k=0;k<searchArray.size();k++)
                       {
                           adapter.notifyDataSetChanged();
                       }



                   }
                   else
                   {
                       if (c.equals(""))
                       {
                           Log.d("ggg","ggg");
                           String t="COUNTRY : "+result.toUpperCase();
                           filterText.setText(t);
                           ArrayList<searchModel> temp=new ArrayList<>();

                           for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren())
                           {
                               searchModel smodel ;
                               smodel=dataSnapshot1.getValue(searchModel.class);
                               temp.add(smodel);

                               String cnt = temp.get(j).getCountry().toLowerCase();

                               if(cnt.equals(result.toLowerCase()))
                               {
                                   if(dataSnapshot1.child("measureState").exists())
                                   {
                                       String measureState=smodel.getMeasureState();

                                       if(measureState.equals("1"))
                                       {
                                           searchArray.add(smodel);
                                           searchArray.get(i).setCheck("subCategory");

                                           searchArray.get(i).setShowStar(true);
                                           searchArray.get(i).setStarCount(Float.valueOf(searchArray.get(i).getStarRev()));

                                           if(searchArray.get(i).getStarRev().equals("0"))
                                           {
                                               carrier=i;
                                               mdataRateMean.addListenerForSingleValueEvent(new ValueEventListener() {
                                                   @Override
                                                   public void onDataChange(@NonNull DataSnapshot dataSnapshotRM) {
                                                       if(dataSnapshotRM.exists())
                                                       {
                                                           String rateMean=dataSnapshotRM.child("meanRate").getValue().toString();
                                                           searchArray.get(carrier).setStarRev(rateMean);
                                                       }
                                                   }

                                                   @Override
                                                   public void onCancelled(@NonNull DatabaseError databaseError) {

                                                   }
                                               });

                                           }

                                           i++;

                                       }
                                   }

                               }

                               j++;

                           }

                           Collections.sort(searchArray, new Comparator<searchModel>() {
                               @Override
                               public int compare(searchModel lhs, searchModel rhs) {
                                   return rhs.getStarRev().compareTo(lhs.getStarRev());

                               }
                           });

                           for (int k=0;k<searchArray.size();k++)
                           {
                               adapter.notifyDataSetChanged();
                           }



                       }

                       else
                       {
                           String t="COUNTRY : "+result.toUpperCase()+" - CITY :"+ c.toUpperCase();
                           filterText.setText(t);
                           ArrayList<searchModel> temp=new ArrayList<>();

                           for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren())
                           {
                               searchModel smodel ;
                               smodel=dataSnapshot1.getValue(searchModel.class);
                               temp.add(smodel);

                               String cnt = temp.get(j).getCountry().toLowerCase();
                               String ctt=temp.get(j).getCity().toLowerCase();

                               if(cnt.equals(result.toLowerCase()) && ctt.equals(c.toLowerCase()) )
                               {
                                   if(dataSnapshot1.child("measureState").exists())
                                   {
                                       String measureState=smodel.getMeasureState();

                                       if(measureState.equals("1"))
                                       {
                                           searchArray.add(smodel);
                                           searchArray.get(i).setCheck("subCategory");

                                           searchArray.get(i).setShowStar(true);
                                           searchArray.get(i).setStarCount(Float.valueOf(searchArray.get(i).getStarRev()));

                                           if(searchArray.get(i).getStarRev().equals("0"))
                                           {
                                               carrier=i;
                                               mdataRateMean.addListenerForSingleValueEvent(new ValueEventListener() {
                                                   @Override
                                                   public void onDataChange(@NonNull DataSnapshot dataSnapshotRM) {
                                                       if(dataSnapshotRM.exists())
                                                       {
                                                           String rateMean=dataSnapshotRM.child("meanRate").getValue().toString();
                                                           searchArray.get(carrier).setStarRev(rateMean);
                                                       }
                                                   }

                                                   @Override
                                                   public void onCancelled(@NonNull DatabaseError databaseError) {

                                                   }
                                               });

                                           }

                                           i++;

                                       }
                                   }

                               }

                               j++;

                           }

                           Collections.sort(searchArray, new Comparator<searchModel>() {
                               @Override
                               public int compare(searchModel lhs, searchModel rhs) {
                                   return rhs.getStarRev().compareTo(lhs.getStarRev());

                               }
                           });

                           for (int k=0;k<searchArray.size();k++)
                           {
                               adapter.notifyDataSetChanged();
                           }




                       }

                   }

                   if(searchArray.size()==0)
                   {

                       Log.d("hhh","hhh");


                           while (count<=0)
                           {
                               final Toast toast=Toast.makeText(getActivity(),"No item to show",Toast.LENGTH_SHORT);
                               toast.show();
                               Handler handler = new Handler();
                               handler.postDelayed(new Runnable() {
                                   @Override
                                   public void run() {
                                       toast.cancel();
                                   }
                               }, 5000);


                               count=count+1;

                           }


                   }




                   flag=false;
                   result="r";


               }
               else
               {
                   if(searchArray.size()==0)
                   {

                       Log.d("ttt","ttt");


                       while (count<=0)
                       {
                           final Toast toast=Toast.makeText(getActivity(),"No item to show",Toast.LENGTH_SHORT);
                           toast.show();
                           Handler handler = new Handler();
                           handler.postDelayed(new Runnable() {
                               @Override
                               public void run() {
                                   toast.cancel();
                               }
                           }, 5000);


                           count=count+1;

                       }


                   }




                   flag=false;
                   result="r";

               }



           }

           @Override
           public void onCancelled(@NonNull DatabaseError databaseError) {

           }
       });



    }




    private void alert() {

        View view1=(LayoutInflater.from(getActivity())).inflate(R.layout.filter,null);
        AlertDialog.Builder alertdialog=new AlertDialog.Builder(getActivity());
        alertdialog.setView(view1);

        final Spinner spinner=view1.findViewById(R.id.spiner);
        final EditText city=view1.findViewById(R.id.city);
        final CheckBox checkShop=view1.findViewById(R.id.shopCheck);

        Locale[] locales = Locale.getAvailableLocales();
        ArrayList<String> countries = new ArrayList<String>();
        for (Locale locale : locales) {
            String country = locale.getDisplayCountry();
            if (country.trim().length() > 0 && !countries.contains(country)) {
                countries.add(country);
            }
        }
        Collections.sort(countries);

        countries.set(0,"-no special-");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, countries);
        // set the view for the Drop down list
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // set the ArrayAdapter to the spinner
        spinner.setAdapter(dataAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                result=spinner.getSelectedItem().toString();

                if(!(result.equals("-no special-")))
                {
                    city.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });





        alertdialog.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        result=spinner.getSelectedItem().toString();
                        c=city.getText().toString();


                        flag = checkShop.isChecked();

                        if(flag)
                        {
                            Log.d("flag","1");
                        }




                    }

                })
                .setNegativeButton("CANCLE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });





        alertdialog.create();
        alertdialog.show();

    }




}
