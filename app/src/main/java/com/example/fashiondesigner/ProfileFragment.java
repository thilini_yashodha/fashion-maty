package com.example.fashiondesigner;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;

import static android.app.Activity.RESULT_OK;

public class ProfileFragment extends Fragment {

    private static final int PERMISSION_REQUEST = 0;
    private static final int RESULT_LOAD_IMAGE = 1;

    DatabaseReference mdata;
    FirebaseUser user;
    StorageReference mstorage;

    String user_ID;
    String fname,fage;
    ProgressDialog progressDialog;
    ImageView image;
    TextView name,tele,email;

    Button setName,setImage,setEmail,setTele;
    public Uri iuri;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_profile,null);

        user= FirebaseAuth.getInstance().getCurrentUser();
        user_ID=user.getUid();
        mdata=FirebaseDatabase.getInstance().getReference();
        mstorage=FirebaseStorage.getInstance().getReference();

        progressDialog=new ProgressDialog(getActivity());

        image=view.findViewById(R.id.customeImage);
        name=view.findViewById(R.id.namec);
        email=view.findViewById(R.id.emailc);
        tele=view.findViewById(R.id.telec);

        setName=view.findViewById(R.id.ceditName);
        setImage=view.findViewById(R.id.ceditImage);
        setEmail=view.findViewById(R.id.ceditEmail);
        setTele=view.findViewById(R.id.ceditTele);

        display();

        Button logout=view.findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(getActivity(),MainActivity.class));
                getActivity().onBackPressed();
              //  getActivity().finish();
            }
        });


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST);
        }


        setImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);

            }
        });

        setName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alert(0);
            }
        });

        setEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alert(1);
            }
        });

        setTele.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alert(2);
            }
        });



        return view;
    }




    private void display() {
        mdata.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String cname=dataSnapshot.child(user_ID).child("customerData").child("name").getValue().toString();
                name.setText(cname);
                String cemail=dataSnapshot.child(user_ID).child("customerData").child("email").getValue().toString();
                email.setText(cemail);
                String ctele=dataSnapshot.child(user_ID).child("customerData").child("tele").getValue().toString();
                tele.setText(ctele);
                String imageUrl=dataSnapshot.child(user_ID).child("customerData").child("imageUrl").getValue().toString();
                Glide.with(getActivity()).load(imageUrl).into(image);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                Toast.makeText(getActivity(),"Error!",Toast.LENGTH_SHORT).show();

            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(), "permission granted !", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(getActivity(), "Permission not granted!", Toast.LENGTH_SHORT).show();
                getActivity().finish();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == RESULT_LOAD_IMAGE) {

                iuri = data.getData();
                InputStream inputStream;

                try {
                    inputStream = getActivity().getContentResolver().openInputStream(iuri);
                    Bitmap Image = BitmapFactory.decodeStream(inputStream);
                    image.setImageBitmap(Image);
                    upload();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "UNABLE TO OPEN IMAGE!", Toast.LENGTH_SHORT).show();
                }

            }
        }
    }


    public void upload()
    {
        if (iuri != null) {
            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            final StorageReference ref =mstorage.child("customer_profile").child(user_ID);
            ref.putFile(iuri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    String duri = uri.toString();

                                    mdata.child(user_ID).child("customerData").child("imageUrl").setValue(duri);


                                }
                            });
                            progressDialog.dismiss();

                            Toast.makeText(getActivity(), "Uploaded", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setMessage("Uploaded " + (int) progress + "%");
                        }
                    });
        }

        else
        {
            Toast.makeText(getActivity(), "Unccessful!", Toast.LENGTH_SHORT).show();
        }
    }


    public void alert(final int x)
    {
        View view=(LayoutInflater.from(getActivity())).inflate(R.layout.tailordalogbox,null);
        AlertDialog.Builder alertdialog=new AlertDialog.Builder(getActivity());
        alertdialog.setView(view);


        final EditText getData=view.findViewById(R.id.reenterbn);


        alertdialog.setCancelable(false)
                .setPositiveButton("EDIT", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if(x==0)
                        {
                            String s=getData.getText().toString();
                            name.setText(s);
                            mdata.child(user_ID).child("customerData").child("name").setValue(s);

                        }

                        if(x==1)
                        {
                            String s1=getData.getText().toString();
                            email.setText(s1);
                            mdata.child(user_ID).child("customerData").child("email").setValue(s1);

                        }

                        if(x==2)
                        {
                            String s2=getData.getText().toString();
                            tele.setText(s2);
                            mdata.child(user_ID).child("customerData").child("tele").setValue(s2);

                        }

                    }
                })
                .setNegativeButton("CANCLE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });



        alertdialog.create();
        alertdialog.show();
    }


}
