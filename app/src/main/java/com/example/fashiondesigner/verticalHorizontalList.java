  package com.example.fashiondesigner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.fashiondesigner.Adapter.HorizontalREcyclerViewAdapter;
import com.example.fashiondesigner.Adapter.Model.HorizontalModel;
import com.example.fashiondesigner.Adapter.Model.VerticalModel;
import com.example.fashiondesigner.Adapter.VerticalRecyclerViewAdapter;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

  public class verticalHorizontalList extends AppCompatActivity {

      RecyclerView verticalRecyclerView;
      VerticalRecyclerViewAdapter adapter;
      ArrayList<VerticalModel> arrayListVertical;

      String click,clickItemName,user_ID,subClick,commanPushID;
      DatabaseReference mdata;
      StorageReference mstorage;

      private int GALLERY = 1, CAMERA = 2;
      public Uri filePath;
      public Uri filepathBack;

      String key=";";
      ArrayList<Integer> keyArr=new ArrayList<>();

      ImageView getImage,imageViewBack;
      Boolean flag=false;
      String savedUri;

      String itemKey=null;
      ImageView imageView;
      Boolean back=false;

      String country,city,shopName,tailorName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vertical_horizontal_list);

        Intent intent = getIntent();
        click = intent.getStringExtra("clickCategory").toLowerCase();
        subClick = intent.getStringExtra("subClickCategory").toLowerCase();
        clickItemName=intent.getStringExtra("clickItemName").toLowerCase();



        FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
        user_ID=user.getUid();
        mdata= FirebaseDatabase.getInstance().getReference().child(user_ID).child("item").child(click).child(subClick).child(clickItemName);
        mstorage= FirebaseStorage.getInstance().getReference();

        TextView clickItem=findViewById(R.id.clickItem);
        clickItem.setText(clickItemName);


        Button addFeature=findViewById(R.id.addFeature);

        Button addDress=findViewById(R.id.addaFullDress);
        CardView gotoShow=findViewById(R.id.gotoShow);

      //  Button fabric=findViewById(R.id.fabric);

        DatabaseReference tdetailData=FirebaseDatabase.getInstance().getReference();

        tdetailData.child(user_ID).child("tailorData").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                country=dataSnapshot.child("country").getValue().toString();
                city=dataSnapshot.child("city").getValue().toString();
                shopName=dataSnapshot.child("businessName").getValue().toString();
                tailorName=dataSnapshot.child("tailorName").getValue().toString();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



        gotoShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(verticalHorizontalList.this ,showList.class);
                intent.putExtra("clickCategory",click);
                intent.putExtra("subClickCategory",subClick);
                intent.putExtra("clickItemName",clickItemName);
                startActivity(intent);
            }
        });




        addFeature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                alert();
            }
        });

        addDress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(stringKeyArray.size()!=0)
                {
                    flag=true;
                    alertHorizontal();
                }
                else
                {
                    Toast.makeText(verticalHorizontalList.this,"Please sealect the features first!",Toast.LENGTH_SHORT).show();

                }


            }
        });





        arrayListVertical=new ArrayList<>();

         verticalRecyclerView=findViewById(R.id.recyclerView);
         verticalRecyclerView.setHasFixedSize(true);

        verticalRecyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));


        adapter=new VerticalRecyclerViewAdapter(this,arrayListVertical);
        verticalRecyclerView.setAdapter(adapter);



        mdata.child("features").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    String data= dataSnapshot1.getValue().toString();
                    VerticalModel mVerticalModel=new VerticalModel();

                    mVerticalModel.setID(user_ID);
                    mVerticalModel.setClick(click);
                    mVerticalModel.setsubClick(subClick);
                    mVerticalModel.setClickItemName(clickItemName);

                    mVerticalModel.setTitle(data.toUpperCase());
                    mVerticalModel.setFselect(true);

                    arrayListVertical.add(mVerticalModel);

                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }




      private void alertHorizontal() {

          View view1=(LayoutInflater.from(verticalHorizontalList.this)).inflate(R.layout.itemdialogboxnew,null);
          AlertDialog.Builder alertdialog=new AlertDialog.Builder(verticalHorizontalList.this);
          alertdialog.setView(view1);


          final EditText getData=view1.findViewById(R.id.itemName);
          getImage=view1.findViewById(R.id.itemSelect);
          imageViewBack=view1.findViewById(R.id.imageBack);
          final EditText getPrice=view1.findViewById(R.id.itemPrice);
          final Button btn=view1.findViewById(R.id.itemAdd);
          Button btnBack=view1.findViewById(R.id.itemAddBack);

          getPrice.setHint("measurement cost");

          btn.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                  showPictureDialog();
              }
          });

          btnBack.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  back=true;
                  showPictureDialog();
              }
          });



          alertdialog.setCancelable(false)
                  .setPositiveButton("ADD", new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialog, int which) {



                          String s=getData.getText().toString();
                          String p=getPrice.getText().toString();
                          uploadHorizontal(s,p);
                          flag=false;



                      }

                  })
                  .setNegativeButton("CANCLE", new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialog, int which) {
                          dialog.cancel();
                      }
                  });





          alertdialog.create();
          alertdialog.show();


      }



      private void alert() {

          View view1=(LayoutInflater.from(verticalHorizontalList.this)).inflate(R.layout.tailordalogbox,null);
          AlertDialog.Builder alertdialog=new AlertDialog.Builder(verticalHorizontalList.this);
          alertdialog.setView(view1);


          final EditText getData=view1.findViewById(R.id.reenterbn);
          getData.setHint("Eg : 'collar type', 'sleeves type'");


          alertdialog.setCancelable(false)
                  .setPositiveButton("ADD", new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialog, int which) {

                         String s=getData.getText().toString();

                         mdata.child("features").child(s.toLowerCase()).setValue(s.toLowerCase());

                          VerticalModel mVerticalModel=new VerticalModel();
                          mVerticalModel.setID(user_ID);
                          mVerticalModel.setClick(click);
                          mVerticalModel.setsubClick(subClick);
                          mVerticalModel.setClickItemName(clickItemName);
                          mVerticalModel.setTitle(s.toUpperCase());
                          mVerticalModel.setFselect(true);

                          arrayListVertical.add(mVerticalModel);

                          adapter.notifyDataSetChanged();

                      }

                  })
                  .setNegativeButton("CANCLE", new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialog, int which) {
                          dialog.cancel();
                      }
                  });





          alertdialog.create();
          alertdialog.show();

      }

      @Override
      public void onActivityResult(int requestCode, int resultCode, Intent data) {

          super.onActivityResult(requestCode, resultCode, data);
          if (resultCode == RESULT_CANCELED) {
              return;
          }
          if (requestCode == GALLERY) {
              if (data != null) {
                  Uri contentURI = data.getData();
                  try {
                      Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), contentURI);


                      if(flag)
                      {
                         // getImage.setImageBitmap(bitmap);



                          if(back)
                          {
                              final Uri contentURIBack = data.getData();
                              filepathBack = contentURIBack;

                              Glide.with(verticalHorizontalList.this).load(contentURIBack).into(imageViewBack);
                              back=false;

                          }
                          else
                          {
                              filePath = contentURI;

                              Glide.with(verticalHorizontalList.this).load(contentURI).into(getImage);


                          }


                      }
                      else
                      {
                          filePath = contentURI;

                          for(int i=0;i<arrayListVertical.size();i++)
                          {
                              Boolean x=arrayListVertical.get(i).getSelecting();
                              if(x==true)
                              {
                                  arrayListVertical.get(i).setBitmap(bitmap);
                                  String featureName=arrayListVertical.get(i).getTitle();
                                  Glide.with(verticalHorizontalList.this).load(contentURI).into(imageView);
                                  arrayListVertical.get(i).setSelecting(false);
                                  break;
                              }
                          }

                      }





                  } catch (IOException e) {
                      e.printStackTrace();
                      Toast.makeText(this, "Failed!", Toast.LENGTH_SHORT).show();
                  }
              }

          }
          else if (requestCode == CAMERA) {


              if(flag)
              {

                  if (back)
                  {
                      File fBack=new File(currentPhotoPath);
                      filepathBack=Uri.fromFile(fBack);

                      Intent mediaScanIntent=new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                      Uri contentUriBack=Uri.fromFile(fBack);
                      Glide.with(verticalHorizontalList.this).load(contentUriBack).into(imageViewBack);
                      mediaScanIntent.setData(contentUriBack);
                      this.sendBroadcast(mediaScanIntent);

                      back=false;

                  }
                  else
                  {
                      File f=new File(currentPhotoPath);
                      filePath=Uri.fromFile(f);

                      Intent mediaScanIntent=new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                      Uri contentUri=Uri.fromFile(f);
                      Glide.with(verticalHorizontalList.this).load(contentUri).into(getImage);
                      mediaScanIntent.setData(contentUri);
                      this.sendBroadcast(mediaScanIntent);

                      back=false;


                  }




              }
              else
              {
                  for(int i=0;i<arrayListVertical.size();i++)
                  {
                      Boolean x=arrayListVertical.get(i).getSelecting();
                      if(x==true)
                      {
                          File f=new File(arrayListVertical.get(i).getPath());
                         // imageView.setImageURI(Uri.fromFile(f));
                          filePath=Uri.fromFile(f);
                          Glide.with(verticalHorizontalList.this).load(filePath).into(imageView);
                          Intent mediaScanIntent=new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                          Uri contentUri=Uri.fromFile(f);
                          mediaScanIntent.setData(contentUri);
                          this.sendBroadcast(mediaScanIntent);
                          break;
                      }
                  }


              }


          }


      }


      String currentPhotoPath;

      private  File createImageFile() throws IOException
      {
          String timeStamp=new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
          String imageFileName="JPEG_"+timeStamp+"_";
          File storageDir=Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
          File image=File.createTempFile(
                  imageFileName,
                  ".jpg",
                  storageDir
          );

          currentPhotoPath=image.getAbsolutePath();
          return  image;
      }


      private  void dispathTakePictureIntent()
      {
          Intent takePictureIntent= new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
          if(takePictureIntent.resolveActivity(getPackageManager())!= null) {
              File photoFile = null;
              try {
                  {
                      photoFile = createImageFile();
                  }
              } catch (IOException ex) {

              }

              if (photoFile != null) {
                  Uri photoUri = FileProvider.getUriForFile(this, "com.example.android.fileprovider", photoFile);
                  takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                  startActivityForResult(takePictureIntent, CAMERA);
              }
          }
      }


      public void upload(final String s) {
          if (filePath != null) {
              final ProgressDialog progressDialog = new ProgressDialog(this);
              progressDialog.setTitle("Wait...");
              progressDialog.show();

              final StorageReference ref = mstorage.child("features/" + UUID.randomUUID().toString());
              ref.putFile(filePath)
                      .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                          @Override
                          public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                              ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                  @Override
                                  public void onSuccess(Uri uri) {
                                      String duri = uri.toString();


                                      for(int i=0;i<arrayListVertical.size();i++)
                                     {
                                         if(arrayListVertical.get(i).getMoreAduter())
                                        {
                                                DatabaseReference databaseRef=FirebaseDatabase.getInstance().getReference().child(user_ID).child("item").child(click).child(subClick).child(clickItemName).child(arrayListVertical.get(i).getTitle().toLowerCase());

                                                HashMap<String,String> hashMap=new HashMap<>();

                                                hashMap.put("imageUrl",duri);
                                                hashMap.put("name",s);

                                                databaseRef.push().setValue(hashMap);

                                               arrayListVertical.get(i).setMoreAduter(false);

                                                break;

                                        }
                                     }


                                  }
                              });
                              progressDialog.dismiss();

                              Toast.makeText(verticalHorizontalList.this, "Succesful", Toast.LENGTH_SHORT).show();
                          }
                      })
                      .addOnFailureListener(new OnFailureListener() {
                          @Override
                          public void onFailure(@NonNull Exception e) {
                              progressDialog.dismiss();
                              Toast.makeText(verticalHorizontalList.this, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                          }
                      })
                      .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                          @Override
                          public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                    .getTotalByteCount());


                              progressDialog.setMessage("Successful " );
                          }
                      });
          }



      }


      private void showPictureDialog() {
          androidx.appcompat.app.AlertDialog.Builder pictureDialog = new androidx.appcompat.app.AlertDialog.Builder(this);
          pictureDialog.setTitle("Select Action");
          String[] pictureDialogItems = {
                  "Select photo from gallery",
                  "Capture photo from camera"};
          pictureDialog.setItems(pictureDialogItems,
                  new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialog, int which) {
                          switch (which) {
                              case 0:
                                  choosePhotoFromGallary();
                                  break;
                              case 1:
                                  dispathTakePictureIntent();
                                  break;
                          }
                      }
                  });
          pictureDialog.show();
      }


      public void choosePhotoFromGallary() {
          Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                  MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

          startActivityForResult(galleryIntent, GALLERY);


      }



      public void uploadHorizontal(final String s,final String p) {
          if (filePath != null) {
              final ProgressDialog progressDialog = new ProgressDialog(this);
              progressDialog.setTitle("Wait...");
              progressDialog.show();

              final StorageReference ref = mstorage.child("FullDress/" + UUID.randomUUID().toString());
              ref.putFile(filePath)
                      .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                          @Override
                          public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                              ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                  @Override
                                  public void onSuccess(Uri uri) {
                                      savedUri = uri.toString();

                                      if (filepathBack != null)
                                      {
                                          final StorageReference ref = mstorage.child("FullDress/" + UUID.randomUUID().toString());
                                          ref.putFile(filepathBack)
                                                  .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                                      @Override
                                                      public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                                          ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                              @Override
                                                              public void onSuccess(Uri uriBack) {
                                                                  final String savedUriBack=uriBack.toString();


                                                                  final ArrayList<String> checkName=new ArrayList<>();
                                                                  final DatabaseReference check=FirebaseDatabase.getInstance().getReference().child(user_ID).child(click).child(subClick);

                                                                  check.child("names").addListenerForSingleValueEvent(new ValueEventListener() {
                                                                      @Override
                                                                      public void onDataChange(@NonNull DataSnapshot dataSnapshotcheck) {
                                                                          for(DataSnapshot dcheck:dataSnapshotcheck.getChildren())
                                                                          {
                                                                              String checkData=dcheck.getValue().toString();
                                                                              checkName.add(checkData);

                                                                          }

                                                                          if(!(checkName.contains(s)))
                                                                          {
                                                                              HashMap<Object,Object> hashMap=new HashMap<>();
                                                                              hashMap.put("imageUrl",savedUri);
                                                                              hashMap.put("imageBack",savedUriBack);
                                                                              hashMap.put("name",s);
                                                                              hashMap.put("price",p);
                                                                              hashMap.put("featuresArray",stringKeyArray);


                                                                              DatabaseReference pushRef = mdata.child("fullDress").push();
                                                                              final String key_ID = pushRef.getKey();

                                                                              hashMap.put("pushID",key_ID);

                                                                              pushRef.setValue(hashMap);

                                                                              final DatabaseReference allRef=FirebaseDatabase.getInstance().getReference();

                                                                              mdata.child("featureState").addListenerForSingleValueEvent(new ValueEventListener() {
                                                                                  @Override
                                                                                  public void onDataChange(@NonNull DataSnapshot dataSnapshotf) {
                                                                                      String fearureKey=dataSnapshotf.getValue().toString();

                                                                                      if(fearureKey.equals("0"))
                                                                                      {
                                                                                          mdata.addListenerForSingleValueEvent(new ValueEventListener() {
                                                                                              @Override
                                                                                              public void onDataChange(@NonNull DataSnapshot dataSnapshotCK) {
                                                                                                  commanPushID=dataSnapshotCK.child("commenKey").getValue().toString();
                                                                                                  allRef.child("allItems").child(commanPushID).removeValue();

                                                                                                  HashMap<String,String> map=new HashMap();
                                                                                                  map.put("subCategory",subClick.toLowerCase());
                                                                                                  map.put("category",click.toLowerCase());
                                                                                                  map.put("country",country.toLowerCase());
                                                                                                  map.put("city",city.toLowerCase());
                                                                                                  map.put("name",s.toLowerCase());
                                                                                                  map.put("image",savedUri);
                                                                                                  map.put("imageBack",savedUriBack);
                                                                                                  map.put("tailorName",tailorName.toLowerCase());
                                                                                                  map.put("user_ID",user_ID);
                                                                                                  map.put("shopName",shopName.toLowerCase());
                                                                                                  map.put("priceRange",p);
                                                                                                  map.put("pushID",key_ID);
                                                                                                  map.put("commonItemName",clickItemName.toLowerCase());
                                                                                                  map.put("measureState","0");


                                                                                                  allRef.child("allItems").child(key_ID).setValue(map);

                                                                                                  //  allRef.child("measurements").child(commanPushID).removeValue();

                                                                                                  mdata.child("featureState").setValue("1");

                                                                                              }

                                                                                              @Override
                                                                                              public void onCancelled(@NonNull DatabaseError databaseError) {

                                                                                              }
                                                                                          });





                                                                                      }

                                                                                      else
                                                                                      {
                                                                                          HashMap<String,String> map=new HashMap();
                                                                                          map.put("subCategory",subClick.toLowerCase());
                                                                                          map.put("category",click.toLowerCase());
                                                                                          map.put("country",country.toLowerCase());
                                                                                          map.put("city",city.toLowerCase());
                                                                                          map.put("name",s.toLowerCase());
                                                                                          map.put("image",savedUri);
                                                                                          map.put("imageBack",savedUriBack);
                                                                                          map.put("tailorName",tailorName.toLowerCase());
                                                                                          map.put("user_ID",user_ID);
                                                                                          map.put("shopName",shopName.toLowerCase());
                                                                                          map.put("priceRange",p);
                                                                                          map.put("pushID",key_ID);
                                                                                          map.put("commonItemName",clickItemName.toLowerCase());
                                                                                          map.put("measureState","0");


                                                                                          allRef.child("allItems").child(key_ID).setValue(map);
                                                                                      }
                                                                                  }

                                                                                  @Override
                                                                                  public void onCancelled(@NonNull DatabaseError databaseError) {

                                                                                  }
                                                                              });



                                                                              DatabaseReference measure=FirebaseDatabase.getInstance().getReference().child(user_ID);
                                                                              measure.child("measurements").child(key_ID).child("state").setValue("0");



                                                                              stringKeyArray.clear();



                                                                              Intent intent = new Intent(verticalHorizontalList.this ,verticalHorizontalList.class);
                                                                              intent.putExtra("clickCategory",click);
                                                                              intent.putExtra("subClickCategory",subClick);
                                                                              intent.putExtra("clickItemName",clickItemName);
                                                                              startActivity(intent);


                                                                          }

                                                                          else
                                                                          {
                                                                              Toast.makeText(verticalHorizontalList.this, "This name is used.Use another name!", Toast.LENGTH_SHORT).show();
                                                                          }
                                                                      }

                                                                      @Override
                                                                      public void onCancelled(@NonNull DatabaseError databaseError) {

                                                                      }
                                                                  });



                                                              }
                                                          });
                                                      }
                                                  });
                                      }

                                      else
                                      {
                                          final ArrayList<String> checkName=new ArrayList<>();
                                          final DatabaseReference check=FirebaseDatabase.getInstance().getReference().child(user_ID).child(click).child(subClick);

                                          check.child("names").addListenerForSingleValueEvent(new ValueEventListener() {
                                              @Override
                                              public void onDataChange(@NonNull DataSnapshot dataSnapshotcheck) {
                                                  for(DataSnapshot dcheck:dataSnapshotcheck.getChildren())
                                                  {
                                                      String checkData=dcheck.getValue().toString();
                                                      checkName.add(checkData);

                                                  }

                                                  if(!(checkName.contains(s)))
                                                  {
                                                      HashMap<Object,Object> hashMap=new HashMap<>();
                                                      hashMap.put("imageUrl",savedUri);
                                                      hashMap.put("name",s);
                                                      hashMap.put("price",p);
                                                      hashMap.put("featuresArray",stringKeyArray);


                                                      DatabaseReference pushRef = mdata.child("fullDress").push();
                                                      final String key_ID = pushRef.getKey();

                                                      hashMap.put("pushID",key_ID);

                                                      pushRef.setValue(hashMap);

                                                      final DatabaseReference allRef=FirebaseDatabase.getInstance().getReference();

                                                      mdata.child("featureState").addListenerForSingleValueEvent(new ValueEventListener() {
                                                          @Override
                                                          public void onDataChange(@NonNull DataSnapshot dataSnapshotf) {
                                                              String fearureKey=dataSnapshotf.getValue().toString();

                                                              if(fearureKey.equals("0"))
                                                              {
                                                                  mdata.addListenerForSingleValueEvent(new ValueEventListener() {
                                                                      @Override
                                                                      public void onDataChange(@NonNull DataSnapshot dataSnapshotCK) {
                                                                          commanPushID=dataSnapshotCK.child("commenKey").getValue().toString();
                                                                          allRef.child("allItems").child(commanPushID).removeValue();

                                                                          HashMap<String,String> map=new HashMap();
                                                                          map.put("subCategory",subClick.toLowerCase());
                                                                          map.put("category",click.toLowerCase());
                                                                          map.put("country",country.toLowerCase());
                                                                          map.put("city",city.toLowerCase());
                                                                          map.put("name",s.toLowerCase());
                                                                          map.put("image",savedUri);
                                                                          map.put("tailorName",tailorName.toLowerCase());
                                                                          map.put("user_ID",user_ID);
                                                                          map.put("shopName",shopName.toLowerCase());
                                                                          map.put("priceRange",p);
                                                                          map.put("pushID",key_ID);
                                                                          map.put("commonItemName",clickItemName.toLowerCase());
                                                                          map.put("measureState","0");


                                                                          allRef.child("allItems").child(key_ID).setValue(map);

                                                                          //  allRef.child("measurements").child(commanPushID).removeValue();

                                                                          mdata.child("featureState").setValue("1");

                                                                      }

                                                                      @Override
                                                                      public void onCancelled(@NonNull DatabaseError databaseError) {

                                                                      }
                                                                  });





                                                              }

                                                              else
                                                              {
                                                                  HashMap<String,String> map=new HashMap();
                                                                  map.put("subCategory",subClick.toLowerCase());
                                                                  map.put("category",click.toLowerCase());
                                                                  map.put("country",country.toLowerCase());
                                                                  map.put("city",city.toLowerCase());
                                                                  map.put("name",s.toLowerCase());
                                                                  map.put("image",savedUri);
                                                                  map.put("tailorName",tailorName.toLowerCase());
                                                                  map.put("user_ID",user_ID);
                                                                  map.put("shopName",shopName.toLowerCase());
                                                                  map.put("priceRange",p);
                                                                  map.put("pushID",key_ID);
                                                                  map.put("commonItemName",clickItemName.toLowerCase());
                                                                  map.put("measureState","0");


                                                                  allRef.child("allItems").child(key_ID).setValue(map);
                                                              }
                                                          }

                                                          @Override
                                                          public void onCancelled(@NonNull DatabaseError databaseError) {

                                                          }
                                                      });



                                                      DatabaseReference measure=FirebaseDatabase.getInstance().getReference().child(user_ID);
                                                      measure.child("measurements").child(key_ID).child("state").setValue("0");



                                                      stringKeyArray.clear();



                                                      Intent intent = new Intent(verticalHorizontalList.this ,verticalHorizontalList.class);
                                                      intent.putExtra("clickCategory",click);
                                                      intent.putExtra("subClickCategory",subClick);
                                                      intent.putExtra("clickItemName",clickItemName);
                                                      startActivity(intent);
                                                      finish();


                                                  }

                                                  else
                                                  {
                                                      Toast.makeText(verticalHorizontalList.this, "This name is used.Use another name!", Toast.LENGTH_SHORT).show();
                                                  }
                                              }

                                              @Override
                                              public void onCancelled(@NonNull DatabaseError databaseError) {

                                              }
                                          });
                                      }



                                  }
                              });
                              progressDialog.dismiss();

                              Toast.makeText(verticalHorizontalList.this, "Succesful", Toast.LENGTH_SHORT).show();
                          }
                      })
                      .addOnFailureListener(new OnFailureListener() {
                          @Override
                          public void onFailure(@NonNull Exception e) {
                              progressDialog.dismiss();
                              Toast.makeText(verticalHorizontalList.this, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                          }
                      })
                      .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                          @Override
                          public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                              double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                      .getTotalByteCount());


                              progressDialog.setMessage("Successful " );
                          }
                      });
          }


      }

      ArrayList<String> stringKeyArray=new ArrayList<>();

      public void functionToRun()
      {
          for(int i=0;i<arrayListVertical.size();i++)
          {
              if(arrayListVertical.get(i).getKeySel())
              {
                  if(keyArr.contains(i))
                  {
                      for(int j=0;j<keyArr.size();j++)
                      {
                          if(i==keyArr.get(j))
                          {

                              stringKeyArray.set(j,arrayListVertical.get(i).getKey());
                              arrayListVertical.get(i).setKeySel(false);
                              break;

                          }
                      }

                  }
                  else
                  {

                      stringKeyArray.add(arrayListVertical.get(i).getKey());
                      keyArr.add(i);
                      arrayListVertical.get(i).setKeySel(false);
                      break;


                  }


              }
          }






      }

      public void featureMoreToRun() {

          alertAdapterMore();
      }

      private void alertAdapterMore() {

          View view1= LayoutInflater.from(this).inflate(R.layout.itemdialogbox2,null);
          AlertDialog.Builder alertdialog=new AlertDialog.Builder(this);
          alertdialog.setView(view1);


          final EditText getData=view1.findViewById(R.id.itemName);
          final Button btn=view1.findViewById(R.id.itemAdd);
          imageView=view1.findViewById(R.id.itemSelect);



          btn.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {

                 // showPictureDialog(vdupterModel);
                  showPictureDialog();

              }
          });



          alertdialog.setCancelable(false)
                  .setPositiveButton("ADD", new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialog, int which) {

                          String s=getData.getText().toString();
                          upload(s);


                      }

                  })
                  .setNegativeButton("CANCLE", new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialog, int which) {
                          dialog.cancel();
                      }
                  });





          alertdialog.create();
          alertdialog.show();


      }


      public void functionToRemove() {

          for(int i=0;i<arrayListVertical.size();i++)
          {
              if(arrayListVertical.get(i).getKeySel())
              {
                  final String check=arrayListVertical.get(i).getTitle();
                  final int o=i;
                  mdata.child(check).addListenerForSingleValueEvent(new ValueEventListener() {
                      @Override
                      public void onDataChange(@NonNull DataSnapshot dataSnapshotR) {
                          if (dataSnapshotR.exists())
                          {

                              for(DataSnapshot ds:dataSnapshotR.getChildren())
                              {
                                  String nm=ds.child("name").getValue().toString().toLowerCase();

                                  if(nm.equals(arrayListVertical.get(o).getKey().toLowerCase()))
                                  {
                                      String ky=ds.getKey();
                                      mdata.child(check).child(ky).removeValue();
                                  }
                              }
                          }
                      }

                      @Override
                      public void onCancelled(@NonNull DatabaseError databaseError) {

                      }
                  });
              }
          }

      }
  }

