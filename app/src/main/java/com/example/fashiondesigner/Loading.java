package com.example.fashiondesigner;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Loading extends AppCompatActivity {

    public FirebaseAuth mFirebaseAuth;
    DatabaseReference mdata2;
    String user_id;
    FirebaseDatabase database;
    FirebaseUser mFirebaseUser;

    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        mFirebaseAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        mdata2 = database.getReference();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();

        handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mFirebaseUser != null) {
                    Toast.makeText(Loading.this, "You are logged in!", Toast.LENGTH_SHORT).show();


                    mdata2.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            user_id = mFirebaseAuth.getCurrentUser().getUid();

                            String fg = dataSnapshot.child(user_id).child("flag").getValue(String.class);
                            String tg = dataSnapshot.child(user_id).child("tag").getValue(String.class);

                            if (fg.equals("1")) {
                                if (tg.equals("0")) {
                                    Intent i = new Intent(Loading.this, spdetail.class);
                                    startActivity(i);
                                    finish();
                                } else if (tg.equals("1")) {
                                    Intent i = new Intent(Loading.this, Tailor2.class);
                                    startActivity(i);
                                    finish();
                                } else {
                                    Toast.makeText(Loading.this, "Logging Error!", Toast.LENGTH_SHORT).show();
                                }

                            } else if (fg.equals("0")) {
                                if (tg.equals("0")) {
                                    Intent i = new Intent(Loading.this, customerProfile.class);
                                    startActivity(i);
                                    finish();
                                } else if (tg.equals("1")) {
                                    Intent i = new Intent(Loading.this, Customer.class);
                                    startActivity(i);
                                    finish();
                                } else {
                                    Toast.makeText(Loading.this, "Logging Error!", Toast.LENGTH_SHORT).show();
                                }

                            } else {
                                Toast.makeText(Loading.this, "Error in logging!", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError error) {

                        }


                    });
                }
                else {
                    Toast.makeText(Loading.this, "Please logging!", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(Loading.this, MainActivity.class);
                    startActivity(i);
                    finish();

                }
            }
        },2000);


    }
}
