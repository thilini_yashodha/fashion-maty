package com.example.fashiondesigner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.fashiondesigner.measurements.conformMeasurement;
import com.example.fashiondesigner.measurements.tMeasurement;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

public class viewItemClick extends AppCompatActivity {

    String click,clickItemName,user_ID,subClick,position,price,imageUrl,pushID;
    TextView priceText;
    DatabaseReference mdata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_item_click);


        Intent intent = getIntent();
        click = intent.getStringExtra("clickCategory").toLowerCase();
        subClick = intent.getStringExtra("subClickCategory").toLowerCase();
        clickItemName=intent.getStringExtra("clickItemName").toLowerCase();
        user_ID=intent.getStringExtra("user_ID");
        price=intent.getStringExtra("price");
        imageUrl=intent.getStringExtra("imageUrl");
        position=intent.getStringExtra("position");
        pushID=intent.getStringExtra("pushID");


        mdata= FirebaseDatabase.getInstance().getReference().child(user_ID).child("item").child(click).child(subClick).child("prices");

        TextView title=findViewById(R.id.title);
        TextView upper=findViewById(R.id.clickItem);
        ImageView image=findViewById(R.id.image);
        priceText=findViewById(R.id.price);
        Button editPrice=findViewById(R.id.editPrice);
        Button features=findViewById(R.id.features);
        Button colours=findViewById(R.id.colours);
        Button measurements=findViewById(R.id.measurements);


        upper.setText(subClick.toUpperCase());
        title.setText(clickItemName.toUpperCase());
        Picasso.get().load(imageUrl).into(image);
        priceText.setText("$ " + price);


        measurements.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatabaseReference mdataM=FirebaseDatabase.getInstance().getReference().child(user_ID).child("measurements").child(pushID).child("state");
                mdataM.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String state=dataSnapshot.getValue().toString();

                        if(state.equals("0"))
                        {
                            Intent intent = new Intent(viewItemClick.this , tMeasurement.class);
                            intent.putExtra("clickCategory",click);
                            intent.putExtra("subClickCategory",subClick);
                            intent.putExtra("clickItemName",clickItemName);
                            intent.putExtra("pushID",pushID);
                            intent.putExtra("userID",user_ID);
                            startActivity(intent);
                        }
                        else if(state.equals("1"))
                        {
                            Intent intent = new Intent(viewItemClick.this , conformMeasurement.class);
                            intent.putExtra("clickCategory",click);
                            intent.putExtra("subClickCategory",subClick);
                            intent.putExtra("clickItemName",clickItemName);
                            intent.putExtra("pushID",pushID);
                            intent.putExtra("userID",user_ID);
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


            }
        });

        editPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    alert();
            }
        });

        features.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(viewItemClick.this ,verticalHorizontalList.class);
                intent.putExtra("clickCategory",click);
                intent.putExtra("subClickCategory",subClick);
                intent.putExtra("clickItemName",clickItemName);
               // intent.putExtra("commanPushID",pushID);
                startActivity(intent);
            }
        });

        colours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewItemClick.this ,fabric.class);
                intent.putExtra("clickCategory",click);
                intent.putExtra("subClickCategory",subClick);
                intent.putExtra("clickItemName",clickItemName);
                intent.putExtra("pushID",pushID);
                startActivity(intent);
            }
        });
    }

    private void alert() {

        View view=(LayoutInflater.from(viewItemClick.this)).inflate(R.layout.tailordalogbox,null);
        AlertDialog.Builder alertdialog=new AlertDialog.Builder(viewItemClick.this);
        alertdialog.setView(view);


        final EditText getData=view.findViewById(R.id.reenterbn);

        alertdialog.setCancelable(false)
                .setPositiveButton("EDIT", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String s=getData.getText().toString();
                        priceText.setText("$ "+ s);
                       mdata.child(position).setValue(s);


                    }
                })
                .setNegativeButton("CANCLE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });



        alertdialog.create();
        alertdialog.show();
    }
}
