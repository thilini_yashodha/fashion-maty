package com.example.fashiondesigner.suggest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.example.fashiondesigner.Adapter.Model.VerticalModel;
import com.example.fashiondesigner.Adapter.Model.searchHorizontalModel;
import com.example.fashiondesigner.Adapter.searchColouAdupter;
import com.example.fashiondesigner.Adapter.searchFabricAdupter;
import com.example.fashiondesigner.R;
import com.example.fashiondesigner.customerSearch.searchColour;
import com.example.fashiondesigner.customerSearch.viewSeaechColour;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class selectFabric extends AppCompatActivity {

    String tailorID,suggestID;

    DatabaseReference mdata;

    ArrayList<searchHorizontalModel> arrayListHorizontal;
    RecyclerView fabricReView;
    searchFabricAdupter adapter;


    RecyclerView verticalRecyclerView;
    searchColouAdupter adapterColour;
    ArrayList<VerticalModel> arrayListVertical;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_fabric);

        Intent intent = getIntent();
        tailorID = intent.getStringExtra("tailorID");
        suggestID=intent.getStringExtra("suggestID");

         mdata= FirebaseDatabase.getInstance().getReference().child(tailorID).child("fabric");

        arrayListHorizontal = new ArrayList<>();

        fabricReView=findViewById(R.id.fabricRe);
        fabricReView.setHasFixedSize(true);

        fabricReView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));

        adapter=new searchFabricAdupter(this,arrayListHorizontal);
        fabricReView.setAdapter(adapter);



        arrayListVertical=new ArrayList<>();

        verticalRecyclerView=findViewById(R.id.colourRe);
        verticalRecyclerView.setHasFixedSize(true);

        verticalRecyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));


        adapterColour=new searchColouAdupter(this,arrayListVertical);
        verticalRecyclerView.setAdapter(adapterColour);


        final ArrayList<String> availableFabric=new ArrayList<>();

        mdata.child("fabricDetails").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                arrayListHorizontal.clear();
                int i=0;
                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren())
                {
                    searchHorizontalModel data=dataSnapshot1.getValue(searchHorizontalModel.class);

                    arrayListHorizontal.add(data);
                    arrayListHorizontal.get(i).setSelectFabric(true);
                    adapter.notifyDataSetChanged();

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }

    String select,fabricType;
    public void functionToRun(String fabricPushID) {

        for(int i=0;i<arrayListHorizontal.size();i++)
        {
            if(arrayListHorizontal.get(i).getSelectHorizontal())
            {

                select=arrayListHorizontal.get(i).getPushID();
                fabricType=arrayListHorizontal.get(i).getName();

                VerticalModel mVerticalModel=new VerticalModel();

                mVerticalModel.setID(tailorID);
                mVerticalModel.setFabricPushID(fabricPushID);
                mVerticalModel.setTitle(fabricType.toUpperCase());
                mVerticalModel.setFselect(true);

                arrayListVertical.clear();

                arrayListVertical.add(mVerticalModel);

                adapterColour.notifyDataSetChanged();

                arrayListHorizontal.get(i).setSelectHorizontal(false);
                break;
            }
        }



    }

    public void SelectToRun() {

        String colourKey,fabricPushkey,image;

        for(int i=0;i<arrayListVertical.size();i++)
        {
            if(arrayListVertical.get(i).getKeySel())
            {
                colourKey=arrayListVertical.get(i).getKey();
                fabricPushkey=arrayListVertical.get(i).getFabricPushID();

                Intent intent = new Intent(selectFabric.this, customerMeasurement.class);
                intent.putExtra("tailorID",tailorID);
                intent.putExtra("fabricKey",fabricPushkey);
                intent.putExtra("colourKey",colourKey);
                intent.putExtra("suggestID",suggestID);

                startActivity(intent);

                arrayListVertical.get(i).setKeySel(false);
                break;

            }





        }
    }
}
