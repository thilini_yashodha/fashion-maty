package com.example.fashiondesigner.suggest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.fashiondesigner.Customer;
import com.example.fashiondesigner.R;
import com.example.fashiondesigner.chat.meassageActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class suggestViewCilickCust extends AppCompatActivity {

    String tailorID,suggestID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggest_view_cilick_cust);

        Intent intent = getIntent();
        tailorID = intent.getStringExtra("tailorID");
        suggestID=intent.getStringExtra("suggestID");

        final ImageView front,back;

        front=findViewById(R.id.front);
        back=findViewById(R.id.back);

        final TextView textFront,textBack,textData,informText;

        textFront=findViewById(R.id.frontText);
        textBack=findViewById(R.id.backText);
        textData=findViewById(R.id.textData);
        informText=findViewById(R.id.informText);

        final Button conform;
        conform=findViewById(R.id.conform);

        FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
        final String userID=user.getUid();
        final DatabaseReference mdata= FirebaseDatabase.getInstance().getReference().child(userID).child("suggestOrder").child(suggestID);


        mdata.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists())
                {
                    if(dataSnapshot.child("imageUrlFront").exists())
                    {

                        String fromtImage=dataSnapshot.child("imageUrlFront").getValue().toString();
                        Glide.with(front.getContext())
                                .load(fromtImage)
                                .into(front);

                        textFront.setText("FRONT VIEW");
                    }

                    if(dataSnapshot.child("imageUrlBack").exists())
                    {
                        String backImage=dataSnapshot.child("imageUrlBack").getValue().toString();

                        Glide.with(back.getContext())
                                .load(backImage)
                                .into(back);

                        textBack.setText("BACK VIEW");
                    }
                    else
                    {
                        back.setVisibility(View.GONE);
                        textBack.setVisibility(View.GONE);
                    }

                    if(dataSnapshot.child("text").exists())
                    {
                        String text=dataSnapshot.child("text").getValue().toString();

                        textData.setText(text);
                    }




                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        final DatabaseReference mdataTailor= FirebaseDatabase.getInstance().getReference().child(tailorID).child("suggestOrder").child(suggestID);
        mdataTailor.child("acceptOrder").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshotAO) {

                if(dataSnapshotAO.exists())
                {
                    informText.setText("Tailor Conform the order.You can continue now!");
                    conform.setText("CONTINUE");

                    conform.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Intent intent = new Intent(suggestViewCilickCust.this, selectFabric.class);
                            intent.putExtra("tailorID",tailorID);
                            intent.putExtra("suggestID",suggestID);
                            startActivity(intent);
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        mdataTailor.child("cancel").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshotCA) {

                if(dataSnapshotCA.exists())
                {
                    informText.setText("Tailor CANCEL the order.Click OK to end the process.");
                    conform.setText("OK");

                    conform.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            mdata.removeValue();
                            mdataTailor.removeValue();

                            Intent intent = new Intent(suggestViewCilickCust.this, Customer.class);
                            startActivity(intent);

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }
}
