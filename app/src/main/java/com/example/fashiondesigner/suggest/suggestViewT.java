package com.example.fashiondesigner.suggest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.fashiondesigner.Adapter.Model.VerticalModel;
import com.example.fashiondesigner.Adapter.VerticalRecyclerViewAdapter;
import com.example.fashiondesigner.R;
import com.example.fashiondesigner.chat.chatUsersAdapter;
import com.example.fashiondesigner.chat.chatUsersModel;
import com.example.fashiondesigner.chat.messageAdupter;
import com.example.fashiondesigner.chat.userModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class suggestViewT extends AppCompatActivity {

    RecyclerView recyclerView;
    chatUsersAdapter adapter;
    ArrayList<chatUsersModel> arrayListSuggest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggest_view_t);


        FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
        final String userID=user.getUid();
        DatabaseReference mdata=FirebaseDatabase.getInstance().getReference().child(userID).child("suggestOrder");

        recyclerView=findViewById(R.id.recyclerView);

        arrayListSuggest=new ArrayList<>();
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));


        adapter=new chatUsersAdapter(this,arrayListSuggest);
        recyclerView.setAdapter(adapter);


        mdata.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                if(dataSnapshot.exists())
                {
                    for(DataSnapshot ds:dataSnapshot.getChildren())
                    {

                        final String custpmerID=ds.child("customerID").getValue().toString();
                        if(ds.child("suggestID").exists())
                        {
                            final String suggestID=ds.child("suggestID").getValue().toString();

                            DatabaseReference cdata=FirebaseDatabase.getInstance().getReference().child(custpmerID).child("customerData");
                            cdata.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshotC) {

                                    String imageUrl=dataSnapshotC.child("imageUrl").getValue().toString();
                                    String name=dataSnapshotC.child("name").getValue().toString();

                                    chatUsersModel mModel=new chatUsersModel();
                                    mModel.setImaageUrl(imageUrl);
                                    mModel.setName(name.toUpperCase() + "\n\n Order ID : " + suggestID );
                                    mModel.setSuggest(true);
                                    mModel.setSuggestID(suggestID);
                                    mModel.setUserID(custpmerID);

                                    arrayListSuggest.add(mModel);
                                    adapter.notifyDataSetChanged();
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                        }
                        else if(ds.child("pushID").exists())
                        {
                            final String suggestID=ds.child("pushID").getValue().toString();

                            DatabaseReference cdata=FirebaseDatabase.getInstance().getReference().child(custpmerID).child("customerData");
                            cdata.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshotC) {

                                    String imageUrl=dataSnapshotC.child("imageUrl").getValue().toString();
                                    String name=dataSnapshotC.child("name").getValue().toString();

                                    chatUsersModel mModel=new chatUsersModel();
                                    mModel.setImaageUrl(imageUrl);
                                    mModel.setName(name.toUpperCase() + "\n\n Order ID : " + suggestID );
                                    mModel.setSuggest(true);
                                    mModel.setSuggestID(suggestID);
                                    mModel.setUserID(custpmerID);

                                    arrayListSuggest.add(mModel);
                                    adapter.notifyDataSetChanged();
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                        }






                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
