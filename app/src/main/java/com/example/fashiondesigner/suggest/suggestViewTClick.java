package com.example.fashiondesigner.suggest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.fashiondesigner.R;
import com.example.fashiondesigner.Tailor2;
import com.example.fashiondesigner.chat.meassageActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class suggestViewTClick extends AppCompatActivity {

    String cutomerID,suggestID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggest_view_tclick);


        Intent intent = getIntent();
        cutomerID = intent.getStringExtra("customerID");
        suggestID=intent.getStringExtra("suggestID");

        final ImageView front,back;

        front=findViewById(R.id.front);
        back=findViewById(R.id.back);

        final TextView textFront,textBack,textData;

        textFront=findViewById(R.id.frontText);
        textBack=findViewById(R.id.backText);
        textData=findViewById(R.id.textData);

        final Button conform,chatTailor,cancel;

        conform=findViewById(R.id.conform);
        chatTailor=findViewById(R.id.chatTailor);
        cancel=findViewById(R.id.cancel);

        FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
        final String userID=user.getUid();
        final DatabaseReference mdata= FirebaseDatabase.getInstance().getReference().child(userID).child("suggestOrder").child(suggestID);

        mdata.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists())
                {
                    if(dataSnapshot.child("imageUrlFront").exists())
                    {

                        String fromtImage=dataSnapshot.child("imageUrlFront").getValue().toString();
                        Glide.with(front.getContext())
                                .load(fromtImage)
                                .into(front);

                        textFront.setText("FRONT VIEW");
                    }

                    if(dataSnapshot.child("imageUrlBack").exists())
                    {
                        String backImage=dataSnapshot.child("imageUrlBack").getValue().toString();

                        Glide.with(back.getContext())
                                .load(backImage)
                                .into(back);

                        textBack.setText("BACK VIEW");
                    }
                    else
                    {
                        back.setVisibility(View.GONE);
                        textBack.setVisibility(View.GONE);
                    }

                    if(dataSnapshot.child("text").exists())
                    {
                        String text=dataSnapshot.child("text").getValue().toString();

                        textData.setText(text);
                    }


                    conform.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if(dataSnapshot.child("acceptOrder").exists())
                            {
                                Intent intent = new Intent(suggestViewTClick.this, viewSuggestMeasurement.class);
                                intent.putExtra("customerID",cutomerID);
                                intent.putExtra("suggestID",suggestID);
                                startActivity(intent);

                            }
                            else
                            {
                                Intent intent = new Intent(suggestViewTClick.this, selectGender.class);
                                intent.putExtra("customerID",cutomerID);
                                intent.putExtra("suggestID",suggestID);
                                startActivity(intent);
                            }


                        }
                    });

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        chatTailor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(suggestViewTClick.this, meassageActivity.class);
                intent.putExtra("userID",cutomerID);
                startActivity(intent);

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mdata.child("cancel").setValue("1");
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(suggestViewTClick.this, Tailor2.class);
        startActivity(intent);
    }
}
