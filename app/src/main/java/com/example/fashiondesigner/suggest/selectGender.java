package com.example.fashiondesigner.suggest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.fashiondesigner.R;
import com.example.fashiondesigner.measurements.gentMeasuremet;
import com.example.fashiondesigner.measurements.ladiesMeasurement;
import com.example.fashiondesigner.measurements.tMeasurement;

public class selectGender extends AppCompatActivity {

    RadioGroup radioGroup;
    RadioButton radioButton;
    String cutomerID,suggestID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_gender);

        Intent intent = getIntent();
        cutomerID = intent.getStringExtra("customerID");
        suggestID=intent.getStringExtra("suggestID");

        Button conti=findViewById(R.id.conti);

        radioGroup=findViewById(R.id.radioGroup);

        conti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int radioID=radioGroup.getCheckedRadioButtonId();
                radioButton=findViewById(radioID);

                if(radioButton.getText().equals("GENTLEMAN"))
                {
                    Intent intent = new Intent(selectGender.this , suggestMeasurements.class);
                    intent.putExtra("customerID",cutomerID);
                    intent.putExtra("suggestID",suggestID);
                    intent.putExtra("gender","1");
                    startActivity(intent);
                }
                else if(radioButton.getText().equals("LADIES"))
                {
                    Intent intent = new Intent(selectGender.this ,suggestMeasurements.class);
                   intent.putExtra("customerID",cutomerID);
                   intent.putExtra("suggestID",suggestID);
                   intent.putExtra("gender","0");
                    startActivity(intent);
                }

            }
        });

    }

    public void  checkButton(View V)
    {
        int radioID=radioGroup.getCheckedRadioButtonId();
        radioButton=findViewById(radioID);

        Toast.makeText(this,radioButton.getText()+ " SELLECTED",Toast.LENGTH_SHORT).show();
    }
}
