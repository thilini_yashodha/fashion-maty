package com.example.fashiondesigner.suggest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.fashiondesigner.R;
import com.example.fashiondesigner.measurements.conformMeasurement;
import com.example.fashiondesigner.measurements.gentMeasuremet;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class suggestMeasurements extends AppCompatActivity {

    String cutomerID,suggestID,gender;
    String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggest_measurements);


        Intent intent = getIntent();
        cutomerID = intent.getStringExtra("customerID");
        suggestID=intent.getStringExtra("suggestID");
        gender=intent.getStringExtra("gender");

        final EditText heighPortion=findViewById(R.id.heightPortion);
        final EditText widthPortion=findViewById(R.id.widthPortion);
        final EditText text=findViewById(R.id.text);
        final  EditText price=findViewById(R.id.price);

        FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
        userID=user.getUid();

        final DatabaseReference mdata= FirebaseDatabase.getInstance().getReference().child(userID).child("suggestOrder").child(suggestID);

        PhotoView photoView = findViewById(R.id.photo);

        heighPortion.setHint("give persentage of dress length to human height(100%)");


        widthPortion.setHint("give persentage of dress width to human width(100%)");



        if(gender.equals("1"))
        {
            photoView.setImageResource(R.drawable.men);
        }
        else if(gender.equals("0"))
        {
            photoView.setImageResource(R.drawable.women);
        }

        LinearLayout checkBoxContainer = findViewById(R.id.checkList);
        CheckBox checkBox;

        final ArrayList<String> list=new ArrayList<>();

        if(gender.equals("1"))
        {
            list.add("1  - Neck circumference");
            list.add("2  - Chest upper");
            list.add("3  - Chest lower");
            list.add("4  - Waist circumference");
            list.add("5  - Hip circumference");
            list.add("6  - Tight circumference at widest palce");
            list.add("7  - Knee circumference just below kneecap");
            list.add("8  - Calf at widest");
            list.add("9  - Arms eye");
            list.add("10 - Blcep circumference");
            list.add("11 - Wrist circumference");
            list.add("12 - Shoulder to shoulder");
            list.add("13 - Neck to waist");
            list.add("14 - Crotch(rise)");
            list.add("15 - Inner thigh");
            list.add("16 - Knee to ankle");
            list.add("17 - Knee to shoulder");
            list.add("18 - From shoulder to end of dres length");
            list.add("19 - Elbow to wrist");
            list.add("20 - Outer thigh");
            list.add("21 - Waist to knee for coats");

        }
        else if(gender.equals("0"))
        {
            list.add("A  - Neck round");
            list.add("B  - Above bust round");
            list.add("C  - Bust round");
            list.add("D  - Below bust round");
            list.add("E  - Waist round");
            list.add("F  - Hips round");
            list.add("G  - Bicep round");
            list.add("I  - Elbow round");
            list.add("J  - Wrist round");
            list.add("K  - Thigh round");
            list.add("L  - Knee round");
            list.add("M  - Ankle round");
            list.add("N  - Neck to apex");
            list.add("O  - Shoulder");
            list.add("P  - Cross Front");
            list.add("Q  - Creoss Back");
            list.add("R  - From weist to end of dress length");
            list.add("S  - From shoulder to end of dres length");
            list.add("T  - Blouse lengh(up to waist or end of dress)");
            list.add("U  - Neck depth(Back)");
            list.add("V  - Neck depth(Back)");
            list.add("W  - Full hand length");
            list.add("X  - Half length hand");

        }

        final  ArrayList<CheckBox> arryOfCheckBox=new ArrayList<>();

        for(int i=0;i<list.size();i++)
        {
            checkBox=new CheckBox(this);
            checkBox.setId(i);
            checkBox.setText(list.get(i));
            checkBox.setTag(list.get(i));

            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked)
                    {
                        Toast.makeText(suggestMeasurements.this,"selected",Toast.LENGTH_SHORT).show();
                    }
                }
            });

            arryOfCheckBox.add(checkBox);
            checkBoxContainer.addView(checkBox);

        }

        Button conform = findViewById(R.id.conform);
        final ArrayList<String> selected=new ArrayList<>();


        conform.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i=0;i<arryOfCheckBox.size();i++)
                {
                    if(arryOfCheckBox.get(i).isChecked())
                    {

                        selected.add(list.get(i));


                    }
                }


                mdata.child("measurementData").setValue(selected);

                String h=heighPortion.getText().toString();
                String w=widthPortion.getText().toString();

                String gettext=text.getText().toString();
                String pc=price.getText().toString();

                mdata.child("heightPortion").setValue(h);
                mdata.child("widthPortion").setValue(w);
                mdata.child("additionalMeasurement").setValue(gettext);
                mdata.child("gender").setValue(gender);
                mdata.child("price").setValue(pc);
                mdata.child("acceptOrder").setValue("1");


                Toast.makeText(suggestMeasurements.this,"Successful!",Toast.LENGTH_SHORT).show();





            }
        });







    }
}
