package com.example.fashiondesigner.suggest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.fashiondesigner.R;
import com.example.fashiondesigner.measurements.deliverAddress;
import com.example.fashiondesigner.measurements.placeOrder;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class suggestAddress extends AppCompatActivity {

    String tailorID,suggestID,fabricKey,colourKey,orderPushID,customerUserID;

    EditText edName,edTele,edStreet,edHomeNo,edState,edCity,edZip;
    Spinner country;
    Button conform;

    DatabaseReference mdata;

    LatLng cPoint,tPoint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggest_address);

        Intent intent = getIntent();
        tailorID = intent.getStringExtra("tailorID");
        suggestID=intent.getStringExtra("suggestID");
        colourKey=intent.getStringExtra("colourKey");
        fabricKey=intent.getStringExtra("fabricKey");
        orderPushID=intent.getStringExtra("orderPushID");

        FirebaseUser currentUser= FirebaseAuth.getInstance().getCurrentUser();
        customerUserID=currentUser.getUid();

        mdata= FirebaseDatabase.getInstance().getReference().child(customerUserID).child("order");

        edName=findViewById(R.id.name);
        edTele=findViewById(R.id.tele);
        edStreet=findViewById(R.id.street);
        edHomeNo=findViewById(R.id.homeNo);
        edState=findViewById(R.id.state);
        edCity=findViewById(R.id.city);
        edZip=findViewById(R.id.zip);

        country=findViewById(R.id.spiner);
        conform=findViewById(R.id.conform);

        Locale[] locales = Locale.getAvailableLocales();
        ArrayList<String> countries = new ArrayList<String>();
        for (Locale locale : locales) {
            String country = locale.getDisplayCountry();
            if (country.trim().length() > 0 && !countries.contains(country)) {
                countries.add(country);
            }
        }
        Collections.sort(countries);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, countries);
        // set the view for the Drop down list
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // set the ArrayAdapter to the spinner
        country.setAdapter(dataAdapter);
        country.setSelection(1);

        final DatabaseReference tailordata= FirebaseDatabase.getInstance().getReference().child(tailorID).child("tailorData");
        tailordata.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                String addresst=dataSnapshot.child("address").getValue().toString();
                String countryt=dataSnapshot.child("country").getValue().toString();
                String cityt=dataSnapshot.child("city").getValue().toString();

                String tadd=addresst+","+countryt+","+cityt;

                tPoint=getLocationFromAddress(suggestAddress.this,tadd);

            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        conform.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                HashMap<String,Object> map=new HashMap<>();

                String result=country.getSelectedItem().toString();

                String name=edName.getText().toString();
                String tele=edTele.getText().toString();
                String street=edStreet.getText().toString();
                String home=edHomeNo.getText().toString();
                String state=edState.getText().toString();
                String city=edCity.getText().toString();
                String zip=edZip.getText().toString();

                String add=street+","+home+","+state+","+city+","+city+","+zip;
                cPoint=getLocationFromAddress(suggestAddress.this,add);

                if(tPoint!=null && cPoint!=null)
                {
                    Location startPoint=new Location("locationA");
                    startPoint.setLatitude(tPoint.latitude);
                    startPoint.setLongitude(tPoint.longitude);

                    Location endPoint=new Location("locationB");
                    endPoint.setLatitude(cPoint.latitude);
                    endPoint.setLongitude(cPoint.longitude);

                    double distance=startPoint.distanceTo(endPoint);
                    double distanceKM=distance/1000;
                    map.put("distance",distanceKM);
                }
                else
                {
                    map.put("distance","0.0");
                }


                map.put("country",result);
                map.put("name",name);
                map.put("tele",tele);
                map.put("street",street);
                map.put("home",home);
                map.put("state",state);
                map.put("city",city);
                map.put("zip",zip);


                mdata.child(orderPushID).child("deliverAddress").setValue(map);


                DatabaseReference suggestData=FirebaseDatabase.getInstance().getReference().child(customerUserID).child("suggestOrder").child(suggestID);

                suggestData.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshotSD) {

                        HashMap<String,String> hashMap=new HashMap<>();

                        if(dataSnapshotSD.child("imageUrlFront").exists())
                        {
                            String front=dataSnapshotSD.child("imageUrlFront").getValue().toString();
                            hashMap.put("frontImage",front);
                        }

                        if(dataSnapshotSD.child("imageUrlBack").exists())
                        {
                            String back=dataSnapshotSD.child("imageUrlBack").getValue().toString();
                            hashMap.put("backImage",back);
                        }

                        if(dataSnapshotSD.child("text").exists())
                        {
                            String text=dataSnapshotSD.child("text").getValue().toString();
                            hashMap.put("text",text);
                        }

                        mdata.child(orderPushID).child("orderItemData").setValue(hashMap);

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

                final Intent intent = new Intent(suggestAddress.this, suggestPlaceOrder.class);

                intent.putExtra("suggestID",suggestID);
                intent.putExtra("fabricKey",fabricKey);
                intent.putExtra("colourKey",colourKey);
                intent.putExtra("orderPushID",orderPushID);
                intent.putExtra("tailorID",tailorID);
                startActivity(intent);



            }
        });



    }



    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng(location.getLatitude(), location.getLongitude() );
            String jhf="jdf";

        } catch (Exception ex) {

            ex.printStackTrace();
        }

        return p1;
    }
}
