package com.example.fashiondesigner.suggest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.fashiondesigner.Adapter.Model.measurement;
import com.example.fashiondesigner.Adapter.measurementAdupter;
import com.example.fashiondesigner.R;
import com.example.fashiondesigner.chat.meassageActivity;
import com.example.fashiondesigner.measurements.PlayVideo;
import com.example.fashiondesigner.measurements.cMeasurement;
import com.example.fashiondesigner.measurements.deliverAddress;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

public class customerMeasurement extends AppCompatActivity {

    String tailorID,suggestID,fabricKey,colourKey;


    DatabaseReference mdata;
    PhotoView photoView;

    ArrayList<measurement> arrayListMeasurement;
    RecyclerView recyclerView;
    measurementAdupter adapter;
    int k=0;

    ArrayList<String> inputMeasurement;
    ArrayList<String> measurementName;
    String customerUserID;

    EditText height;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_measurement);

        Intent intent = getIntent();
        tailorID = intent.getStringExtra("tailorID");
        suggestID=intent.getStringExtra("suggestID");
        colourKey=intent.getStringExtra("colourKey");
        fabricKey=intent.getStringExtra("fabricKey");

        height=findViewById(R.id.height);

        mdata= FirebaseDatabase.getInstance().getReference().child(tailorID).child("suggestOrder").child(suggestID);

        FirebaseUser currentUser= FirebaseAuth.getInstance().getCurrentUser();
        customerUserID=currentUser.getUid();

        final DatabaseReference customerData=FirebaseDatabase.getInstance().getReference().child(customerUserID);

        photoView = findViewById(R.id.photo);
        Button conform=findViewById(R.id.conform);
        Button video=findViewById(R.id.video);

        Button bookTailor=findViewById(R.id.bookTailor);

        bookTailor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(customerMeasurement.this, meassageActivity.class);
                intent.putExtra("userID",tailorID);
                startActivity(intent);

            }
        });

        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(customerMeasurement.this, PlayVideo.class);
                startActivity(intent);
            }
        });


        arrayListMeasurement = new ArrayList<>();

        recyclerView=findViewById(R.id.recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        adapter=new measurementAdupter(this,arrayListMeasurement);
        recyclerView.setAdapter(adapter);

        mdata.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String gender=dataSnapshot.child("gender").getValue().toString();

                if(gender.equals("1"))
                {
                    photoView.setImageResource(R.drawable.men);
                }
                else if(gender.equals("0"))
                {
                    photoView.setImageResource(R.drawable.women);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mdata.child("measurementData").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot1) {

                for(DataSnapshot ds:dataSnapshot1.getChildren())
                {
                    measurement mModel=new measurement();
                    String data=ds.getValue().toString();
                    mModel.setQuestion(data);
                    mModel.setSuggestMnt(true);
                    arrayListMeasurement.add(mModel);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        inputMeasurement=new ArrayList<>();
        measurementName=new ArrayList<>();
        final HashMap<String,Object> map=new HashMap<>();


        conform.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String ht=height.getText().toString();


                if((arrayListMeasurement.size()==inputMeasurement.size()) && (ht!= null)  )
                {
                    mdata.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            String gender=dataSnapshot.child("gender").getValue().toString();

                            if(gender.equals("1"))
                            {
                                map.put("gender","1");
                            }
                            else if(gender.equals("0"))
                            {
                                map.put("gender","0");
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                    DatabaseReference  pushData=  customerData.child("order").push();
                    String push_key=pushData.getKey();
                    map.put("mData",inputMeasurement);
                    map.put("orderPushID",push_key);
                    map.put("suggestID",suggestID);
                    map.put("height",ht);

                    pushData.setValue(map);

                    customerData.child("order").child(push_key).child("orderState").setValue("0");


                    Toast.makeText(customerMeasurement.this,"saved",Toast.LENGTH_SHORT).show();


                    Intent intent = new Intent(customerMeasurement.this, suggestAddress.class);

                    intent.putExtra("suggestID",suggestID);
                    intent.putExtra("fabricKey",fabricKey);
                    intent.putExtra("colourKey",colourKey);
                    intent.putExtra("orderPushID",push_key);
                    intent.putExtra("tailorID",tailorID);
                    startActivity(intent);

                }
                else
                {
                    Toast.makeText(customerMeasurement.this,"You should provide all the data!",Toast.LENGTH_SHORT).show();
                }


            }
        });






    }

    public void goTOGetMeasuremet(String data, String question) {
        if(!(measurementName.contains(question)))
        {
            measurementName.add(question);
            inputMeasurement.add(data);
        }
        else
        {
            for (int j=0;j<measurementName.size();j++)
            {
                if(question==measurementName.get(j))
                {
                    inputMeasurement.set(j,data);
                    break;
                }
            }
        }

    }
}
