package com.example.fashiondesigner.suggest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.fashiondesigner.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class viewSuggestMeasurement extends AppCompatActivity {

    String cutomerID,suggestID;
    String build="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_suggest_measurement);

        Intent intent = getIntent();
        cutomerID = intent.getStringExtra("customerID");
        suggestID=intent.getStringExtra("suggestID");


        final TextView measurement=findViewById(R.id.measurents);
        final TextView additonal=findViewById(R.id.additional);

        Button back=findViewById(R.id.back);

        FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
        final String userID=user.getUid();
        final DatabaseReference mdata= FirebaseDatabase.getInstance().getReference().child(userID).child("suggestOrder").child(suggestID);

        mdata.child("measurementData").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot ds: dataSnapshot.getChildren())
                {

                    String data=ds.getValue().toString();

                    build=build+data+"\n\n";
                }

                measurement.setText(build);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mdata.child("additionalMeasurement").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshotAM) {
                if (dataSnapshotAM.exists())
                {

                    String add=dataSnapshotAM.getValue().toString();
                    additonal.setText(add);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


       back.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

                  Intent intent = new Intent(viewSuggestMeasurement.this, selectGender.class);
                  intent.putExtra("customerID",cutomerID);
                  intent.putExtra("suggestID",suggestID);
                    startActivity(intent);

           }
       });

    }
}
