package com.example.fashiondesigner.suggest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.fashiondesigner.R;
import com.example.fashiondesigner.chat.chatUsersAdapter;
import com.example.fashiondesigner.chat.chatUsersModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class suggestViewC extends AppCompatActivity {

    RecyclerView recyclerView;
    chatUsersAdapter adapter;
    ArrayList<chatUsersModel> arrayListSuggest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggest_view_c);

        FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
        final String userID=user.getUid();
        DatabaseReference mdata= FirebaseDatabase.getInstance().getReference().child(userID).child("suggestOrder");

        recyclerView=findViewById(R.id.recyclerView);

        arrayListSuggest=new ArrayList<>();
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));


        adapter=new chatUsersAdapter(this,arrayListSuggest);
        recyclerView.setAdapter(adapter);


        mdata.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                if(dataSnapshot.exists())
                {
                    for(DataSnapshot ds:dataSnapshot.getChildren())
                    {

                        final String tailorID=ds.child("tailorID").getValue().toString();

                        if(ds.child("suggestID").exists())
                        {
                            final String suggestID=ds.child("suggestID").getValue().toString();

                            DatabaseReference cdata=FirebaseDatabase.getInstance().getReference().child(tailorID).child("tailorData");
                            cdata.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshotC) {

                                    String imageUrl=dataSnapshotC.child("imageuri").getValue().toString();
                                    String name=dataSnapshotC.child("businessName").getValue().toString();

                                    chatUsersModel mModel=new chatUsersModel();
                                    mModel.setImaageUrl(imageUrl);
                                    mModel.setName(name.toUpperCase() + "\n\n Order ID : " + suggestID );
                                    mModel.setSuggestCustomer(true);
                                    mModel.setSuggestID(suggestID);
                                    mModel.setUserID(tailorID);

                                    arrayListSuggest.add(mModel);
                                    adapter.notifyDataSetChanged();
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });

                        }
                        else if(ds.child("pushID").exists())
                        {
                            final String suggestID=ds.child("pushID").getValue().toString();

                            DatabaseReference cdata=FirebaseDatabase.getInstance().getReference().child(tailorID).child("tailorData");
                            cdata.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshotC) {

                                    String imageUrl=dataSnapshotC.child("imageuri").getValue().toString();
                                    String name=dataSnapshotC.child("businessName").getValue().toString();

                                    chatUsersModel mModel=new chatUsersModel();
                                    mModel.setImaageUrl(imageUrl);
                                    mModel.setName(name.toUpperCase() + "\n\n Order ID : " + suggestID );
                                    mModel.setSuggestCustomer(true);
                                    mModel.setSuggestID(suggestID);
                                    mModel.setUserID(tailorID);

                                    arrayListSuggest.add(mModel);
                                    adapter.notifyDataSetChanged();
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });

                        }




                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
