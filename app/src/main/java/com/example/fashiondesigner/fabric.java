package com.example.fashiondesigner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.fashiondesigner.Adapter.FabricVerticalAdupter;
import com.example.fashiondesigner.Adapter.Model.FabricHrizontalAdupter;
import com.example.fashiondesigner.Adapter.Model.HorizontalModel;
import com.example.fashiondesigner.Adapter.Model.VerticalModel;
import com.example.fashiondesigner.customerSearch.colourFull;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

public class fabric extends AppCompatActivity {

    String click,clickItemName,user_ID,subClick,key,itempushID;
    ArrayList<Integer> keyArr=new ArrayList<>();


    DatabaseReference mdata;
    DatabaseReference mdata1;
    StorageReference mstorage;

    ImageView getImage;
    private int GALLERY = 1, CAMERA = 2;
    public Uri filePath;

    ArrayList<HorizontalModel> arrayListHorizontal;
    RecyclerView fabricReView;
    FabricHrizontalAdupter adapter;

    RecyclerView verticalRecyclerView;
    FabricVerticalAdupter adapterColour;
    ArrayList<VerticalModel> arrayListVertical;
    boolean flag=false;
    String fabricName=";";

    String colourKey=";";

     Button remove;
     int q=0;
     private int w=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fabric);

        Intent intent = getIntent();
        click = intent.getStringExtra("clickCategory").toLowerCase();
        subClick = intent.getStringExtra("subClickCategory").toLowerCase();
        clickItemName=intent.getStringExtra("clickItemName").toLowerCase();
        itempushID=intent.getStringExtra("pushID");


        FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
        user_ID=user.getUid();

        mdata1= FirebaseDatabase.getInstance().getReference().child(user_ID).child("fabric");
        mstorage= FirebaseStorage.getInstance().getReference();


        TextView clickItem=findViewById(R.id.clickItem);
        clickItem.setText(clickItemName);

        Button more=findViewById(R.id.more);
        Button viewDress=findViewById(R.id.viewDress);
        remove=findViewById(R.id.remove);

        viewDress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(fabric.this , colourFull.class);
                intent.putExtra("clickCategory",click);
                intent.putExtra("subClickCategory",subClick);
                intent.putExtra("clickItemName",clickItemName);
                intent.putExtra("itempushID",itempushID);

                startActivity(intent);
            }
        });



        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert();
            }
        });


        arrayListHorizontal = new ArrayList<>();

        fabricReView=findViewById(R.id.fabricRe);
        fabricReView.setHasFixedSize(true);

        fabricReView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));

        adapter=new FabricHrizontalAdupter(this,arrayListHorizontal);
        fabricReView.setAdapter(adapter);


        arrayListVertical=new ArrayList<>();

        verticalRecyclerView=findViewById(R.id.colourRe);
        verticalRecyclerView.setHasFixedSize(true);

        verticalRecyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));


        adapterColour=new FabricVerticalAdupter(this,arrayListVertical);
        verticalRecyclerView.setAdapter(adapterColour);


       mdata1.child("fabricDetails").addValueEventListener(new ValueEventListener() {
           @Override
           public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
               arrayListHorizontal.clear();

               for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                   HorizontalModel data= dataSnapshot1.getValue(HorizontalModel.class);

                   arrayListHorizontal.add(data);
                   adapter.notifyDataSetChanged();

               }
           }

           @Override
           public void onCancelled(@NonNull DatabaseError databaseError) {

           }
       });




    }


    private void alert() {

        View view1=(LayoutInflater.from(fabric.this)).inflate(R.layout.fabric_alert,null);
        AlertDialog.Builder alertdialog=new AlertDialog.Builder(fabric.this);
        alertdialog.setView(view1);


        final EditText getData=view1.findViewById(R.id.itemName);
        final EditText getPattern=view1.findViewById(R.id.fabricPattern);
        final EditText getCost=view1.findViewById(R.id.cost);
        getImage=view1.findViewById(R.id.itemSelect);
        final Button btn=view1.findViewById(R.id.itemAdd);


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
            }
        });


        alertdialog.setCancelable(false)
                .setPositiveButton("ADD", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {



                        String s=getData.getText().toString();
                        String p=getPattern.getText().toString();
                        String c=getCost.getText().toString();
                        upload(s,p,c);



                    }

                })
                .setNegativeButton("CANCLE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });





        alertdialog.create();
        alertdialog.show();

    }


    private void showPictureDialog() {

        androidx.appcompat.app.AlertDialog.Builder pictureDialog = new androidx.appcompat.app.AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                dispathTakePictureIntent();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);


    }


    private  void dispathTakePictureIntent()
    {
        Intent takePictureIntent= new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(takePictureIntent.resolveActivity(getPackageManager())!= null) {
            File photoFile = null;
            try {
                {
                    photoFile = createImageFile();
                }
            } catch (IOException ex) {

            }

            if (photoFile != null) {
                Uri photoUri = FileProvider.getUriForFile(this, "com.example.android.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(takePictureIntent, CAMERA);
            }
        }
    }

    String currentPhotoPath;

    private  File createImageFile() throws IOException
    {
        String timeStamp=new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName="JPEG_"+timeStamp+"_";
        File storageDir= Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image=File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        currentPhotoPath=image.getAbsolutePath();
        return  image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_CANCELED) {
            return;
        }

        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                filePath = contentURI;

                Glide.with(fabric.this).load(contentURI).into(getImage);

            }

        } else if (requestCode == CAMERA) {

            File f=new File(currentPhotoPath);
            filePath=Uri.fromFile(f);

            Intent mediaScanIntent=new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri=Uri.fromFile(f);
            Glide.with(fabric.this).load(contentUri).into(getImage);
            mediaScanIntent.setData(contentUri);
            this.sendBroadcast(mediaScanIntent);

        }
    }

    private void upload(final String s,final String p,final String c) {

        if (filePath != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Wait...");
            progressDialog.show();

            final StorageReference ref = mstorage.child("Fabric/" + UUID.randomUUID().toString());
            ref.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                   String savedUri = uri.toString();

                                    HashMap<String,String> hashMap=new HashMap<>();
                                    hashMap.put("imageUrl",savedUri);
                                    hashMap.put("name",s);
                                    hashMap.put("pattern",p);
                                    hashMap.put("price",c);

                                    DatabaseReference pushRef= mdata1.child("fabricDetails").push();
                                    String key_ID = pushRef.getKey();
                                    hashMap.put("pushID",key_ID);
                                    pushRef.setValue(hashMap);


                                    mdata1.child("itemFabricColours").child(itempushID).child("fabric").child(key_ID).setValue(key_ID);


                                }
                            });
                            progressDialog.dismiss();

                            Toast.makeText(fabric.this, "Succesful", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(fabric.this, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                    .getTotalByteCount());


                            progressDialog.setMessage("Successful " );
                        }
                    });
        }


    }


    String select,fabricType;
    //int i;
   public void functionToRun(final String fabricPushID)
    {

        for(q=0;q<arrayListHorizontal.size();q++)
        {
            if(arrayListHorizontal.get(q).getSelectHorizontal())
            {

                select=arrayListHorizontal.get(q).getPushID();
                fabricType=arrayListHorizontal.get(q).getName();

                VerticalModel mVerticalModel=new VerticalModel();

                mVerticalModel.setID(user_ID);

                mVerticalModel.setFabricPushID(fabricPushID);

                mVerticalModel.setTitle(arrayListHorizontal.get(q).getName().toUpperCase());

                arrayListVertical.clear();

                arrayListVertical.add(mVerticalModel);

                adapterColour.notifyDataSetChanged();



                remove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(w==1)
                        {
                            final String t=arrayListHorizontal.get(q).getPushID();
                            mdata1.child("fabricDetails").child(t).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot3) {
                                    if(dataSnapshot3.exists())
                                    {
                                        mdata1.child("fabricDetails").child(t).removeValue();
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                            mdata1.child("colours").child(t).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot1) {
                                    if(dataSnapshot1.exists())
                                    {
                                        mdata1.child("colours").child(t).removeValue();
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });

                            mdata1.child("fabricFulldress").child(itempushID).child(t).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot2) {
                                    if (dataSnapshot2.exists())
                                    {
                                        mdata1.child("fabricFulldress").child(itempushID).child(t).removeValue();
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });

                            mdata1.child("fullFabricColours").child(itempushID).child("fabric").child(t).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshotR) {
                                    if (dataSnapshotR.exists())
                                    {
                                        mdata1.child("fullFabricColours").child(itempushID).child("fabric").child(t).removeValue();
                                        mdata1.child("fullFabricColours").child(itempushID).child("colour").child(t).removeValue();
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });

                            mdata1.child("itemFabricColours").child(itempushID).child("fabric").child(t).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshotRt) {
                                    if (dataSnapshotRt.exists())
                                    {
                                        mdata1.child("itemFabricColours").child(itempushID).child("fabric").child(t).removeValue();
                                        mdata1.child("itemFabricColours").child(itempushID).child("colour").child(t).removeValue();
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });

                            arrayListHorizontal.get(q).setSelectHorizontal(false);
                           // w=0;
                            Intent intent = new Intent(fabric.this , fabric.class);
                            intent.putExtra("clickCategory",click);
                            intent.putExtra("subClickCategory",subClick);
                            intent.putExtra("clickItemName",clickItemName);
                            intent.putExtra("pushID",itempushID);

                            startActivity(intent);
                        }


                        w=1;
                    }
                });

                arrayListHorizontal.get(q).setSelectHorizontal(false);
                break;

            }
        }



    }



    String fabricPushkey;
    public void MoreToRun(String fabricPushID)
    {
        for(int i=0;i<arrayListVertical.size();i++)
        {
            if(arrayListVertical.get(i).getMore())
            {

                flag=true;
                fabricName=arrayListVertical.get(i).getTitle();
                fabricPushkey=arrayListVertical.get(i).getFabricPushID();
                alertColour(fabricPushID);
                arrayListVertical.get(i).setMore(false);

            }
        }

    }

    private void alertColour(final String fabricPushID) {

        View view1=(LayoutInflater.from(fabric.this)).inflate(R.layout.itemdialogbox2,null);
        AlertDialog.Builder alertdialog=new AlertDialog.Builder(fabric.this);
        alertdialog.setView(view1);


        final EditText getData=view1.findViewById(R.id.itemName);
        getImage=view1.findViewById(R.id.itemSelect);
        final Button btn=view1.findViewById(R.id.itemAdd);
        getData.setHint("colour");


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
            }
        });


        alertdialog.setCancelable(false)
                .setPositiveButton("ADD", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {



                        String s=getData.getText().toString();
                        Cupload(s,fabricPushID);



                    }

                })
                .setNegativeButton("CANCLE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });





        alertdialog.create();
        alertdialog.show();



    }

    private void Cupload(final String s,final String fabricPushID) {

        if (filePath != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Wait...");
            progressDialog.show();

            final StorageReference ref = mstorage.child("Colours/" + UUID.randomUUID().toString());
            ref.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    String savedUri = uri.toString();

                                    HashMap<String,String> hashMap=new HashMap<>();
                                    hashMap.put("imageUrl",savedUri);
                                    hashMap.put("name",s);

                                    DatabaseReference pushRef= mdata1.child("colours").child(fabricPushID).push();
                                    String key_ID = pushRef.getKey();
                                    hashMap.put("pushID",key_ID);
                                    pushRef.setValue(hashMap);

                                    mdata1.child("itemFabricColours").child(itempushID).child("colour").child(fabricPushID).child(key_ID).setValue(key_ID);


                                }
                            });
                            progressDialog.dismiss();

                            Toast.makeText(fabric.this, "Succesful", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(fabric.this, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                    .getTotalByteCount());


                            progressDialog.setMessage("Successful " );
                        }
                    });
        }


    }
    //String fabricPushKey;

    public void SelectToRun(String fabricPushID, String colourpushID) {

        for(int i=0;i<arrayListVertical.size();i++)
        {
            if(arrayListVertical.get(i).getKeySel())
            {
               // colourKey=colourKey+arrayListVertical.get(i).getTitle()+";"+arrayListVertical.get(i).getKey()+";";
                colourKey=arrayListVertical.get(i).getKey();
                fabricPushkey=arrayListVertical.get(i).getFabricPushID();
                alertAdd(fabricPushID,colourpushID);
                arrayListVertical.get(i).setKeySel(false);

            }
        }
    }

    private void alertAdd(final String fabricPushID, final String colourpushID) {


        View view1=(LayoutInflater.from(fabric.this)).inflate(R.layout.itemdialogbox,null);
        AlertDialog.Builder alertdialog=new AlertDialog.Builder(fabric.this);
        alertdialog.setView(view1);


        final EditText getData=view1.findViewById(R.id.itemName);
        final EditText getPrice=view1.findViewById(R.id.itemPrice);
        getImage=view1.findViewById(R.id.itemSelect);
        final Button btn=view1.findViewById(R.id.itemAdd);


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
            }
        });


        alertdialog.setCancelable(false)
                .setPositiveButton("ADD", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {



                        String s=getData.getText().toString();
                        String sP=getPrice.getText().toString();
                        uploadColour(s,sP,fabricPushID,colourpushID);



                    }

                })
                .setNegativeButton("CANCLE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });





        alertdialog.create();
        alertdialog.show();

    }

    private void uploadColour(final String s, final String sP, final String fabricPushID, final String colourpushID) {

        if (filePath != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Wait...");
            progressDialog.show();

            final StorageReference ref = mstorage.child("Colour/" + UUID.randomUUID().toString());
            ref.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    String savedUri = uri.toString();

                                    HashMap<String,String> hashMap=new HashMap<>();
                                    hashMap.put("imageUrl",savedUri);
                                    hashMap.put("name",s);
                                    hashMap.put("price",sP);



                                  //  DatabaseReference pushRef= mdata1.child("fabricFulldress").child(fabricPushID).child(colourpushID).child(itempushID).push();

                                   DatabaseReference pushRef=mdata1.child("fabricFulldress").child(itempushID).child(fabricPushID).child(colourpushID).push();

                                    mdata1.child("fullFabricColours").child(itempushID).child("fabric").child(fabricPushID).setValue(fabricPushID);
                                    mdata1.child("fullFabricColours").child(itempushID).child("colour").child(fabricPushID).child(colourpushID).setValue(colourpushID);

                                    String key_ID = pushRef.getKey();
                                    hashMap.put("pushID",key_ID);
                                    pushRef.setValue(hashMap);




                                }
                            });
                            progressDialog.dismiss();

                            Toast.makeText(fabric.this, "Succesful", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(fabric.this, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                    .getTotalByteCount());


                            progressDialog.setMessage("Successful " );
                        }
                    });
        }
    }

   /* public void functionToRunR(String fabricPushID) {

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for(;q<arrayListHorizontal.size();q++)
                {
                    if(arrayListHorizontal.get(q).getSelectHorizontal()) {



                    }

                }


            }
        });

        if(w==0)
        {
            for(i=0;i<arrayListHorizontal.size();i++)
            {
                if(arrayListHorizontal.get(i).getSelectHorizontal())
                {

                    select=arrayListHorizontal.get(i).getPushID();
                    fabricType=arrayListHorizontal.get(i).getName();

                    VerticalModel mVerticalModel=new VerticalModel();

                    mVerticalModel.setID(user_ID);

                    mVerticalModel.setFabricPushID(fabricPushID);

                    mVerticalModel.setTitle(arrayListHorizontal.get(i).getName().toUpperCase());

                    arrayListVertical.clear();

                    arrayListVertical.add(mVerticalModel);

                    adapterColour.notifyDataSetChanged();

                    arrayListHorizontal.get(i).setSelectHorizontal(false);
                    break;

                }
            }


        }

    }

    */
}
