package com.example.fashiondesigner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.solver.widgets.Snapshot;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {

    EditText getemail, password;
    Button btLogin;
    TextView tvRegister;
    public FirebaseAuth mFirebaseAuth;
   // private FirebaseAuth.AuthStateListener mAuthListner;
    FirebaseDatabase database;
    DatabaseReference mdata2;
    String user_id;
    FirebaseUser currentUser;
    //String fg="aaa";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mFirebaseAuth = FirebaseAuth.getInstance();
        getemail = findViewById(R.id.editText);
        password = findViewById(R.id.editText2);
        btLogin = findViewById(R.id.button);
        tvRegister = findViewById(R.id.textView);
        database = FirebaseDatabase.getInstance();
        mdata2 = database.getReference();



        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    String email = getemail.getText().toString();
                    String pwd = password.getText().toString();

                    if (email.isEmpty()) {
                        getemail.setError("Please Enter email");
                        getemail.requestFocus();
                    } else if (pwd.isEmpty()) {
                        password.setError("Please Enter Password");
                        password.requestFocus();
                    } else if (email.isEmpty() && pwd.isEmpty()) {
                        Toast.makeText(MainActivity.this, "Fiels are empty!", Toast.LENGTH_SHORT).show();
                    } else if (!(email.isEmpty() && pwd.isEmpty())) {
                        mFirebaseAuth.signInWithEmailAndPassword(email, pwd).addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {

                                if (!task.isSuccessful()) {
                                    Toast.makeText(MainActivity.this, "Logging is unsuccessful!", Toast.LENGTH_SHORT).show();

                                }
                                else {

                                    currentUser=mFirebaseAuth.getCurrentUser();

                                    if(currentUser.isEmailVerified())
                                    {

                                        mdata2.addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                user_id = mFirebaseAuth.getCurrentUser().getUid();

                                                String fg = dataSnapshot.child(user_id).child("flag").getValue(String.class);
                                                String tg = dataSnapshot.child(user_id).child("tag").getValue(String.class);

                                                if (fg.equals("1")) {
                                                    if (tg.equals("0")) {
                                                        Intent i = new Intent(MainActivity.this, spdetail.class);
                                                        startActivity(i);
                                                        finish();
                                                    } else if (tg.equals("1")) {
                                                        Intent i = new Intent(MainActivity.this, Tailor2.class);
                                                        startActivity(i);
                                                        finish();
                                                    } else {
                                                        Toast.makeText(MainActivity.this, "Logging Error!", Toast.LENGTH_SHORT).show();
                                                    }

                                                } else if (fg.equals("0")) {
                                                    if (tg.equals("0")) {
                                                        Intent i = new Intent(MainActivity.this, customerProfile.class);
                                                        startActivity(i);
                                                        finish();
                                                    } else if (tg.equals("1")) {
                                                        Intent i = new Intent(MainActivity.this, Customer.class);
                                                        startActivity(i);
                                                        finish();
                                                    } else {
                                                        Toast.makeText(MainActivity.this, "Logging Error!", Toast.LENGTH_SHORT).show();
                                                    }
                                                } else {
                                                    Toast.makeText(MainActivity.this, "Error in logging!", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError error) {
                                                // Failed to read value
                                                // Log.w(TAG, "Failed to read value.", error.toException());
                                            }


                                        });

                                    }

                                    else
                                    {
                                        final Toast toast=Toast.makeText(MainActivity.this,"Please verification the email first!",Toast.LENGTH_SHORT);
                                        toast.show();
                                        Handler handler = new Handler();
                                        handler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                toast.cancel();
                                            }
                                        }, 5000);

                                      //  Intent i = new Intent(MainActivity.this, MainActivity.class);
                                      //  startActivity(i);
                                      //  finish();
                                    }



                                }

                            }
                        });
                    } else {
                        Toast.makeText(MainActivity.this, "Error occured!", Toast.LENGTH_SHORT).show();
                    }

                }

        });


        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent i = new Intent(MainActivity.this, Register.class);
                startActivity(i);
                finish();
            }
        });


    }
}
