package com.example.fashiondesigner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class category extends AppCompatActivity {

    Spinner category;
    Button set;
    ListView list;
    ArrayList<String> arrayList;
    ArrayAdapter<String> adapter;
    ArrayAdapter<String> categoryAdapter;

    String click,result;
    List<String> subCategory;

    String uid,country,city;
    DatabaseReference mdata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        category=findViewById(R.id.spinnerCategory);
        set=findViewById(R.id.addCategory);
        list=findViewById(R.id.listCategory);

        Intent intent = getIntent();
        click = intent.getStringExtra("click");

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        uid=user.getUid();
        mdata= FirebaseDatabase.getInstance().getReference();

        TextView title=findViewById(R.id.profile);
        title.setText(click.toUpperCase());


        subCategory=new ArrayList<>();
        subCategory.add("-select your sub category-");

        mdata.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                for (DataSnapshot dataSnapshot1 : dataSnapshot.child("sub "+click.toLowerCase()).getChildren()) {
                    String data = dataSnapshot1.getValue().toString();
                    subCategory.add(data.toUpperCase());
                }
                subCategory.add("Add sub category");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        arrayList=new ArrayList<String>();
        adapter=new ArrayAdapter<String>(category.this, R.layout.raw_item,arrayList);

        categoryAdapter=new ArrayAdapter<>(category.this,android.R.layout.simple_spinner_item,subCategory);
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        category.setAdapter(categoryAdapter);


        category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                result=category.getSelectedItem().toString();
                String gdh="dj";
                if((!result.equals("-select your sub category-"))&& (!result.equals("Add sub category")))
                {
                    set.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if(!(arrayList.contains(result)))
                            {
                                mdata.child(uid).child("subCategory").child("sub "+ click.toLowerCase()).child(result.toLowerCase()).setValue(result.toLowerCase());

                                arrayList.add(result.toUpperCase());
                                adapter.notifyDataSetChanged();
                            }

                        }
                    });
                }

                else if (result.equals("Add sub category")) {

                    alert();

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        mdata.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                arrayList.clear();
                for (DataSnapshot dataSnapshot1 : dataSnapshot.child(uid).child("subCategory").child("sub "+ click.toLowerCase()).getChildren()) {
                    String data = dataSnapshot1.getValue().toString();
                    arrayList.add(data.toUpperCase()); // add all data into list.
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        list.setAdapter(adapter);


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String subClick =(String) parent.getItemAtPosition(position);
                Intent intent = new Intent(category.this ,ViewItem.class);
                intent.putExtra("click",click);
                intent.putExtra("subClick",subClick);
                startActivity(intent);

            }
        });


    }


    public void alert()
    {
        View view1=(LayoutInflater.from(category.this)).inflate(R.layout.tailordalogbox,null);
        AlertDialog.Builder alertdialog=new AlertDialog.Builder(category.this);
        alertdialog.setView(view1);


        final EditText getData=view1.findViewById(R.id.reenterbn);

        alertdialog.setCancelable(false)
                .setPositiveButton("ADD", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String s=getData.getText().toString();
                        if(!(arrayList.contains(s)))
                        {
                            mdata.child(uid).child("subCategory").child("sub "+click.toLowerCase()).child(s.toLowerCase()).setValue(s.toLowerCase());
                            subCategory.remove(0);
                            subCategory.remove(subCategory.size() - 1);
                            subCategory.add(s.toUpperCase());
                            mdata.child("sub "+click.toLowerCase()).setValue(subCategory);
                            subCategory.add("Add sub category");
                            subCategory.add(0,"-select your sub category-");



                            arrayList.add(s.toUpperCase());
                            adapter.notifyDataSetChanged();
                        }


                    }

                })
                .setNegativeButton("CANCLE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });



        alertdialog.create();
        alertdialog.show();

    }
}
