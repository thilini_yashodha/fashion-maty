package com.example.fashiondesigner.chat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fashiondesigner.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;

public class messageAdupter extends RecyclerView.Adapter<messageAdupter.ViewHolder> {

   public  static  final int msg_type_left=0;
    public  static  final int msg_type_right=1;


    private Context mContext;
    private List<userModel> mUser;

    FirebaseUser fuser;

    public messageAdupter(Context mContext, List<userModel> mUser) {
        this.mContext = mContext;
        this.mUser = mUser;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType==msg_type_right)
        {
            View view= LayoutInflater.from(mContext).inflate(R.layout.chat_item_right,parent,false);
            return new messageAdupter.ViewHolder(view);
        }
        else
        {
            View view= LayoutInflater.from(mContext).inflate(R.layout.chat_item_left,parent,false);
            return new messageAdupter.ViewHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


        userModel musermodel=mUser.get(position);
        holder.show_message.setText(musermodel.getMessage());

    }

    @Override
    public int getItemCount() {
        return mUser.size();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView show_message;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            show_message=itemView.findViewById(R.id.showMessage);
        }
    }

    @Override
    public int getItemViewType(int position) {


        fuser= FirebaseAuth.getInstance().getCurrentUser();
        if(mUser.get(position).getSender().equals(fuser.getUid()))
        {
            return msg_type_right;
        }
        else
        {
            return msg_type_left;
        }
    }
}


