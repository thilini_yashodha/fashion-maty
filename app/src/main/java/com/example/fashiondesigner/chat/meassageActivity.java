package com.example.fashiondesigner.chat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.fashiondesigner.Customer;
import com.example.fashiondesigner.MainActivity;
import com.example.fashiondesigner.R;
import com.example.fashiondesigner.Register;
import com.example.fashiondesigner.Tailor2;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class meassageActivity extends AppCompatActivity {

    messageAdupter msgAdupter;
    List<userModel> muserChat;
    RecyclerView cRecyclerView;
    DatabaseReference mdata;
    String senderUserID;
    String index="0";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meassage);

        Intent intent = getIntent();
        final String receiverUserID = intent.getStringExtra("userID");
        index=intent.getStringExtra("index");

        FirebaseUser currentUser= FirebaseAuth.getInstance().getCurrentUser();
        senderUserID=currentUser.getUid();

        ImageButton sendBtn=findViewById(R.id.sendBtn);
        final ImageButton call=findViewById(R.id.call);

        mdata=FirebaseDatabase.getInstance().getReference();

        cRecyclerView=findViewById(R.id.recyclerView);
        cRecyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setStackFromEnd(true);
        cRecyclerView.setLayoutManager(linearLayoutManager);

        final ImageView image=findViewById(R.id.image);
        final TextView titleName=findViewById(R.id.titleName);


        mdata.child(receiverUserID).child("customerData").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshotx) {

                if(dataSnapshotx.exists())
                {
                    final String name=dataSnapshotx.child("name").getValue().toString();
                    final String imageUrl=dataSnapshotx.child("imageUrl").getValue().toString();

                    titleName.setText(name);

                    Glide.with(image.getContext())
                            .load(imageUrl)
                            .into(image);


                    call.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Intent i = new Intent(meassageActivity.this, callingActivity.class);
                            i.putExtra("receiverID",receiverUserID);
                            startActivity(i);
                            finish();
                        }
                    });
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        mdata.child(receiverUserID).child("tailorData").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshoty) {

                if(dataSnapshoty.exists())
                {
                    final String name=dataSnapshoty.child("businessName").getValue().toString();
                    final String imageUrl=dataSnapshoty.child("imageuri").getValue().toString();

                    titleName.setText(name);

                    Glide.with(image.getContext())
                            .load(imageUrl)
                            .into(image);

                    call.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Intent i = new Intent(meassageActivity.this, callingActivity.class);
                            i.putExtra("receiverID",receiverUserID);
                            startActivity(i);
                        }
                    });
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TextView message=findViewById(R.id.sendText);
                String textMsg=message.getText().toString();

                HashMap<String,String> map=new HashMap<>();

                map.put("message",textMsg);
                map.put("reciver",receiverUserID);
                map.put("sender",senderUserID);

                mdata.child("chats").push().setValue(map);

                message.setText("");


            }
        });


        muserChat=new ArrayList<>();

        mdata.child("chats").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshotc) {

                if(dataSnapshotc.exists())
                {
                    muserChat.clear();

                    for(DataSnapshot ds: dataSnapshotc.getChildren())
                    {
                        userModel chat=ds.getValue(userModel.class);
                        if(chat.getReciver().equals(senderUserID)&& chat.getSender().equals(receiverUserID)|| chat.getReciver().equals(receiverUserID)&& chat.getSender().equals(senderUserID))
                        {
                            muserChat.add(chat);
                        }

                        msgAdupter=new messageAdupter(meassageActivity.this,muserChat)  ;
                        cRecyclerView.setAdapter(msgAdupter);
                    }

                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }

    @Override
    protected void onStart() {
        super.onStart();

        mdata.child(senderUserID).child("CallingData").child("Ringing")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        if(dataSnapshot.hasChild("ring"))
                        {
                            String callBy=dataSnapshot.child("ring").getValue().toString();

                            Intent i = new Intent(meassageActivity.this, callingActivity.class);
                            i.putExtra("receiverID",callBy);
                            startActivity(i);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    @Override
    public void onBackPressed() {

        if(index!=null)
        {
            mdata.child(senderUserID).child("customerData").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists())
                    {
                        Intent i = new Intent(meassageActivity.this, Customer.class);
                        startActivity(i);
                        finish();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            mdata.child(senderUserID).child("tailorData").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists())
                    {
                        Intent i = new Intent(meassageActivity.this, Tailor2.class);
                        startActivity(i);
                        finish();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });


        }
        else
        {
            super.onBackPressed();
        }

    }
}
