package com.example.fashiondesigner.chat;

public class chatUsersModel {

    String imaageUrl,name,userID,suggestID;

    boolean suggest=false,suggestCustomer=false;

    public String getImaageUrl() {
        return imaageUrl;
    }

    public void setImaageUrl(String imaageUrl) {
        this.imaageUrl = imaageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public boolean getSuggest() {
        return suggest;
    }

    public void setSuggest(boolean suggest) {
        this.suggest = suggest;
    }

    public String getSuggestID() {
        return suggestID;
    }

    public void setSuggestID(String suggestID) {
        this.suggestID = suggestID;
    }

    public boolean getSuggestCustomer() {
        return suggestCustomer;
    }

    public void setSuggestCustomer(boolean suggestCustomer) {
        this.suggestCustomer = suggestCustomer;
    }
}
