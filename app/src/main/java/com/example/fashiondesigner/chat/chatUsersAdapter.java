package com.example.fashiondesigner.chat;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.fashiondesigner.R;
import com.example.fashiondesigner.order.cViewOrderClick;
import com.example.fashiondesigner.suggest.suggestViewCilickCust;
import com.example.fashiondesigner.suggest.suggestViewTClick;

import java.util.List;

public class chatUsersAdapter extends RecyclerView.Adapter<chatUsersAdapter.ViewHolder> {

    private Context mContext;
    private List<chatUsersModel> mUser;

    public chatUsersAdapter(Context mContext, List<chatUsersModel> mUser) {
        this.mContext = mContext;
        this.mUser = mUser;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.chat_users_row,parent,false);
        return new chatUsersAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


        final chatUsersModel cusermodel=mUser.get(position);

        holder.profileName.setText(cusermodel.getName());

        Glide.with(holder.profileImage.getContext())
                .load(cusermodel.getImaageUrl())
                .into(holder.profileImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(cusermodel.getSuggest())
                {
                    Intent intent = new Intent(v.getContext(), suggestViewTClick.class);
                    intent.putExtra("customerID",cusermodel.getUserID());
                    intent.putExtra("suggestID",cusermodel.getSuggestID());
                   mContext.startActivity(intent);
                }
                else if(cusermodel.getSuggestCustomer())
                {
                    Intent intent = new Intent(v.getContext(), suggestViewCilickCust.class);
                    intent.putExtra("tailorID",cusermodel.getUserID());
                    intent.putExtra("suggestID",cusermodel.getSuggestID());
                    mContext.startActivity(intent);
                }
                else
                {
                    Intent intent = new Intent(v.getContext(), meassageActivity.class);
                    intent.putExtra("userID",cusermodel.getUserID());
                    mContext.startActivity(intent);
                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return mUser.size();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder
    {
        ImageView profileImage;
        TextView profileName;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            profileImage=itemView.findViewById(R.id.profileImage);
            profileName=itemView.findViewById(R.id.profileName);
        }
    }
}
