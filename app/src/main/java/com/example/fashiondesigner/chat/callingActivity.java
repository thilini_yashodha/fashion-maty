package com.example.fashiondesigner.chat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.fashiondesigner.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class callingActivity extends AppCompatActivity {

    DatabaseReference mdata;
    String receiverID;
    String senderUserID;
    ImageView call;
    String check="";
    TextView text;

    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calling);

        Intent intent = getIntent();
        receiverID=intent.getStringExtra("receiverID");

        final ImageView imageView=findViewById(R.id.proImage);
        final TextView nameTV=findViewById(R.id.name_calling);
        text=findViewById(R.id.txt);
        call=findViewById(R.id.call);
        ImageView cut=findViewById(R.id.cut);

        mediaPlayer=MediaPlayer.create(this,R.raw.ring);


        FirebaseUser currentUser= FirebaseAuth.getInstance().getCurrentUser();
        senderUserID=currentUser.getUid();

        mdata= FirebaseDatabase.getInstance().getReference();

        cut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mediaPlayer.stop();
                check="true";
                cancel();
            }
        });


        mdata.child(receiverID).child("customerData").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshotx) {

                if(dataSnapshotx.exists())
                {
                    String rname=dataSnapshotx.child("name").getValue().toString();
                    String rimage=dataSnapshotx.child("imageUrl").getValue().toString();

                    Glide.with(imageView.getContext())
                            .load(rimage)
                            .into(imageView);


                    nameTV.setText(rname);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        mdata.child(receiverID).child("tailorData").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshoty) {

                if(dataSnapshoty.exists())
                {
                    String rname=dataSnapshoty.child("businessName").getValue().toString();
                    String rimage=dataSnapshoty.child("imageuri").getValue().toString();

                    Glide.with(imageView.getContext())
                            .load(rimage)
                            .into(imageView);


                    nameTV.setText(rname);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mdata.child(senderUserID).child("CallingData").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists())
                {
                    if(dataSnapshot.getValue().toString().equals("1"))
                    {
                        mediaPlayer.stop();
                        mdata.child(senderUserID).child("CallingData").removeValue();

                        Intent i = new Intent(callingActivity.this, meassageActivity.class);
                        i.putExtra("userID",receiverID);
                        i.putExtra("index","1");
                        startActivity(i);
                        finish();

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mdata.child(senderUserID).child("Tone").removeValue();
                mediaPlayer.stop();

                //final HashMap<String,Object> callingPickUpMap=new HashMap<>();
                //   callingPickUpMap.put("picked","picked");

                mdata.child(senderUserID).child("picked")
                        .setValue("1")
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {

                                if(task.isComplete())
                                {

                                  //  mediaPlayer.stop();
                                    Intent i = new Intent(callingActivity.this, videoActivity.class);
                                    i.putExtra("receiverID",receiverID);
                                    startActivity(i);
                                    finish();
                                }
                            }
                        });


            }
        });

        mdata.child(senderUserID).child("Tone").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                {
                    mediaPlayer.start();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mdata.child(receiverID).child("picked").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshotx) {

                if(dataSnapshotx.exists())
                {
                    mediaPlayer.stop();
                    mdata.child(receiverID).child("picked").removeValue();
                    Intent i = new Intent(callingActivity.this, videoActivity.class);
                    i.putExtra("receiverID",receiverID);
                    startActivity(i);
                    finish();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }


    @Override
    protected void onStart() {
        super.onStart();

        mdata.child(receiverID).child("CallingData")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        if( !check.equals("true") && !dataSnapshot.hasChild("Calling") && !dataSnapshot.hasChild("Ringing"))
                        {

                            final HashMap<String,Object> callinInf=new HashMap<>();

                            callinInf.put("call",receiverID);

                            mdata.child(senderUserID).child("CallingData").child("Calling")
                                    .updateChildren(callinInf)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {

                                            if(task.isSuccessful())
                                            {
                                                final HashMap<String,Object> ringingInf=new HashMap<>();

                                                ringingInf.put("ring",senderUserID);

                                                mdata.child(receiverID).child("CallingData").child("Ringing").updateChildren(ringingInf);
                                                mdata.child(receiverID).child("Tone").setValue("1");

                                            }
                                        }
                                    });
                        }


                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


        mdata.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshotx) {
                if(dataSnapshotx.child(senderUserID).child("CallingData").hasChild("Ringing") && !dataSnapshotx.child(senderUserID).child("CallingData").hasChild("Calling"))
                {

                    call.setVisibility(View.VISIBLE);
                    text.setVisibility(View.GONE);

                }

              /*  if(dataSnapshotx.child(receiverID).child("CallingData").child("Ringing").hasChild("picked"))
                {
                    mediaPlayer.stop();
                    Intent i = new Intent(callingActivity.this, videoActivity.class);
                    i.putExtra("receiverID",receiverID);
                    startActivity(i);
                }

               */
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }


    private void cancel() {

        mdata.child(senderUserID).child("CallingData").child("Calling").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String callingID;

                if(dataSnapshot.exists() && dataSnapshot.hasChild("call"))
                {
                    callingID=dataSnapshot.child("call").getValue().toString();

                    mdata.child(callingID).child("CallingData").child("Ringing").removeValue();
                    mdata.child(callingID).child("CallingData").setValue("1")
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                    if(task.isSuccessful())
                                    {
                                        mdata.child(senderUserID).child("CallingData").child("Calling").removeValue()
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {

                                                        mediaPlayer.stop();
                                                        Intent i = new Intent(callingActivity.this, meassageActivity.class);
                                                        i.putExtra("userID",receiverID);
                                                        i.putExtra("index","1");
                                                        startActivity(i);
                                                        finish();
                                                    }
                                                });
                                    }
                                }
                            });
                }

              /*  else
                {
                    Intent i = new Intent(callingActivity.this, meassageActivity.class);
                    i.putExtra("userID",receiverID);
                    startActivity(i);
                    finish();
                }

               */


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mdata.child(senderUserID).child("CallingData").child("Ringing").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists() && dataSnapshot.hasChild("ring"))
                {
                    String ringingID=dataSnapshot.child("ring").getValue().toString();

                    mdata.child(ringingID).child("CallingData").child("Calling").removeValue();
                    mdata.child(ringingID).child("CallingData").setValue("1")
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                    if(task.isSuccessful())
                                    {
                                        mdata.child(senderUserID).child("CallingData").child("Ringing").removeValue()
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {

                                                        mediaPlayer.stop();
                                                        Intent i = new Intent(callingActivity.this, meassageActivity.class);
                                                        i.putExtra("userID",receiverID);
                                                        i.putExtra("index","1");
                                                        startActivity(i);
                                                        finish();
                                                    }
                                                });
                                    }
                                }
                            });

                }

              /*  else
                {
                    Intent i = new Intent(callingActivity.this, meassageActivity.class);
                    i.putExtra("userID",receiverID);
                    startActivity(i);
                    finish();
                }

               */
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mdata.child(receiverID).child("CallingData").child("Ringing").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists() && dataSnapshot.hasChild("ring"))
                {
                    String ringingID=dataSnapshot.child("ring").getValue().toString();

                    mdata.child(ringingID).child("CallingData").child("Calling").removeValue();
                    mdata.child(ringingID).child("CallingData").setValue("1")
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                    if(task.isSuccessful())
                                    {
                                        mdata.child(receiverID).child("CallingData").child("Ringing").removeValue()
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {

                                                        mediaPlayer.stop();
                                                        Intent i = new Intent(callingActivity.this, meassageActivity.class);
                                                        i.putExtra("userID",receiverID);
                                                        i.putExtra("index","1");
                                                        startActivity(i);
                                                        finish();
                                                    }
                                                });
                                    }
                                }
                            });
                }

               /* else
                {
                    Intent i = new Intent(callingActivity.this, meassageActivity.class);
                    i.putExtra("userID",receiverID);
                    startActivity(i);
                    finish();
                }

                */


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mdata.child(receiverID).child("CallingData").child("Calling").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String callingID;

                if(dataSnapshot.exists() && dataSnapshot.hasChild("call"))
                {
                    callingID=dataSnapshot.child("call").getValue().toString();

                    mdata.child(callingID).child("CallingData").child("Ringing").removeValue();
                    mdata.child(callingID).child("CallingData").setValue("1")
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                    if(task.isSuccessful())
                                    {
                                        mdata.child(receiverID).child("CallingData").child("Calling").removeValue()
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {

                                                        mediaPlayer.stop();
                                                        Intent i = new Intent(callingActivity.this, meassageActivity.class);
                                                        i.putExtra("userID",receiverID);
                                                        i.putExtra("index","1");
                                                        startActivity(i);
                                                        finish();
                                                    }
                                                });
                                    }
                                }
                            });
                }

              /*  else
                {
                    Intent i = new Intent(callingActivity.this, meassageActivity.class);
                    i.putExtra("userID",receiverID);
                    startActivity(i);
                    finish();
                }

               */


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }


}