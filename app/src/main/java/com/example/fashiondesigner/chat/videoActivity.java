package com.example.fashiondesigner.chat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.example.fashiondesigner.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.opentok.android.OpentokError;
import com.opentok.android.Publisher;
import com.opentok.android.PublisherKit;
import com.opentok.android.Session;
import com.opentok.android.Stream;
import com.opentok.android.Subscriber;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class videoActivity extends AppCompatActivity  implements Session.SessionListener, PublisherKit.PublisherListener {

    private static String API_key="46972824";
    private static String SESSION_ID="1_MX40Njk3MjgyNH5-MTYwNDIyMDQxMzgxMH5tVXprNWVMTTdlbXBLdmVNZkcwbVM3cEh-fg";
    private static String TOKEN="T1==cGFydG5lcl9pZD00Njk3MjgyNCZzaWc9NTFkMmUzMzhjODI2YzE1YzQ1MzhmYzdjNzAzNDBmM2EyMDI3YzczNTpzZXNzaW9uX2lkPTFfTVg0ME5qazNNamd5Tkg1LU1UWXdOREl5TURReE16Z3hNSDV0Vlhwck5XVk1UVGRsYlhCTGRtVk5aa2N3YlZNM2NFaC1mZyZjcmVhdGVfdGltZT0xNjA0MjIwNDU3Jm5vbmNlPTAuNzE2NDYwMzM1NzI3NTM4NSZyb2xlPXB1Ymxpc2hlciZleHBpcmVfdGltZT0xNjA2ODE2MTQzJmluaXRpYWxfbGF5b3V0X2NsYXNzX2xpc3Q9";
    private  static final String LOG_TAG= videoActivity.class.getSimpleName();
    private  static  final  int RC_VIDEO_APP_PERM=124;

    private FrameLayout mPublisherViewController;
    private FrameLayout msubscriberViewController;
    private Session mSession;
    private Publisher mPublisher;
    private Subscriber mSubscriber;

    String receiverID,senderUserID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        Intent intent = getIntent();
        receiverID=intent.getStringExtra("receiverID");

        FirebaseUser currentUser= FirebaseAuth.getInstance().getCurrentUser();
        senderUserID=currentUser.getUid();

        final DatabaseReference mdata= FirebaseDatabase.getInstance().getReference();

        ImageButton close_video=findViewById(R.id.close_video);

        close_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mdata.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        if(dataSnapshot.child(senderUserID).child("CallingData").hasChild("Ringing"))
                        {
                            mdata.child(senderUserID).child("CallingData").child("Ringing").removeValue();

                            mdata.child(receiverID).child("CallingData").child("Calling").removeValue();
                            mdata.child(receiverID).child("CallingStop").setValue("1");

                            if (mPublisher!=null)
                            {
                                mPublisher.destroy();
                            }
                            if (mSubscriber!=null)
                            {
                                mSubscriber.destroy();
                            }


                            Intent i = new Intent(videoActivity.this, meassageActivity.class);
                            i.putExtra("userID",receiverID);
                            startActivity(i);
                            finish();
                        }

                        else if(dataSnapshot.child(senderUserID).child("CallingData").hasChild("Calling"))
                        {
                            mdata.child(senderUserID).child("CallingData").child("Calling").removeValue();

                            mdata.child(receiverID).child("CallingData").child("Ringing").removeValue();
                            mdata.child(receiverID).child("CallingStop").setValue("1");

                            if (mPublisher!=null)
                            {
                                mPublisher.destroy();
                            }
                            if (mSubscriber!=null)
                            {
                                mSubscriber.destroy();
                            }

                            Intent i = new Intent(videoActivity.this, meassageActivity.class);
                            i.putExtra("userID",receiverID);
                            startActivity(i);
                            finish();
                        }


                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


            }
        });


        mdata.child(senderUserID).child("CallingStop").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshotx) {

                if(dataSnapshotx.exists())
                {
                    if(dataSnapshotx.getValue().equals("1"))
                    {

                        mdata.child(receiverID).child("CallingStop").removeValue();
                        mdata.child(senderUserID).child("CallingStop").removeValue();

                        if (mPublisher!=null)
                        {
                            mPublisher.destroy();
                        }
                        if (mSubscriber!=null)
                        {
                            mSubscriber.destroy();
                        }

                        Intent i = new Intent(videoActivity.this, meassageActivity.class);
                        i.putExtra("userID",receiverID);
                        i.putExtra("index","1");
                        startActivity(i);
                        finish();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


      /*  mdata.child(receiverID).child("CallingStop").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshotx) {

                if(dataSnapshotx.exists())
                {
                    if(dataSnapshotx.getValue().equals("1"))
                    {

                        mdata.child(receiverID).child("CallingData").removeValue();
                        mdata.child(senderUserID).child("CallingData").removeValue();

                        if (mPublisher!=null)
                        {
                            mPublisher.destroy();
                        }
                        if (mSubscriber!=null)
                        {
                            mSubscriber.destroy();
                        }

                        Intent i = new Intent(videoActivity.this, meassageActivity.class);
                        i.putExtra("userID",receiverID);
                        startActivity(i);
                        finish();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

       */



        requestPermission();


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        EasyPermissions.onRequestPermissionsResult(requestCode,permissions,grantResults,videoActivity.this);

    }


    @AfterPermissionGranted(RC_VIDEO_APP_PERM)
    private void requestPermission()
    {
        String[] perms={Manifest.permission.INTERNET,Manifest.permission.CAMERA,Manifest.permission.RECORD_AUDIO};

        if(EasyPermissions.hasPermissions(this,perms))
        {
            mPublisherViewController=findViewById(R.id.pushlisher_container);
            msubscriberViewController=findViewById(R.id.subcryber_container);


            mSession=new Session.Builder(this,API_key,SESSION_ID).build();
            mSession.setSessionListener(videoActivity.this);
            mSession.connect(TOKEN);

        }
        else
        {
            EasyPermissions.requestPermissions(this,"Hey this app needs MIC and Camera, Please allow",RC_VIDEO_APP_PERM,perms);
        }
    }


    @Override
    public void onStreamCreated(PublisherKit publisherKit, Stream stream) {

    }

    @Override
    public void onStreamDestroyed(PublisherKit publisherKit, Stream stream) {

    }

    @Override
    public void onError(PublisherKit publisherKit, OpentokError opentokError) {

    }

    @Override
    public void onConnected(Session session) {

        Log.i(LOG_TAG,"Session Connectted");

        mPublisher =new Publisher.Builder(this).build();
        mPublisher.setPublisherListener(videoActivity.this);
        mPublisherViewController.addView(mPublisher.getView());

        if(mPublisher.getView() instanceof GLSurfaceView)
        {
            ((GLSurfaceView)mPublisher.getView()).setZOrderOnTop(true);
        }

        mSession.publish(mPublisher);

    }

    @Override
    public void onDisconnected(Session session) {

        Log.i(LOG_TAG,"Stream Disconnected");
    }

    @Override
    public void onStreamReceived(Session session, Stream stream) {

        Log.i(LOG_TAG,"Streem Received");

        if(mSubscriber==null)
        {
            mSubscriber=new Subscriber.Builder(this,stream).build();
            mSession.subscribe(mSubscriber);
            msubscriberViewController.addView(mSubscriber.getView());
        }
    }

    @Override
    public void onStreamDropped(Session session, Stream stream) {

        Log.i(LOG_TAG,"Stream Dreopped");

        if(mSubscriber!=null)
        {
            mSubscriber=null;
            msubscriberViewController.removeAllViews();
        }
    }

    @Override
    public void onError(Session session, OpentokError opentokError) {
        Log.i(LOG_TAG,"Stream Error");
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}